import abstractions.Engine;
import com.google.gson.Gson;
import exceptions.PathNotExistsException;
import exceptions.RepoNotInitializedException;
import managers.MagitEngine;
import models.Changes;
import models.Commit;
import models.Repository;
import models.Status;
import utils.FilesHandler;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet(urlPatterns = {"/PRActions"})
public class PRActions extends HttpServlet {
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, PathNotExistsException, RepoNotInitializedException {
        response.setContentType("application/json");
        Engine engine = (MagitEngine) getServletContext().getAttribute("engineObj");
        Repository repo=engine.getActiveRepo();
        String action=request.getParameter("action");
        try (PrintWriter out = response.getWriter()) {
            Gson gson = new Gson();
            String json = null;
            if (action.equals("watch")) {
                String data = request.getParameter("data");
                String id=getCommitId(data);
                List<Commit> allCommits =engine.getPrCommits(id);
                json = gson.toJson(allCommits);
            }
            else{
                if (action.equals("files")){
                    String sha1 = request.getParameter("sha1");
                    String id=request.getParameter("id");
                    List<Commit> allCommits =engine.getPrCommits(id);
                    Commit parent=null;
                    for (Commit commit : allCommits){
                        if (commit.getSha1().equals(sha1))
                            parent=commit;
                    }
                    Changes diff = engine.getDiff(parent.getSha1(), sha1);
                    String deleted=diff.DeletedFiles.toString();
                    String created=diff.CreatedFiles.toString();
                    String updated=diff.UpdatedFiles.toString();

                    json = gson.toJson(diff);
                }
                else{
                    if(action.equals("see")) {
                        String tempPath = request.getParameter("data");
                        String filePath = tempPath.replace(",", "\\");
                        String fileContent = FilesHandler.readFile(filePath);
                        json = gson.toJson(fileContent);
                    }
                    else{
                        if(action.equals("reject")) {
                            String data = request.getParameter("data");
                            String id=getCommitId(data);
                            engine.closePr(id, Status.REJECT);
                            json = gson.toJson("The PR has been Rejected");
                        }
                        else{
                            String message;
                            String data = request.getParameter("data");
                            String id=getCommitId(data);
                            String base=getParametersData(data,0);
                            String target=getParametersData(data,1);
                            engine.switchBranch(base);

                            if (engine.isOpenChanges())
                                message="you have some open changes, please commit your changes before you merge";
                            else{
                                engine.handleFFMerge(target);
                                message="The merge has been done successfully";

                            }
                            engine.closePr(id, Status.CLOSE);
                            json = gson.toJson(message);
                        }

                    }
                }

            }

            out.println(json);
            out.flush();
        }
    }
    public String getCommitId (String data){
        String[] split1 = data.split(",");
        return split1[0];

    }
    public String getParametersData (String data, int index) {
        String[] split1 = data.split(",");
         return split1[index+1];
    }


        /**
         * Handles the HTTP <code>GET</code> method.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (PathNotExistsException e) {
            e.printStackTrace();
        } catch (RepoNotInitializedException e) {
            e.printStackTrace();
        }

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (PathNotExistsException e) {
            e.printStackTrace();
        } catch (RepoNotInitializedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
