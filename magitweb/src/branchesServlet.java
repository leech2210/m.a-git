import abstractions.Engine;
import com.google.gson.Gson;
import exceptions.PathNotExistsException;
import exceptions.RepoNotInitializedException;
import managers.MagitEngine;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;

@WebServlet(urlPatterns = {"/branches"})
public class branchesServlet extends HttpServlet {

        protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, PathNotExistsException, RepoNotInitializedException {
        response.setContentType("application/json");
        Engine engine = (MagitEngine) getServletContext().getAttribute("engineObj");
        String action=request.getParameter("action");
        try (PrintWriter out = response.getWriter()) {
            Gson gson = new Gson();
            String json = null;
            if(action.equals("show")) {
                Map<String, List<String>> branchesSha1 = engine.getAllBranchesSha1();
                json = gson.toJson(branchesSha1);
            }
            else{
                if (action.equals("headBranch")){
                    String headBranch = engine.getHeadBranch();
                    json = gson.toJson(headBranch);
                }
                else{
                        String message;
                        String branchAction = getParametersData(action, 1);
                        String branchName = getParametersData(action, 0);
                        if (branchAction.equals("checkout")) {
                            if (branchName.contains("."))
                                message = "The branch you choose is rb, you can create an rtb and switch to him";
                            else {
                                if (engine.isOpenChanges())
                                    message = "You have some open changes, please commit your changes before you switch branch";
                                else {
                                    engine.switchBranch(branchName);
                                    message = "Branch switched to - " + branchName;
                                }
                            }
                            json = gson.toJson(message);
                        } else {
                            if (!engine.getHeadBranch().equals(branchName)) {
                                branchName = branchName.replace(".", "\\");
                                engine.deleteBranch(branchName);
                                message = "Branch deleted - " + branchName;
                            } else
                                message = "branch - " + branchName + " is the head branch, you can't delete it!";

                            json = gson.toJson(message);
                        }


                    }
            }

            //String json = gson.toJson(message);
            out.println(json);
            out.flush();
        }
    }
    public String getParametersData (String data, int index){
        String[] split1 = data.split("-");
        return split1[index];

    }

        /**
         * Handles the HTTP <code>GET</code> method.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        @Override
        protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (PathNotExistsException e) {
            e.printStackTrace();
        } catch (RepoNotInitializedException e) {
            e.printStackTrace();
        }

    }

        /**
         * Handles the HTTP <code>POST</code> method.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        @Override
        protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (PathNotExistsException e) {
            e.printStackTrace();
        } catch (RepoNotInitializedException e) {
            e.printStackTrace();
        }
    }

        /**
         * Returns a short description of the servlet.
         *
         * @return a String containing servlet description
         */
        @Override
        public String getServletInfo() {
        return "Short description";
    }

}


