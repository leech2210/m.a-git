import managers.MagitEngine;

import javax.servlet.ServletContext;

public class servletUtils {


    private static final Object userManagerLock1 = new Object();
    private static final Object userManagerLock2 = new Object();

    public static userManager getUserManager(ServletContext servletContext) {
        synchronized (userManagerLock1) {
            if (servletContext.getAttribute("userManager") == null) {
                servletContext.setAttribute("userManager", new userManager());
            }
        }
        return (userManager) servletContext.getAttribute("userManager");
    }

    public static void creatEngineObj(ServletContext servletContext) {
        synchronized (userManagerLock2) {
            if (servletContext.getAttribute("engineObj") == null)
                servletContext.setAttribute("engineObj", new MagitEngine());
        }

    }
}
