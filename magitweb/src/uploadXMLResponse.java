import abstractions.Engine;
import com.google.gson.Gson;
import exceptions.XmlFileExceptions;
import managers.MagitEngine;
import models.Repository;
import utils.FilesHandler;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

import static utils.Consts.GIT_FOLDER;

@WebServlet(urlPatterns = {"/uploadXML"})
@MultipartConfig(fileSizeThreshold = 1024 * 1024, maxFileSize = 1024 * 1024 * 5, maxRequestSize = 1024 * 1024 * 5 * 5)
public class uploadXMLResponse extends HttpServlet {
    private static final String ALL_REPO_PATH = "C:\\magit-ex3";
    private static final String USERNAME = "UserName";

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession session = request.getSession(false);
        String localUserName = session.getAttribute(USERNAME).toString();
        List<String> tt = new ArrayList<>();
        String data = request.getParameter("data");
        String[] split1=data.split(",");
        String repoPath=ALL_REPO_PATH + "\\" + localUserName + "\\" + split1[1];
        Path index = Paths.get(repoPath);
        Files.walk(index)
                .sorted(Comparator.reverseOrder())  // as the file tree is traversed depth-first and that deleted dirs have to be empty
                .forEach(t -> {
                    try {
                        Files.delete(t);
                    } catch (IOException e) {
                        tt.add(e.getMessage());

                    }
                });
        //FilesHandler.deleteAllDirectory(repoPath);
        response.setContentType("application/json");

        try (PrintWriter out = response.getWriter()) {
            if (tt.size() == 0)
                tt.add("The repo was successfully uploaded");
            Gson gson = new Gson();
            String json = gson.toJson(tt);
            out.println(json);
            out.flush();
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        String localUserName = session.getAttribute(USERNAME).toString();

        Collection<Part> parts = request.getParts();
        StringBuilder fileContent = new StringBuilder();
        InputStream inStream = null;
        InputStream inStreamAgain = null;
        InputStream again = null;
        List<String> t=new ArrayList<>();
        for (Part part : parts) {
            //to write the content of the file to a string
            fileContent.append(readFromInputStream(part.getInputStream()));
            inStream = part.getInputStream();
            inStreamAgain = inStream;
            again = inStream;
            if(part.getSubmittedFileName()== null)
                t.add("You didn't insert path");
        }
        Engine engine = (MagitEngine) getServletContext().getAttribute("engineObj");
        response.setContentType("application/json");

        //Map<String, InputStream> test = new HashMap<>();

        //t.add("te");
        String repoPath;
        try (PrintWriter out = response.getWriter()) {
            try {

                if(t.size()==0) {
                    Repository repo = engine.getXmlRepo(inStream);
                    repoPath = ALL_REPO_PATH + "\\" + localUserName + "\\" + repo.getName();
                    engine.xmlFileValidation(inStreamAgain);

                    if (FilesHandler.isDirectory(repoPath + GIT_FOLDER))
                        //test.put("folder .magit is already exists", again);
                        t.add("The repo," + repo.getName() + ",is already exists");
                        //showRepoExistsAlert(repo, xmlFilePath);
                    else {

                        engine.loadRepoFromXML(again, repoPath);
                    }
                }
            } catch (XmlFileExceptions xmlFileExceptions) {
             //test.put("The xml file is invalid: " ,null);
                t.add("The xml file is invalid: "+"\n" + xmlFileExceptions.getMessage());
            } catch (Exception e) {
                //test.put(e.getMessage(),null);
                t.add(e.getMessage());
            }
            if(t.size()==0)
                t.add("The repo was successfully uploaded");
            Gson gson = new Gson();
            String json = gson.toJson(t);
            out.println(json);
            out.flush();
        }
    }


        //String xmlString=fileContent.toString();

        //out.println(fileContent.toString());

    private String readFromInputStream(InputStream inputStream) {
        return new Scanner(inputStream).useDelimiter("\\Z").next();
    }



    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
