import abstractions.Engine;
import com.google.gson.Gson;
import exceptions.PathNotExistsException;
import exceptions.RemoteException;
import exceptions.RepoNotInitializedException;
import managers.MagitEngine;
import models.Repository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(urlPatterns = {"/remote"})
public class collaborationServlet extends HttpServlet {
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, PathNotExistsException, RepoNotInitializedException {
        response.setContentType("application/json");
        Engine engine = (MagitEngine) getServletContext().getAttribute("engineObj");
        String action = request.getParameter("action");
        Repository repo = engine.getActiveRepo();
        String remote =repo.getRemoteLocation();
        try (PrintWriter out = response.getWriter()) {
            Gson gson = new Gson();
            String json = null;
            String message = null;
            if (action.equals("pull")) {
                try {
                    if (remote.equals(""))
                        message = "Your repository is not tracking after another repository, you cannot perform PULL";
                    else {
                        engine.pull();
                        message = "Pull successfully!";
                    }
                    } catch(IOException | PathNotExistsException | RepoNotInitializedException
                            | RemoteException e){
                        message = e.getMessage();
                    }


            } else { try{
                         if (remote.equals(""))
                             message = "Your repository is not tracking after another repository, you cannot perform PUSH";
                         else {
                             engine.push();
                             message = "Push successfully!";
                         }
                    } catch (IOException | PathNotExistsException | RepoNotInitializedException
                             |RemoteException e) {
                              message=e.getMessage();
                    }

            }

                json = gson.toJson(message);
                out.println(json);
                out.flush();

        }
    }
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (PathNotExistsException e) {
            e.printStackTrace();
        } catch (RepoNotInitializedException e) {
            e.printStackTrace();
        }

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (PathNotExistsException e) {
            e.printStackTrace();
        } catch (RepoNotInitializedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }


}
