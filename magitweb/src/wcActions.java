import abstractions.Engine;
import com.google.gson.Gson;
import exceptions.PathNotExistsException;
import exceptions.RepoNotInitializedException;
import managers.MagitEngine;
import models.Commit;
import models.Repository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

@WebServlet(urlPatterns = {"/wcActions"})
public class wcActions extends HttpServlet {
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, PathNotExistsException, RepoNotInitializedException {
        response.setContentType("application/json");
        Engine engine = (MagitEngine) getServletContext().getAttribute("engineObj");
        String headBranch=engine.getHeadBranch();
        Repository repo=engine.getActiveRepo();
        String action=request.getParameter("action");
        try (PrintWriter out = response.getWriter()) {
            Gson gson = new Gson();
            String json = null;
            if (action.equals("commits")) {
                Map<String, String> commitsData= new TreeMap<>();
                List<Commit> allCommits = engine.getBranchCommits(headBranch);
                Map<String, List<String>> allBranches=engine.getAllBranchesSha1();
                Map<String, List<String>> branchesSha1 = engine.getAllBranchesSha1();
                for (Commit commit : allCommits){
                    List<String> branches=allBranches.get(commit.getSha1());
                    if(branches!=null) {
                        String branchesUnion = listToString(branches);
                        String Data = putAllData(commit);
                        commitsData.put(Data, branchesUnion);
                    }
                    else{
                        String Data = putAllData(commit);
                        commitsData.put(Data, "-");
                    }
                }
                json = gson.toJson(commitsData);
            }
            else{
                if (action.equals("files")){
                    String sha1=request.getParameter("data");
                    List<String> allFiles=engine.getCommitFiles(sha1);
                    json = gson.toJson(allFiles);
                }


            }


            out.println(json);
            out.flush();
        }
    }
    public String getFilePath (String data){
        String[] split1 = data.split(",");
        return split1[0];

    }
    public String listToString(List<String> branches){
        String data="";
        for(String branch : branches)
            data += branch + ",";
        return data;
    }
    public String putAllData(Commit commit) {
        return String.join(";",
                commit.getSha1(),
                commit.getCommitMessage(),
                commit.getDate(),
                commit.getUserName());

    }


    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (PathNotExistsException e) {
            e.printStackTrace();
        } catch (RepoNotInitializedException e) {
            e.printStackTrace();
        }

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (PathNotExistsException e) {
            e.printStackTrace();
        } catch (RepoNotInitializedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
