import abstractions.Engine;
import com.google.gson.Gson;
import exceptions.InvalidMagitRepoExceptions;
import exceptions.PathNotExistsException;
import exceptions.RepoNotInitializedException;
import managers.MagitEngine;
import models.Repository;
import utils.FilesHandler;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

@WebServlet(urlPatterns = {"/fork"})
public class userAction extends HttpServlet {
    private static final String ALL_REPO_PATH = "C:\\magit-ex3";
    private static final String USERNAME = "UserName";
    private static final String REMOTE_REPO= "repoAndAction";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, PathNotExistsException, RepoNotInitializedException {
        response.setContentType("application/json");
        List<String> message=new ArrayList<>();
        try (PrintWriter out = response.getWriter()) {
            String popup="true";
            String action = getParametersData(request.getParameter(REMOTE_REPO), 1);

            HttpSession session = request.getSession(false);
            String localUserName = session.getAttribute(USERNAME).toString();
            String repoLocalPath = ALL_REPO_PATH + "\\" + localUserName;
            String repoTempName = getParametersData(request.getParameter(REMOTE_REPO), 0);
            String repoName=repoTempName.replace("!", " ");
            Engine engine = (MagitEngine) getServletContext().getAttribute("engineObj");

            if (action.equals("open")) {
                popup=request.getContextPath() + "/singlerepo.html";
                message.add(popup);
                try {
                    engine.changeRepo(repoLocalPath + "\\" + repoName);
                } catch (InvalidMagitRepoExceptions invalidMagitRepoExceptions) {
                    invalidMagitRepoExceptions.printStackTrace();
                }

            }
            else {
                message.add(popup);
                message.add(repoName);

                String forkedUserName=request.getParameter(USERNAME);
                String forkNotification="The user: " + localUserName + " has fork your repo: " + repoName;
                String forkUserPath=ALL_REPO_PATH + "\\" + forkedUserName +"\\" + "notifications.txt";
                //TODO: change to lee function
                FilesHandler.appendToFile(forkUserPath,forkNotification + "\n");
                String repoRemotePath = ALL_REPO_PATH + "\\" + request.getParameter(USERNAME) + "\\" + repoName;

                Repository repo = new Repository(repoName, repoLocalPath + "\\" + repoName, repoRemotePath);
                engine.clone(repoRemotePath, repo);
            }
            Gson gson = new Gson();
            String json = gson.toJson(message);
            out.println(json);
            out.flush();
        }
    }

    public String getParametersData (String data, int index){
        String[] split1 = data.split("-");
        if (index==0)
        return split1[index];
        else{
            String[] split2=split1[index].split(".png");
            return split2[0];
        }
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (PathNotExistsException e) {
            e.printStackTrace();
        } catch (RepoNotInitializedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (PathNotExistsException e) {
            e.printStackTrace();
        } catch (RepoNotInitializedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
