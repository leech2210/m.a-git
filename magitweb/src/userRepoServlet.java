import abstractions.Engine;
import com.google.gson.Gson;
import exceptions.PathNotExistsException;
import exceptions.RepoNotInitializedException;
import managers.MagitEngine;
import models.Repository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

@WebServlet(urlPatterns = {"/myRepoMain"})
public class userRepoServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, PathNotExistsException, RepoNotInitializedException {
        response.setContentType("application/json");
        List<String> message=new ArrayList<>();
        try (PrintWriter out = response.getWriter()) {
            Engine engine = (MagitEngine) getServletContext().getAttribute("engineObj");
            Repository repo = engine.getActiveRepo();
            String remote =repo.getRemoteLocation();
            message.add(repo.getName() + " repository");
            if (!remote.equals(""))
            {
                String remoteUser=getUserData(remote, 1);
                String remoteRepo=getUserData(remote, 2);
                message.add("Tracking after: " + remoteRepo + " of: " + remoteUser);
            }


            Gson gson = new Gson();
            String json = gson.toJson(message);
            out.println(json);
            out.flush();
        }
    }

    public String getUserData (String data, int index){
        String[] split1 = data.split("magit-ex3");
        String[] split2 = split1[1].split("\\\\");

        return split2[index];


    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (PathNotExistsException e) {
            e.printStackTrace();
        } catch (RepoNotInitializedException e) {
            e.printStackTrace();
        }

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (PathNotExistsException e) {
            e.printStackTrace();
        } catch (RepoNotInitializedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}

