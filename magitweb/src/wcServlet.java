import abstractions.Engine;
import com.google.gson.Gson;
import exceptions.PathNotExistsException;
import exceptions.RepoNotInitializedException;
import managers.MagitEngine;
import models.Changes;
import models.Commit;
import models.Folder;
import models.Repository;
import utils.FilesHandler;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@WebServlet(urlPatterns = {"/WC"})
public class wcServlet extends HttpServlet {
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, PathNotExistsException, RepoNotInitializedException {
        response.setContentType("application/json");
        Engine engine = (MagitEngine) getServletContext().getAttribute("engineObj");
        String headBranch=engine.getHeadBranch();
        Repository repo=engine.getActiveRepo();
        String action=request.getParameter("action");
        try (PrintWriter out = response.getWriter()) {
            Gson gson = new Gson();
            String json = null;

            if(action.equals("myCommit")) {
                Commit currCommit = engine.getBranchCommit(headBranch);
                Folder folder = engine.getRootFolderCommit(currCommit.getSha1());
                try (Stream<Path> walk = Files.walk(Paths.get(folder.getFullName()))) {
                    List<String> result = walk.filter(Files::isRegularFile).map(x -> x.toString())
                            .filter(f -> !f.contains(".magit")).collect(Collectors.toList());
                    json = gson.toJson(result);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                //List<String> allFiles = engine.getCommitFiles(currCommit.getSha1());
            }
            else{
                if(action.equals("watch")){
                    String data = request.getParameter("data");
                    String tempPath=getFilePath(data);
                    String temp=tempPath.replace("!", "\\");
                    String filePath = temp.replace(";", " ");
                    String fileContent = FilesHandler.readFile(filePath);
                    json = gson.toJson(fileContent);
                }
                else{
                    if(action.equals("delete")){
                        String data = request.getParameter("data");
                        String tempPath=getFilePath(data);
                        String temp=tempPath.replace("!", "\\");
                        String filePath = temp.replace(";", " ");
                        FilesHandler.deleteFile(filePath);
                        json = gson.toJson("The file has been deleted");
                    }
                     else{
                         if (action.equals("edit")){
                             String content = request.getParameter("data");
                             String tempData=request.getParameter("path");
                             String tempPath=getFilePath(tempData);
                             String temp=tempPath.replace("!", "\\");
                             String filePath = temp.replace(";", " ");
                             FilesHandler.writeToFile(filePath,content,false);
                             json = gson.toJson("The file: " + filePath + " has been updated");
                         }
                          else {
                             if (action.equals("create")) {
                                 String repoPath = repo.getLocation();
                                 String content = request.getParameter("data");
                                 String tempData = request.getParameter("path");
                                 String tempPath = getFilePath(tempData);
                                 String temp=tempPath.replace("!", "\\");
                                 String filePath = temp.replace(";", " ");
                                 FilesHandler.writeToFile(repoPath + "\\" + filePath, content, false);
                                 json = gson.toJson("The file: " + repoPath + "\\" + filePath + " has been created");
                             }
                             else{
                                 if(action.equals("status")) {
                                     Changes status= engine.getStatus();
                                     json = gson.toJson(status.toString());
                                 }
                                 else{
                                     if(action.equals("openChanges")) {
                                         if (!engine.isOpenChanges())
                                             json = gson.toJson("There are no changes to commit!");
                                         else
                                             json = gson.toJson("yes");
                                     }
                                     else {
                                         String commitMessage = request.getParameter("data");
                                         String commitSha1 = engine.commit(commitMessage);
                                         String message="all the changes are committed - " + commitSha1;
                                         json = gson.toJson(message);
                                     }
                                 }

                             }
                         }
                     }
                }
            }

            out.println(json);
            out.flush();
        }
    }
    public String getFilePath (String data){
        String[] split1 = data.split(",");
        return split1[0];

    }
    public String listToString(List<String> branches){
        String data="";
        for(String branch : branches)
            data += branch + ",";
        return data;
    }
    public String putAllData(Commit commit) {
        return String.join("!!",
                commit.getSha1(),
                commit.getCommitMessage(),
                commit.getDate(),
                commit.getUserName());

    }


    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (PathNotExistsException e) {
            e.printStackTrace();
        } catch (RepoNotInitializedException e) {
            e.printStackTrace();
        }

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (PathNotExistsException e) {
            e.printStackTrace();
        } catch (RepoNotInitializedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
