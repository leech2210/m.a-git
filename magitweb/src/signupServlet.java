import abstractions.Engine;
import exceptions.PathNotExistsException;
import managers.MagitEngine;
import utils.FilesHandler;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;


@WebServlet(urlPatterns = {"/login"})
public class signupServlet extends HttpServlet {
    private static final String USERNAME = "UserName";
    private static final String ALL_REPO_PATH = "C:\\magit-ex3";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        servletUtils.creatEngineObj(getServletContext());
        Engine engine = (MagitEngine) getServletContext().getAttribute("engineObj");
        userManager userManager = servletUtils.getUserManager(getServletContext());

        String userName = req.getParameter(USERNAME);
        if (userName == null || userName.isEmpty())
            resp.sendRedirect(req.getContextPath() + "/index.html");
        else {//Todo: return this lines after finish project
             if (userManager.isUserExists(userName)== false){
                 FilesHandler.createDirectory(ALL_REPO_PATH + "\\" + userName);
                 try {
                     FilesHandler.writeToFile(ALL_REPO_PATH + "\\" + userName + "\\"
                             + "notifications.txt" , "", false);
                 } catch (PathNotExistsException e) {
                     e.printStackTrace();
                 }
             }

               userManager.addUser(userName);
               engine.updateUser(userName);
               HttpSession aSession = req.getSession();
               aSession.setAttribute(USERNAME, userName);
               resp.sendRedirect(req.getContextPath() + "/myuser.html");
                }
            }
        }




