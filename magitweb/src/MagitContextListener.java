import utils.Consts;
import utils.FilesHandler;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;

public class MagitContextListener implements ServletContextListener {
    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        if(FilesHandler.isDirectory(Consts.ALL_REPO_PATH)){
            Path index = Paths.get(Consts.ALL_REPO_PATH);
            try {
                Files.walk(index)
                        .sorted(Comparator.reverseOrder())  // as the file tree is traversed depth-first and that deleted dirs have to be empty
                        .forEach(t -> {
                            try {
                                Files.delete(t);
                            } catch (IOException e) {
                                e.printStackTrace();

                            }
                        });
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            FilesHandler.createDirectory(Consts.ALL_REPO_PATH);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        if(FilesHandler.isDirectory(Consts.ALL_REPO_PATH)){
            Path index = Paths.get(Consts.ALL_REPO_PATH);
            try {
                Files.walk(index)
                        .sorted(Comparator.reverseOrder())  // as the file tree is traversed depth-first and that deleted dirs have to be empty
                        .forEach(t -> {
                            try {
                                Files.delete(t);
                            } catch (IOException e) {
                                e.printStackTrace();

                            }
                        });
            } catch (IOException e) {
                e.printStackTrace();
            }
            FilesHandler.deleteDirectory(Consts.ALL_REPO_PATH);
        }
    }
}
