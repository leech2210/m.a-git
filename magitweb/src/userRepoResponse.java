import com.google.gson.Gson;
import exceptions.PathNotExistsException;
import utils.Consts;
import utils.FilesHandler;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;


@WebServlet(urlPatterns = {"/theUsersRepo"})
public class userRepoResponse extends HttpServlet {
    private static final String ALL_REPO_PATH = "C:\\magit-ex3";
    private static final String USERNAME = "UserName";
    private static final String MY_REPO = "open.png";
    private static final String FORK = "fork.png";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, PathNotExistsException {
        String icon=FORK;
        response.setContentType("application/json");
        String userName=request.getParameter(USERNAME);
        if (userName == null){
            HttpSession session = request.getSession(false);
            userName=session.getAttribute(USERNAME).toString();
            icon =MY_REPO;
        }
        else{
            HttpSession session = request.getSession(false);
            String sessionUserName=session.getAttribute(USERNAME).toString();
            if(userName.equals(sessionUserName))
                icon =MY_REPO;
        }

        Map <String,List<String>> userData = new TreeMap<>();
        String userDirPath= ALL_REPO_PATH + "\\" + userName;
        Boolean isExist=FilesHandler.isDirectory(userDirPath);
        if (isExist) {
            List<String> userRepos = FilesHandler.listOnlyFoldersFilter(userDirPath);
            if (userRepos.size() != 0) {
                List<String> repoData;
                for (String repo : userRepos) {
                    repoData = new ArrayList<>();
                    //Repo name
                    repoData.add(repo);
                    String repoTempName=nameWithoutSpace(repo);
                    String userRepoPath = userDirPath + "\\" + repo;
                    //Active Branch
                    String dataToAdd = FilesHandler.readFile(userRepoPath + "\\" + Consts.HEAD_BRANCH_FILE);
                    //sha1
                    String commitSha1=getSha1(userRepoPath + "\\" + Consts.BRANCHES_FOLDER + "\\"
                            + dataToAdd + ".txt", userRepoPath);
                    String commitData = FilesHandler.readFile(userRepoPath + "\\" + Consts.OBJECTS_FOLDER
                            + "\\" + commitSha1 + ".zip");
                    String[] split = commitData.split("\n");
                    String commitMessage = split[0];
                    String commitDate = split[1];
                    String[] split2 = commitDate.split("-");
                    commitDate = split2[0];
                    repoData.add(dataToAdd);
                    int numBranches = FilesHandler.numberOfFiles(userRepoPath + "\\" + Consts.BRANCHES_FOLDER) - 1;
                    repoData.add(Integer.toString(numBranches));
                    repoData.add(commitDate);
                    repoData.add(commitMessage);
                    repoData.add(icon);
                    userData.put(repoTempName, repoData);
                }
            }
        }

        try (PrintWriter out = response.getWriter()) {
            Gson gson = new Gson();
            String json = gson.toJson(userData);
            out.println(json);
            out.flush();
        }

    }

    public String nameWithoutSpace(String repoName){
        return repoName.replace(" ", "!");
    }

    public String getSha1(String filePath, String repoPath) throws IOException, PathNotExistsException {
        String remote=FilesHandler.readFile(repoPath + "\\" + Consts.REMOTE_PATH_FILE);
        String data=FilesHandler.readFile(filePath);
        if (remote.equals(""))
         return data;
        String[] split = data.split(",");
        return split[0];
    }
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (PathNotExistsException e) {
            e.printStackTrace();
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (PathNotExistsException e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
