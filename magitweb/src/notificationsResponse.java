import com.google.gson.Gson;
import exceptions.PathNotExistsException;
import utils.FilesHandler;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;


@WebServlet(urlPatterns = {"/theNotifications"})
public class notificationsResponse extends HttpServlet {
    private static final String ALL_REPO_PATH = "C:\\magit-ex3";
    private static final String USERNAME = "UserName";
    private static final String NOTIFICATION_PATH = "notifications.txt";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, PathNotExistsException {
        response.setContentType("application/json");
        try (PrintWriter out = response.getWriter()){
            HttpSession session = request.getSession(false);
            String userName=session.getAttribute(USERNAME).toString();

            String userDirPath= ALL_REPO_PATH + "\\" + userName;
            String content= FilesHandler.readFile(userDirPath + "\\" + NOTIFICATION_PATH);
            FilesHandler.writeToFile(userDirPath + "\\" + NOTIFICATION_PATH, "", false);
            List<String> notifications=new ArrayList<>();
            String[] split = content.split("\n");
            for (String not: split){
                notifications.add(not);
            }
            //FilesHandler.deleteFile(userDirPath + "\\" + NOTIFICATION_PATH);
            Gson gson = new Gson();
            String json = gson.toJson(notifications);
            out.println(json);
            out.flush();
        }

    }


    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (PathNotExistsException e) {
            e.printStackTrace();
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (PathNotExistsException e) {
            e.printStackTrace();
        }
    }
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
