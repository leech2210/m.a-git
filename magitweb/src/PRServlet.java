import abstractions.Engine;
import com.google.gson.Gson;
import exceptions.PathNotExistsException;
import exceptions.RepoNotInitializedException;
import managers.MagitEngine;
import models.PullRequest;
import models.Repository;
import utils.Consts;
import utils.FilesHandler;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@WebServlet(urlPatterns = {"/PR"})
public class PRServlet extends HttpServlet {
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, PathNotExistsException, RepoNotInitializedException {
        response.setContentType("application/json");
        Engine engine = (MagitEngine) getServletContext().getAttribute("engineObj");
        Repository repo=engine.getActiveRepo();
        String action = request.getParameter("action");

        try (PrintWriter out = response.getWriter()) {
            Gson gson = new Gson();
            String json = null;
            if (action.equals("show")) {
                List<PullRequest> OPEN = new ArrayList<PullRequest>();
                List<PullRequest> CLOSE = new ArrayList<PullRequest>();
                List<PullRequest> REJECT = new ArrayList<PullRequest>();
                Map<String, List<PullRequest>> allPR = new HashMap<>();
                String filePath = repo.getLocation() + Consts.PULL_REQUEST;
                if (FilesHandler.isFile(filePath)){
                    String PRFile = FilesHandler.readFile(repo.getLocation() + Consts.PULL_REQUEST);
                    String[] split = PRFile.split("\n");
                    for (String line : split) {
                         String fixedLine = getUserPRLine(line);
                         PullRequest pr = PullRequest.parse(fixedLine);
                         String array = pr.getStatus();
                         switch (array) {
                            case "OPEN":
                                OPEN.add(pr);
                                break;
                            case "CLOSE":
                                CLOSE.add(pr);
                                break;
                            case "REJECT":
                                REJECT.add(pr);
                                break;
                    }
                }
            }
                allPR.put("OPEN",OPEN);
                allPR.put("CLOSE",CLOSE);
                allPR.put("REJECT",REJECT);

                json = gson.toJson(allPR);
            }
            else{
                if (action.equals("branchTarget")){
                    Map<String, List<String>> branchesSha1 = engine.getAllRTBBranchesSha1();
                    json = gson.toJson(branchesSha1);
                }
                else{
                    if (action.equals("branchBase")){
                        Map<String, List<String>> branchesSha1 = engine.getAllRTBAndRbBranchesSha1();
                        json = gson.toJson(branchesSha1);
                    }
                    else{
                        if(action.equals("create")){
                            String targetTemp = request.getParameter("target");
                            String target = targetTemp.replace("!", "\\");
                            String baseTemp = request.getParameter("base");
                            String base = baseTemp.replace("!", "\\");
                            String message = request.getParameter("message");
                            String userAssigned=getUserAssigned(repo.getRemoteLocation());
                            engine.createNewPr(target,base,message, userAssigned );
                            json = gson.toJson("success");
                        }
                        else {
                            String remote = repo.getRemoteLocation();
                            if (remote.equals(""))
                                json = gson.toJson("Your repository is not tracking after another repository, you cannot create PR");
                            else
                                json=gson.toJson("Ok");

                        }
                    }

                }
            }

                out.println(json);
                out.flush();
            }
        }
    public String getUserAssigned (String repoPath) {
        String[] split1 = repoPath.split("magit-ex3\\\\");
        String[] split2=split1[1].split("\\\\");
        return split2[0];
    }
    public String getUserPRLine (String data) {
        String[] split1 = data.split("\r");
        if(split1.length==0)
          return data;
        return split1[0];
    }



    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (PathNotExistsException e) {
            e.printStackTrace();
        } catch (RepoNotInitializedException e) {
            e.printStackTrace();
        }

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (PathNotExistsException e) {
            e.printStackTrace();
        } catch (RepoNotInitializedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
