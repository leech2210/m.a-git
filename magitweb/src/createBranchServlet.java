import abstractions.Engine;
import com.google.gson.Gson;
import exceptions.PathNotExistsException;
import exceptions.RepoNotInitializedException;
import managers.MagitEngine;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(urlPatterns = {"/createBranch"})
public class createBranchServlet extends HttpServlet {

        protected void processRequest(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException, PathNotExistsException, RepoNotInitializedException {
            response.setContentType("application/json");
            Engine engine = (MagitEngine) getServletContext().getAttribute("engineObj");
            String action=request.getParameter("action");
            try (PrintWriter out = response.getWriter()) {
                Gson gson = new Gson();
                String json = null;
                String message = null;
                if(action.equals("create")){ 
                    String commitSha1=request.getParameter("sha1");
                    String name=request.getParameter("name");
                    if(engine.isPointedAsRB(commitSha1).equals("")){
                        engine.addNewBranch(name, commitSha1,"2", engine.isPointedAsRB(commitSha1));
                        message= "Branch added - " + name;
                    }
                    else
                        message="This sha1 pointed by the branch: " + engine.isPointedAsRB(commitSha1) +  "\n" +
                                "Do you want to creat the new branch as RTB to " + engine.isPointedAsRB(commitSha1) + "?";

                        }
                else{
                    String type=request.getParameter("type");
                    String commitSha1=request.getParameter("sha1");
                    String name=request.getParameter("name");
                    engine.addNewBranch(name, commitSha1,type, engine.isPointedAsRB(commitSha1));
                    if(type.equals("1"))
                       message= "RTB branch added - " + name;
                    else
                        message= "Branch added - " + name;
                }
                json = gson.toJson(message);
                out.println(json);
                out.flush();
            }
        }
        /**
         * Handles the HTTP <code>GET</code> method.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        @Override
        protected void doGet(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException {
            try {
                processRequest(request, response);
            } catch (PathNotExistsException e) {
                e.printStackTrace();
            } catch (RepoNotInitializedException e) {
                e.printStackTrace();
            }

        }

        /**
         * Handles the HTTP <code>POST</code> method.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        @Override
        protected void doPost(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException {
            try {
                processRequest(request, response);
            } catch (PathNotExistsException e) {
                e.printStackTrace();
            } catch (RepoNotInitializedException e) {
                e.printStackTrace();
            }
        }

        /**
         * Returns a short description of the servlet.
         *
         * @return a String containing servlet description
         */
        @Override
        public String getServletInfo() {
            return "Short description";
        }

    }

