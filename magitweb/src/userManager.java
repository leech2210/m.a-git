import javax.servlet.ServletContext;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class userManager {
    private final Set<String> usersSet;

    public userManager() {
        usersSet = new HashSet<>();
    }

    public synchronized void addUser(String username) {
        usersSet.add(username);
    }

    public synchronized void removeUser(String username) {
        usersSet.remove(username);
    }

    public synchronized Set<String> getUsers() {
        return Collections.unmodifiableSet(usersSet);
    }

    public boolean isUserExists(String username) {
        return usersSet.contains(username);
    }

    public userManager getUserManager(ServletContext servletContext) {

		synchronized (usersSet) {
			if (servletContext.getAttribute("userManager") == null) {
				servletContext.setAttribute("userManager", new userManager());
			}
		}
		return (userManager) servletContext.getAttribute("userManager");
	}
}


