var idOfCommit;
var refreshRate = 1000;

function logoutFunc() {
    $.ajax({
        url:"logout",
        success: function(message) {
            var temp=message.substring(1, 14);
            window.location.href = temp;
            alert("You have successfully logged out");
        }
    });

}
function view_text () {
    // Find html elements.
    var textArea = document.getElementById('my_text');
    var div = document.getElementById('view_text');
    // Put the text in a variable so we can manipulate it.
    var text = textArea.value;
    // Make sure html and php tags are unusable by disabling < and >.
    text = text.replace(/\</gi, "<");
    text = text.replace(/\>/gi, ">");
    text = text.replace(/\n/gi, "<br />");
    // Print the text in the div made for it.
    div.innerHTML = text;
}


function addTargetBranchesList(userBranches) {
    $.each(userBranches || [], function (sha1, branches) {
        $.each(branches || [], function (index, branch){
            $('<option value= '+ branch + '>' + branch + '</option>').appendTo($("#selectT"));
            $('<option value= '+ branch + '>' + branch + '</option>').appendTo($("#selectB"));
        });
    });
}
function cleanCommitsData() {
    $("#allCommits").empty();

}
function cleanCommitFilesData() {
    $("#allFiles").empty();

}
function cleanFilesContent() {
    $("#fileContent").empty();

}

function showAllCommits(commits) {
    $('<div class="commits">Commits</div>').appendTo($("#allCommits"));
    $('<div class="action"></div>').appendTo($("#allCommits"));

    $.each(commits || [], function (index, commit) {
        $('<div id=' + commit.sha1 + '>' + commit.sha1 +' <br> created by: ' + commit.userName +
            '<br> at: ' + commit.dateStr +' <br>' + commit.commitMessage + '</div>').appendTo($("#allCommits"));
        $('<button id=' + commit.sha1 + '> <img src=images/files.png></button>').appendTo($("#allCommits"));

    });

}
function showFileContent(content) {
    $('<textarea class="text_edit" id="my_text">' + content + '</textarea><br />').appendTo($("#fileContent"));
    $('<div id="view_text"></div>').appendTo($("#fileContent"));
    view_text();
}
function showAllCommitFiles(files) {
    var created=files.CreatedFiles;
    var deleted=files.DeletedFiles;
    var updated=files.UpdatedFiles;

    $('<div class="created">CreatedFiles</div>').appendTo($("#allFiles"));
    if(created.length!==0){
        $.each(created || [], function (index, file) {
            var temp=file;
            while(temp.includes("\\")){
                temp=temp.replace("\\", ",");
            }
            $('<button id=' + temp + '>' + temp + '</button>').appendTo($("#allFiles"));
        });
    }
    else
        $('<br>').appendTo($("#allFiles"));

    $('<div class="deleted">DeletedFiles</div>').appendTo($("#allFiles"));
    if(deleted.length!==0){
        $.each(deleted || [], function (index, file) {
            var temp=file;
            while(temp.includes("\\")){
                temp=temp.replace("\\", ",");
            }
            $('<div id=' + temp + '>' + temp + '</div>').appendTo($("#allFiles"));
        });
    }
    else
        $('<br>').appendTo($("#allFiles"));

    $('<div class="updated">updatedFiles</div>').appendTo($("#allFiles"));
    if(updated.length!==0){
        $.each(updated || [], function (index, file) {
            var temp=file;
            while(temp.includes("\\")){
                temp=temp.replace("\\", ",");
            }
            $('<button id=' + temp + '>' + temp + '</button>').appendTo($("#allFiles"));
        });
    }
    else
        $('<br>').appendTo($("#allFiles"));

}
function  ajaxTargetBranchesList() {
    $.ajax({
        data:"action=branchTarget",
        url:"PR",
        success: function(userBranches) {
            addTargetBranchesList(userBranches);
        }
    });
}
/*function addBaseBranchesList(userBranches) {
    $.each(userBranches || [], function (sha1, branches) {
        $.each(branches || [], function (index, branch){
            $('<option value= '+ branch + '>' + branch + '</option>').appendTo($("#selectB"));
        });
    });
}
function  ajaxBaseBranchesList() {
    $.ajax({
        data:"action=branchBase",
        url:"PR",
        success: function(userBranches) {
            addBaseBranchesList(userBranches);
        }
    });
}*/


function openForm() {
    $.ajax({
        data:"action=isRemote",
        url:"PR",
        success: function(message) {
            if(message.includes("is not tracking after"))
                alert(message);
            else{
                ajaxTargetBranchesList();
                document.getElementById("myForm").style.display = "block";
            }
        }
    });
}

function closeForm() {
    document.getElementById("myForm").style.display = "none";
}
function cleanPRData() {
    $("#PRData").empty();
    $('<div class="createdBy">Created by</div>').appendTo($("#PRData"));
    $('<div class="targetBranch">Target branch</div>').appendTo($("#PRData"));
    $('<div class="baseBranch">Base branch</div>').appendTo($("#PRData"));
    $('<div class="createdDate">Created date</div>').appendTo($("#PRData"));
    $('<div class="status">Status</div>').appendTo($("#PRData"));
    $('<div class="action">Action</div>').appendTo($("#PRData"));

}
function showPRList(userPR) {
    $.each(userPR || [], function (status, list) {
        $.each(list || [], function (index, pr){
            $('<div id=' + pr.id +'> '+  pr.createdBy + '</div>').appendTo($("#PRData"));
            $('<div id=' + pr.id +'> '+  pr.targetBranch + '</div>').appendTo($("#PRData"));
            $('<div id=' + pr.id +'> '+  pr.baseBranch + '</div>').appendTo($("#PRData"));
            $('<div id=' + pr.id +'> '+  pr.dateCreated + '</div>').appendTo($("#PRData"));
            $('<div id=' + pr.id +'> '+  pr.status + '</div>').appendTo($("#PRData"));

            if (pr.status.toString()=="OPEN") {
                $('<div id=actions> '
                    + '<button id=' + pr.id + ',watch> <img src=images/watch.png></button>'
                    + '<button id=' + pr.id + ',' + pr.baseBranch + ',' + pr.targetBranch
                    + ',approve> <img src=images/v.png></button>'
                    + '<button id=' + pr.id + ',reject> <img src=images/x.png></button>'+
                    '</div>').appendTo($("#PRData"));
            }
            else
                $('<div id=actions> '
                    + '<button id=' + pr.id + ',watch> <img src=images/watch.png></button>' +
                    '</div>').appendTo($("#PRData"));

        });
    });
}
function  ajaxPRList() {
    $.ajax({
        data:"action=show",
        url:"PR",
        success: function(userPR) {
            cleanPRData();
            showPRList(userPR);
        }
    });
}

$(function() {
    $("#uploadForm").submit(function() {
        var target = this[0].value;
        var base=this[1].value
        var message=this[2].value;

        var temp1=target;
        while(temp1.includes("\\")){
            temp1=temp1.replace("\\", "!");
        }
        var temp2=base;
        while(temp2.includes("\\")){
            temp2=temp2.replace("\\", "!");
        }
        $.ajax({
            data: "target=" + temp1 + "&base=" + temp2 + "&message="+ message + "&action=create" ,
            url: this.action,
            error: function() {
                alert("fail");
            },
            success: function() {
                alert("Pull request was successfully created");
                document.getElementById('message').value = '';
                closeForm();

                //document.getElementById('targetBranch').value = '';
                //document.getElementById('baseBranch').value = '';
                //document.getElementById('message').value = '';
                //var cList = document.querySelector("#targetBranch");
                //cList.children[0].remove();
                //var cList = document.querySelector("#baseBranch");
                //cList.children[0].remove()
            }
        });
        return false;
    });

    $("#PRData").on('click','button', function () {
        var action=this.id;
        idOfCommit=action;
        if(action.includes("watch")) {
            $.ajax({
                data: "action=watch" + "&data=" + this.id,
                url: "PRActions",
                error: function () {
                    alert("error");
                },
                success: function (commits) {
                    cleanCommitsData();
                    showAllCommits(commits);
                }
            });
        }
        else{
            if(action.includes("reject")){
                $.ajax({
                    data: "action=reject" + "&data=" + this.id,
                    url: "PRActions",
                    error: function () {
                        alert("error");
                    },
                    success: function (message) {
                        alert(message);
                        cleanPRData();
                        ajaxPRList();
                    }
                });
            }
            else{
                $.ajax({
                    data: "action=approve" + "&data=" + this.id,
                    url: "PRActions",
                    error: function () {
                        alert("error");
                    },
                    success: function (message) {
                        alert(message);
                        cleanPRData();
                        ajaxPRList();
                    }
                });
            }
        }
    });

    $("#allCommits").on('click','button', function () {
        var temp = idOfCommit.indexOf(",");
        var res = idOfCommit.substr(0, temp);
            $.ajax({
                data: "action=files" + "&sha1=" + this.id + "&id=" + res,
                url: "PRActions",
                error: function () {
                    alert("error");
                },
                success: function (files) {
                    cleanCommitFilesData();
                    showAllCommitFiles(files);
                }
            });
    });

    $("#allFiles").on('click','button', function () {
            $.ajax({
                data:"action=see" + "&data=" + this.id,
                url: "PRActions",
                error: function () {
                    alert("error");
                },
                success: function (content) {
                    cleanFilesContent();
                    showFileContent(content);
                }
            });
    });
    cleanCommitsData();
    ajaxPRList();
});