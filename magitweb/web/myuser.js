var refreshRate = 1000; //milli seconds
var notificationsArr;
var name;
var creatrNewFile;

function logoutFunc() {
    $.ajax({
        url:"logout",
        success: function(message) {
            var temp=message.substring(1, 14);
            $("#userOfRepos").empty();
            window.location.href = temp;
            alert("You have successfully logged out");
        }
    });

}
function openForm() {
    document.getElementById("myForm").style.display = "block";
}

function closeForm() {
    document.getElementById("myForm").style.display = "none";
}
function refreshUsersList(users) {
    //clear all current users
    $("#users").empty();
    $.each(users || [], function(index, username) {
        console.log("Adding user #" + index + ": " + username);
        $('<p id=' + username + '>' + username + '</p>').appendTo($("#users"));
    });
}

function refreshNotesList(notes) {
    $("#note").empty();
    var temp=new Array();
    $.each(notes || [], function(index, notification) {
        $('<div id=' + index + '>' + notification + '</div>').appendTo($("#note"));
        temp.push(notification);
    });
    $.each(notificationsArr || [], function(index, notification) {
        $('<div id=' + index + '>' + notification + '</div>').appendTo($("#note"));
        temp.push(notification)
    });
    notificationsArr=temp;
}
function cleanData() {
    $("#repoData").empty();
    $("#userOfRepos").empty();
    $("#thisRepos").empty();
    $('<div class=repoName> Repository Name </div>').appendTo($("#repoData"));
    $('<div class=activeBranch>Active Branch </div>').appendTo($("#repoData"));
    $('<div class=numBranches>#Branches </div>').appendTo($("#repoData"));
    $('<div class=timeLastCommit>Last Commit</div>').appendTo($("#repoData"));
    $('<div class=messageLastCommit>Commit Massage</div>').appendTo($("#repoData"));
    $('<div class="action">Action</div>').appendTo($("#repoData"));
}
function showReposList(userRepos) {
    $.each(userRepos || [], function (index, reposname) {
        var arry=reposname;
        var todo=arry[5];
        todo=todo.split(".");
        $('<div id=' + index + 'RepoName> '+ arry[0].trim() + '</div>').appendTo($("#repoData"));
        $('<div id=' + index + 'ActiveBranch> ' + arry[1] + '</div>').appendTo($("#repoData"));
        $('<div id=' + index + 'NumBranches> '+ arry[2] + '</div>').appendTo($("#repoData"));
        $('<div id=' + index + 'TimeLastCommit> ' + arry[3] + '</div>').appendTo($("#repoData"));
        $('<div id=' + index + 'MessageLastCommit> '+ arry[4] + '</div>').appendTo($("#repoData"));
        $('<button id=' + index + '-' + arry[5] +' title=' + todo[0]+ '> <img src=images/' + arry[5] + '></button>').appendTo($("#repoData"));
    });

}
function showForkMessage(message) {
    if (message[0]=="true")
        alert("You forked successfully the repository: " + message[1]);
    else {
        $("#userOfRepos").empty();
        window.location.href = message[0];
    }
    
}
function  deleteExistRepo(massage) {
    $.ajax({
        data:"path=" + creatrNewFile.name +"&data=" + massage,
        url: "uploadXML",
        error: function() {
            alert("error");
        },
        success: function() {
            createNewRepo();
        }

    });
}
  function createNewRepo(){
      var formData = new FormData();
      formData.append("fake-key-1", creatrNewFile);
      $.ajax({
          method:'POST',
          data: formData,
          url: 'uploadXML',
          processData: false, // Don't process the files
          contentType: false, // Set content type to false as jQuery will tell the server its a query string request
          error: function() {
              alert("fail");
          },
          success: function(message) {
              alert(message);
              if(message=="The repo was successfully uploaded"){
                  cleanData();
                  ajaxRepoList()
              }
          }
      });
      return false;

}
function showXMLMessage(update){
    $.each(update || [], function (index, message) {
        var test1=index;
        var t=message;
        alert(message);
        if(message=="The repo was successfully uploaded"){
            cleanData();
            ajaxRepoList()
        }
        else{
            if(message.includes("is already exist")){
                if (confirm("Do yow want to delete and initialize new repo? (if you will press no, you will continue with the existing folder)"))
                    deleteExistRepo(message);
                else
                    alert("You choose to stay with the current repo");

                }
            }
        });
}
function ajaxUsersList() {
    $.ajax({
        url:"theUsers",
        success: function(users) {
            refreshUsersList(users);
        }
    });
}

function ajaxNotesList() {
    $.ajax({
        url:"theNotifications",
        success: function(notes) {
            refreshNotesList(notes);
        }
    });
}
function ajaxRepoList() {
    $.ajax({
        url:"theUsersRepo",
        success: function(userRepos) {
            showReposList(userRepos);
        }
    });
}



$(function() {
    $("#users").on('click', 'p', function() {
        name=this.textContent;
        $.ajax({
            data: "UserName=" + this.textContent,
            url: "theUsersRepo",
            success: function(userRepos) {
                cleanData();
                $('<div id=userOfRepos>'+ name + ' repositories</div>').appendTo($("#thisRepos"));
                showReposList(userRepos);
            }
        });
    });

    $("#repoData").on('click','button', function () {
        $.ajax({
            data:"UserName=" + name + "&repoAndAction=" + this.id,
            url: "fork",
            error: function() {
                alert("error");
            },
            success: function(message) {
                showForkMessage(message);
                }

        });
    });

    $("#uploadForm").submit(function() {
        var file1 = this[0].files[0];
        creatrNewFile=file1;
        var formData = new FormData();
        formData.append("fake-key-1", file1);

        $.ajax({
            method:'POST',
            data: formData,
            url: this.action,
            processData: false, // Don't process the files
            contentType: false, // Set content type to false as jQuery will tell the server its a query string request
            error: function() {
                alert("fail");
            },
            success: function(update) {
                showXMLMessage(update);
            }
        });
        return false;
    });
    $("#userOfRepos").empty();
    $("#thisRepos").empty();
    //The users list is refreshed automatically every second
    setInterval(ajaxUsersList, refreshRate);
    notificationsArr=new Array();
    setInterval(ajaxNotesList, refreshRate);
    ajaxRepoList();

    //The chat content is refreshed only once (using a timeout) but
    //on each call it triggers another execution of itself later (1 second later)

});