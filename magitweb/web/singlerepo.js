var refreshRate = 1000;
var notificationsArr;
function logoutFunc() {
    $.ajax({
        url:"logout",
        success: function(message) {
            var temp=message.substring(1, 14);
            window.location.href = temp;
            alert("You have successfully logged out");
        }
    });

}
function showRepoData(title) {
    $("#titles").empty();
    $.each(title || [], function(index, line) {
        $('<h1 id=' + index + '>' + line + '</h1>').appendTo($("#titles"));
    });
}

function LoadRepoTitle(){
    $.ajax({
        url:"myRepoMain",
        success: function(title) {
            showRepoData(title);
        }
    });
}
function refreshNotesList(notes) {
    $("#note").empty();
    var temp=new Array();
    $.each(notes || [], function(index, notification) {
        $('<div id=' + index + '>' + notification + '</div>').appendTo($("#note"));
        temp.push(notification);
    });
    $.each(notificationsArr || [], function(index, notification) {
        $('<div id=' + index + '>' + notification + '</div>').appendTo($("#note"));
        temp.push(notification)
    });
    notificationsArr=temp;
}
function ajaxNotesList() {
    $.ajax({
        url:"theNotifications",
        success: function(notes) {
            refreshNotesList(notes);
        }
    });
}

$(function(){
     LoadRepoTitle();
    notificationsArr=new Array();
    setInterval(ajaxNotesList, refreshRate);

  });