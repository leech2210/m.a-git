var link;
var filePath;
var refreshRate = 3000;

function logoutFunc() {
    $.ajax({
        url:"logout",
        success: function(message) {
            var temp=message.substring(1, 14);
            window.location.href = temp;
            alert("You have successfully logged out");
        }
    });

}
function create_text() {
    var x = document.getElementById("my_text").value;
    var temp=filePath;
    while(temp.includes("\\")){
        temp=temp.replace("\\", "!");
    }
    $.ajax({
        data: "action=create" + "&data=" + x + "&path=" + temp,
        url: "WC",
        error: function () {
            alert("error");
        },
        success: function (message) {
            alert(message);
            cleanWC();
            viewWC();
        }
    });

}
function showEmptyFile() {
    $("#fileContent").empty();
    $('<textarea class="text_edit" id="my_text"></textarea><br />').appendTo($("#fileContent"));
    $('<input id="saveBtn" type="button" value="Save" onclick="create_text()" />').appendTo($("#fileContent"));
    $('<div id="view_text"></div>').appendTo($("#fileContent"));
    view_text();
}

function newFile(){
    filePath = prompt("Please insert the path of the new file (relative to your current repo):");
    if (filePath == null || filePath == "")
        alert("You didn't insert path");
    else
        showEmptyFile();
}

function  showCommitsList(userCommits) {
    $.each(userCommits|| [], function (commit, listBranches) {
        var arr=commit.split(";");
        $('<div id=' + arr[0] +'>'+  arr[0] + '</div>').appendTo($("#allCommits"));
        $('<div id=' + arr[0] +'>'+  arr[1] + '</div>').appendTo($("#allCommits"));
        $('<div id=' + arr[0] +'>'+  arr[2] + '</div>').appendTo($("#allCommits"));
        $('<div id=' + arr[0] +'>'+  arr[3] + '</div>').appendTo($("#allCommits"));
        $('<div id=' + arr[0] + '> ' + listBranches + '</div>').appendTo($("#allCommits"));
        $('<button id=' + arr[0] + '> <img src=images/watch.png></button>' + '</div>').appendTo($("#allCommits"));

        });
    }
function cleanCommitsData() {
    $("#allCommits").empty();
    $('<div class="sha1">Sha-1</div>').appendTo($("#allCommits"));
    $('<div class="message">Message</div>').appendTo($("#allCommits"));
    $('<div class="creationDate">Creation date</div>').appendTo($("#allCommits"));
    $('<div class="createdBy">Created by</div>').appendTo($("#allCommits"));
    $('<div class="pointedBranches">Pointed branches</div>').appendTo($("#allCommits"));
    $('<div class="files">Files</div>').appendTo($("#allCommits"));
}

function cleanCommitFilesData(){
    $("#allFiles").empty();
}
function  ajaxCommitsList() {
    $.ajax({
        data:"action=commits",
        url:"wcActions",
        success: function(userCommits) {
            cleanCommitsData();
            showCommitsList(userCommits);
        }
    });
}

function showAllCommitFiles(files) {
    $('<div class="title">Commit files</div>').appendTo($("#allFiles"));
    $('<div class="title">Actions</div>').appendTo($("#allFiles"));
    $.each(files || [], function (index, file) {
        var temp=file;
        while(temp.includes("\\")){
            temp=temp.replace("\\", "!");
        }
        while(temp.includes(" ")){
            temp=temp.replace(" ", ";");
        }
        $('<div id=' + file + '>' + file + '<br></div>').appendTo($("#allFiles"));
        $('<button id=' + temp + ',watch> <img src=images/watch.png></button>').appendTo($("#allFiles"));

    });

}
function view_text () {
    // Find html elements.
    var textArea = document.getElementById('my_text');
    var div = document.getElementById('view_text');
    // Put the text in a variable so we can manipulate it.
    var text = textArea.value;
    // Make sure html and php tags are unusable by disabling < and >.
    text = text.replace(/\</gi, "<");
    text = text.replace(/\>/gi, ">");
    text = text.replace(/\n/gi, "<br />");
    // Print the text in the div made for it.
    //div.innerHTML = text;
}
function edit_text() {
    var x = document.getElementById("my_text").value;
    $.ajax({
        data: "action=edit" + "&data=" + x + "&path=" + link,
        url: "WC",
        error: function () {
            alert("error");
        },
        success: function (message) {
            alert(message);
        }
    });

}
function cleanFilesContent() {
    $("#fileContent").empty();

}
function cleanWC() {
    $("#WCFiles").empty();

}
function showFileContent(content) {
    $('<textarea class="text_edit" id="my_text">' + content + '</textarea><br />').appendTo($("#fileContent"));
    $('<input id="saveBtn" type="button" value="Close" onclick="cleanFilesContent()" />').appendTo($("#fileContent"));
    $('<div id="view_text"></div>').appendTo($("#fileContent"));
    view_text();
}

function showFileContentToEdit(content) {
    $('<textarea class="text_edit" id="my_text">' + content + '</textarea><br />').appendTo($("#fileContent"));
    $('<input id="saveBtn" type="button" value="Save" onclick="edit_text()" />').appendTo($("#fileContent"));
    $('<input id="closeBtn" type="button" value="Close" onclick="cleanFilesContent()" />').appendTo($("#fileContent"));
    $('<div id="view_text"></div>').appendTo($("#fileContent"));
    view_text();
}

function  doCommit(){
    var commitM= prompt("Please insert the commit message:");
    $.ajax({
        data:"action=commit"+ "&data=" + commitM,
        url:"WC",
        success: function(message) {
            alert(message);
            viewWC();
        }
    });


}
function Commit() {
    $.ajax({
        data:"action=openChanges",
        url:"WC",
        success: function(message) {
            if(message=="yes")
                doCommit();
            else
                alert(message);
        }
    });
}
function Status() {
    cleanFilesContent();
    $.ajax({
        data:"action=status",
        url:"WC",
        success: function(content) {
            showFileContent(content);
        }
    });

}
function showWCList(files) {
    $('<button onclick="Commit()">Commit</button>').appendTo($("#wcButtons"));
    $('<button onclick="newFile()"><img src=images/new.png></button>').appendTo($("#wcButtons"));
    $('<button onclick="Status()">Status</button>').appendTo($("#wcButtons"));
    $('<div class="title">Commit files</div>').appendTo($("#WCFiles"));
    $('<div class="title">Actions</div>').appendTo($("#WCFiles"));
    $.each(files || [], function (index, file) {
        var temp=file;
        while(temp.includes("\\")){
            temp=temp.replace("\\", "!");
        }
        while(temp.includes(" ")){
            temp=temp.replace(" ", ";");
        }
        $('<div id=' + file + '>' + file + '<br></div>').appendTo($("#WCFiles"));
        $('<div id=actions> '
            + '<button id=' + temp + ',watch> <img src=images/watch.png></button>'
            + '<button id=' + temp + ',edit> <img src=images/edit.png></button>'
            + '<button id=' + temp + ',delete> <img src=images/remove.png></button>'+
            '</div>').appendTo($("#WCFiles"));
    });

}
function viewWC(){
    $("#allCommits").empty();
    $("#wcButtons").empty();
    $("#WCFiles").empty();
    $("#fileContent").empty();
    cleanCommitFilesData();
    $.ajax({
        data:"action=myCommit",
        url:"WC",
        success: function(files) {
            //cleanCommitsData();
            showWCList(files);
        }
    });

}


$(function() {
    $("#allFiles").on('click','button', function () {
            $.ajax({
                data: "action=watch" + "&data=" + this.id,
                url: "WC",
                error: function () {
                    alert("error");
                },
                success: function (content) {
                    cleanFilesContent();
                    showFileContent(content);
                }
            });
        });
    $("#allCommits").on('click','button', function () {
            $.ajax({
                data: "action=files" + "&data=" + this.id,
                url: "wcActions",
                error: function () {
                    alert("error");
                },
                success: function (files) {
                    cleanCommitFilesData();
                    showAllCommitFiles(files);
                }
            });
    });
    $("#WCFiles").on('click','button', function () {
        var action=this.id;
        idOfCommit=action;
        if(action.includes("watch")) {
            $.ajax({
                data: "action=watch" + "&data=" + this.id,
                url: "WC",
                error: function () {
                    alert("error");
                },
                success: function (content) {
                    cleanFilesContent();
                    showFileContent(content);
                }
            });
        }
        else{
            if(action.includes("delete")){
                $.ajax({
                    data: "action=delete" + "&data=" + this.id,
                    url: "WC",
                    error: function () {
                        alert("error");
                    },
                    success: function (message) {
                        alert(message);
                        cleanWC();
                        viewWC();
                    }
                });
            }

            else{
                if (action.includes("edit")){
                    link=this.id;
                    $.ajax({
                        data: "action=watch" + "&data=" + this.id,
                        url: "WC",
                        error: function () {
                            alert("error");
                        },
                        success: function (content) {
                            cleanFilesContent();
                            showFileContentToEdit(content);
                        }
                    });
                }
            }
        }
    });

    ajaxCommitsList();
});

