function logoutFunc() {
    $.ajax({
        url:"logout",
        success: function(message) {
            var temp=message.substring(1, 14);
            window.location.href = temp;
            alert("You have successfully logged out");
        }
    });

}
function  typeBranch(commitSha1, branchName, type){
    $.ajax({
        data:"action=type&sha1=" + commitSha1 + "&name="+ branchName + "&type="+ type ,
        url:"createBranch",
        success: function(message) {
                alert(message);
            cleanData();
            ajaxBranchesList();
        }
    });
}
function  createNewBranch(commitSha1, branchName){
    $.ajax({
        data:"action=create&sha1=" + commitSha1 + "&name="+ branchName ,
        url:"createBranch",
        success: function(message) {
            if (message.includes("Branch added")) {
                alert(message);
                cleanData();
                ajaxBranchesList();
            }
            else{
                if (confirm(message)) {
                    typeBranch(commitSha1, branchName, "1")
                } else {
                    typeBranch(commitSha1, branchName, "2")
                }

            }
        }
    });
}


function myFunction() {
    var branchName = prompt("Please insert the branch name:");
    if (branchName == null || branchName == "") {
        alert("You didn't insert branch name to create");
    } else {
        var commitSha1 = prompt("Please insert the commit sha1:");
        if (commitSha1 == null || commitSha1 == "")
            alert("You didn't insert the commit sha1");
        else
        createNewBranch(commitSha1, branchName);
    }

}
function showBranchesList(userBranches) {
    var name;
    $.each(userBranches || [], function (sha1, branches) {
        $.each(branches || [], function (index, branch){
        $('<div id=' + branch + ',' + sha1 +'> '+ branch + '</div>').appendTo($("#branchesData"));
        $('<div id=' + branch + ',' + sha1 +'> '+ sha1 + '</div>').appendTo($("#branchesData"));
            name = branch;
        if (branch.includes("\\"))
            name = name.replace("\\", ".");

        if (branch.includes(" "))
            name=name.replace(" ", "!");

        $('<div id=actions> '
                + '<button id=' + name + '-checkout> <img src=images/checkout.png></button>'
                + '<button id=' + name + '-delete> <img src=images/delete.png></button>'+
            '</div>').appendTo($("#branchesData"));
    });
     });
}
function cleanData() {
    $("#branchesData").empty();
    $('<div class="branchName">Branch Name</div>').appendTo($("#branchesData"));
    $('<div class="activeBranch">Sha-1</div>').appendTo($("#branchesData"));
    $('<div class="action">Checkout/Delete</div>').appendTo($("#branchesData"));

}

function showHeadBranch(branch) {
    $("#hBranch").empty();
    $('<label id=headBranch> Head branch: ' + branch + '</label>').appendTo($("#hBranch"));
}
function  ajaxHeadBranch() {
    $.ajax({
        data:"action=headBranch",
        url:"branches",
        success: function(branch) {
            showHeadBranch(branch);
        }
    });
}
function  ajaxBranchesList() {
    $.ajax({
        data:"action=show",
        url:"branches",
        success: function(userBranches) {
            showBranchesList(userBranches);
        }
    });
}

function showActionMessage(message) {
    if(message.includes("Branch switched to")){
        alert(message);
        ajaxHeadBranch();
    }
    else {
        if (message.includes("Branch deleted")) {
            alert(message);
            cleanData();
            ajaxBranchesList();
        } else
            alert(message);
    }
}
$(function() {
    $("#branchesData").on('click','button', function () {
        $.ajax({
            data:"action=" + this.id,
            url: "branches",
            error: function() {
                alert("error");
            },
            success: function(message) {
                showActionMessage(message);
            }

        });
    });
    ajaxHeadBranch();
    ajaxBranchesList();
});