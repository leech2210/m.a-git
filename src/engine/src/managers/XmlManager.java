package managers;

import exceptions.PathNotExistsException;
import exceptions.XmlFileExceptions;
import generated.*;
import models.Commit;
import models.Folder;
import models.GitObject;
import models.Repository;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.collections4.list.TreeList;
import utils.FilesHandler;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static utils.Consts.*;

public class XmlManager {

    private final static String packageName = "generated";
    private Repository repository;
    private String headBranchName;
    private String remotePath;
    private MagitCommits mCommits;
    private MagitFolders mFolders;
    private MagitBlobs mBlobs;
    private MagitBranches mBranches;
    //private MagitRepository test;
    private MagitRepository.MagitRemoteReference mRemote;
    private Map<String, MagitBlob> blobsById = new HashMap<>();
    private Map<String, MagitSingleFolder> nonRootfoldersById = new HashMap<>();
    private Map<String, MagitSingleFolder> rootfoldersById = new HashMap<>();
    private Map<String, MagitSingleCommit> commitsById = new HashMap<>();
    private Map<String, MagitSingleBranch> branchesByName = new HashMap<>();
    private Map<String, MagitSingleBranch> rbByName = new HashMap<>();
    private Map<String, MagitSingleBranch> rtbByName = new HashMap<>();
    private Map<String, String> foldersSha1 = new HashMap<>();
    private Map<String, String> commitsSha1 = new HashMap<>();


    public XmlManager(String xmlFilePath) throws JAXBException {
        File file = new File(xmlFilePath);
        JAXBContext jc = JAXBContext.newInstance(packageName);
        Unmarshaller u = jc.createUnmarshaller();
        MagitRepository mRepository = (MagitRepository) u.unmarshal(file);
        mCommits = mRepository.getMagitCommits();
        mFolders = mRepository.getMagitFolders();
        mBlobs = mRepository.getMagitBlobs();
        mBranches = mRepository.getMagitBranches();
        mRemote=mRepository.getMagitRemoteReference();

        String repoPath = mRepository.getLocation();
        String repoName = mRepository.getName();
        if (mRemote!=null) {
            if (mRemote.getLocation() != null) {
                remotePath = mRemote.getLocation();
            }
        }
        else
            remotePath="";
        headBranchName = mBranches.getHead();
        repository = new Repository(repoName, repoPath, remotePath);


    }
    public XmlManager(InputStream inStream) throws JAXBException {
        JAXBContext jc = JAXBContext.newInstance(packageName);
        Unmarshaller u = jc.createUnmarshaller();
        try {
            inStream.reset();
        } catch (IOException e) {
            e.printStackTrace();
        }
        MagitRepository mRepository = (MagitRepository) u.unmarshal(inStream);
        mCommits = mRepository.getMagitCommits();
        mFolders = mRepository.getMagitFolders();
        mBlobs = mRepository.getMagitBlobs();
        mBranches = mRepository.getMagitBranches();
        mRemote=mRepository.getMagitRemoteReference();

        String repoPath = mRepository.getLocation();
        String repoName = mRepository.getName();
        if (mRemote!=null) {
            if (mRemote.getLocation() != null) {
                remotePath = mRemote.getLocation();
            }
        }
        else
            remotePath="";
        headBranchName = mBranches.getHead();
        repository = new Repository(repoName, repoPath, remotePath);


    }

    public void creatMaps () throws XmlFileExceptions {
        List<MagitBlob> allMagitBlobs = mBlobs.getMagitBlob();
        for (MagitBlob tempBlob : allMagitBlobs) {
            if (blobsById.containsKey(tempBlob.getId()))
                throw new XmlFileExceptions("There is more then 1 blob with the same id");
            else {
                blobsById.put(tempBlob.getId(), tempBlob);
            }
        }
        List<MagitSingleFolder> allMagitFolders = mFolders.getMagitSingleFolder();
        for (MagitSingleFolder tempFolder : allMagitFolders) {
            if (rootfoldersById.containsKey(tempFolder.getId()) ||
                nonRootfoldersById.containsKey(tempFolder.getItems()))
                throw new XmlFileExceptions("There is more then 1 folder with the same id");
            if (tempFolder.isIsRoot())
                rootfoldersById.put(tempFolder.getId(), tempFolder);
            else {
                nonRootfoldersById.put(tempFolder.getId(), tempFolder);
            }
        }

        List<MagitSingleCommit> allMagitCommits = mCommits.getMagitSingleCommit();
        for (MagitSingleCommit tempCommit : allMagitCommits) {
            if (!rootfoldersById.containsKey((tempCommit.getRootFolder().getId())))
                throw new XmlFileExceptions("There is no root folder with the id: " + tempCommit.getRootFolder().getId());
            else {
                if (commitsById.containsKey(tempCommit.getId()))
                    throw new XmlFileExceptions("There is more then 1 commit with the same id");
                else{
                    commitsById.put(tempCommit.getId(), tempCommit);
                }
            }
        }
        List <MagitSingleBranch> allMagitBranches = mBranches.getMagitSingleBranch();
        for (MagitSingleBranch tempBranch : allMagitBranches) {
            if (tempBranch.isIsRemote() == true) {
                rbByName.put(tempBranch.getName(), tempBranch);
            }
            else {
                if (tempBranch.isTracking() == true)
                    rtbByName.put(tempBranch.getName(), tempBranch);
                }
            if (!tempBranch.getPointedCommit().getId().equals("")) {
                        if (!commitsById.containsKey(tempBranch.getPointedCommit().getId()))
                            throw new XmlFileExceptions("There is no branch that point to commit with the id: " + tempBranch.getPointedCommit().getId());
                    }
                    branchesByName.put(tempBranch.getName(), tempBranch);
                }
            }

    public void foldersValidation(Map<String, MagitSingleFolder> folderMap) throws XmlFileExceptions {
        for (MagitSingleFolder tempFolder :folderMap.values()){
            List<Item> listFiles = tempFolder.getItems().getItem();
            for (Item tempItem : listFiles) {
                if (tempItem.getType().equals("blob")) {
                    if (blobsById.get(tempItem.getId()) == null)
                        throw new XmlFileExceptions("There is no file with id: " + tempItem.getId());
                }else{
                    MagitSingleFolder theFolder = nonRootfoldersById.get(tempItem.getId());
                    if(theFolder == null)
                        theFolder = rootfoldersById.get(tempItem.getId());
                    if (theFolder == null)
                        throw new XmlFileExceptions("There is no folder with id: " + tempItem.getId());
                    if (theFolder.getId().equals(tempFolder.getId()))
                        throw new XmlFileExceptions("The folder: "+ tempItem.getId()+ " cannot be self-contained");
                }
            }
        }
    }

    public void branchValidation() throws XmlFileExceptions {
        if (!branchesByName.containsKey(headBranchName)) {
            throw new XmlFileExceptions("The branch: " + headBranchName + " is not exist");
        }

        for (MagitSingleBranch tempBranch: rtbByName.values()){
                String trackingAfter=tempBranch.getTrackingAfter();
                if (rbByName.get(trackingAfter).isIsRemote()==false){
                    throw new XmlFileExceptions("The branch:" + tempBranch.getName() +"is tracking after a branch that is not defined as a remote branch.");
                }
            }
        }


    public void remoteValidation()  throws XmlFileExceptions{
        if (mRemote!=null) {
            if (mRemote.getLocation() != null) {
                remotePath = mRemote.getLocation();
            }
        }
        if(mRemote!=null) {
            if (mRemote.getLocation() != null) {
                String location = mRemote.getLocation() + GIT_FOLDER;
                if (!FilesHandler.isDirectory(location)) {
                    throw new XmlFileExceptions("There is no repository in the location: " + location);
                }
            }
        }
    }

    public void isXmlValid() throws XmlFileExceptions {
        creatMaps();
        foldersValidation(rootfoldersById);
        foldersValidation(nonRootfoldersById);
        branchValidation();
        remoteValidation();
    }
    private Folder creatFolderObjects(String dirPath, String userName,
                                      String commitTime, String activeRepo, MagitSingleFolder tempFolder)
            throws IOException, PathNotExistsException {
        TreeList<GitObject> result = new TreeList<>();
        List<Item> listFiles = tempFolder.getItems().getItem();
        String folderId = "";
        for (Item tempItem : listFiles) {
            if (tempItem.getType().equals("blob")) {
                MagitBlob blob = blobsById.get(tempItem.getId());
                String content = blob.getContent();
                String sha1 = DigestUtils.sha1Hex(content);
                FilesHandler.writeToFile(activeRepo + OBJECTS_FOLDER + "\\" + sha1, content, true);
                result.add(new models.File(dirPath + "\\" + blob.getName(), blob.getLastUpdater(), sha1, blob.getLastUpdateDate()));
            } else if (tempItem.getType().equals("folder")) {
                MagitSingleFolder theFolder = nonRootfoldersById.get(tempItem.getId());
                if(theFolder == null)
                    theFolder = rootfoldersById.get(tempItem.getId());
                Folder folder = creatFolderObjects(dirPath + "\\" + theFolder.getName(), theFolder.getLastUpdater(), theFolder.getLastUpdateDate(), activeRepo, theFolder);
                folderId = tempFolder.getId();
                result.add(folder);
            }
        }
        String folderMetaFile = GitObject.createMetadata(result);
        String sha1 = DigestUtils.sha1Hex(folderMetaFile);
        foldersSha1.put(folderId,sha1);

        FilesHandler.writeToFile(activeRepo + OBJECTS_FOLDER + "\\" + sha1, folderMetaFile, true);
        return new Folder(dirPath, userName, sha1, commitTime, result);
    }

    public void creatBranches() throws IOException, PathNotExistsException {

        for (MagitSingleBranch tempBranch: branchesByName.values()){
            if (!tempBranch.getName().equals(headBranchName)) {
                if(rbByName.containsKey(tempBranch.getName())){
                    FilesHandler.writeToFile(repository.getLocation() + BRANCHES_FOLDER + "\\"
                            + tempBranch.getName() + ".txt", "", false);
                }
                else{
                    FilesHandler.writeToFile(repository.getLocation() + BRANCHES_FOLDER + "\\"
                            + tempBranch.getName() + ".txt", "", false);
                }
            }
        }
    }
    public void UpdateRemote() throws IOException, PathNotExistsException {
        FilesHandler.writeToFile(repository.getLocation() + REMOTE_PATH_FILE
                , remotePath, false);
    }

    public void updateBranchCommit() throws IOException, PathNotExistsException {
            String commitId = branchesByName.get(headBranchName).getPointedCommit().getId();
            if (!commitId.equals("")) {
                MagitSingleCommit commitFile = commitsById.get(commitId);
                parseMetadataCommitFile(commitFile, repository.getLocation());
            }
            for (MagitSingleBranch tempBranch: branchesByName.values()) {
                if (commitsSha1.get(tempBranch.getPointedCommit().getId()) == null) {
                    commitId = tempBranch.getPointedCommit().getId();
                    if (!commitId.equals("")) {
                        MagitSingleCommit commitFile = commitsById.get(commitId);
                        parseMetadataCommitFile(commitFile, repository.getLocation());
                    }
                }
            }
    }

    private Commit parseMetadataCommitFile(MagitSingleCommit content, String repo)
            throws IOException, PathNotExistsException {
        Commit prev = null;
        String repoPath = repository.getLocation();
        boolean isPrev = false;
        String commitMessage = content.getMessage();
        String date = content.getDateOfCreation();
        String user = content.getAuthor();
        String prevCommitId;

        if (content.getPrecedingCommits() != null) {
            if (content.getPrecedingCommits().getPrecedingCommit().size()!= 0) {
                prevCommitId = content.getPrecedingCommits().getPrecedingCommit().get(0).getId();
                MagitSingleCommit commitFile = commitsById.get(prevCommitId);
                prev = parseMetadataCommitFile(commitFile, repoPath);
                isPrev = true;
            }
        }
        String rootId = content.getRootFolder().getId();
        creatFolderObjects(repoPath, user, date, repoPath, rootfoldersById.get(rootId));
        String rootSha1 = foldersSha1.get(rootId);
        if (isPrev) {
            String currentContent = createMetadataCommitFile(commitMessage, date, prev.getSha1(), rootSha1, user);
            String currentsha1 = DigestUtils.sha1Hex(currentContent);
            FilesHandler.writeToFile(repo + OBJECTS_FOLDER + "\\" + currentsha1, currentContent, true);
            updateBranchesCommit(content.getId(), currentsha1, repo);
            commitsSha1.put(content.getId(),currentsha1);
            return new Commit(commitMessage, user, currentsha1, date, rootSha1, prev);
        } else{
            String currentContent = createMetadataCommitFile(commitMessage, date, "", rootSha1, user);
            String currentsha1 = DigestUtils.sha1Hex(currentContent);
            FilesHandler.writeToFile(repo + OBJECTS_FOLDER + "\\" + currentsha1, currentContent, true);
            updateBranchesCommit(content.getId(),currentsha1,repo);
            commitsSha1.put(content.getId(),currentsha1);
            return new Commit(commitMessage, user, currentsha1, date, rootSha1, null);
        }
    }

    private String createMetadataCommitFile(String commitMessage, String date, String prevCommit,
                                            String rootSha1, String activeUser){
        return String.join("\n", commitMessage, date, activeUser, rootSha1, prevCommit);
    }

    private void updateBranchesCommit(String rootId, String sha1, String repo)
            throws IOException, PathNotExistsException {
        for (MagitSingleBranch tempBranch : branchesByName.values()) {
            if (tempBranch.getPointedCommit().getId().equals(rootId)) {
                if (rtbByName.containsKey(tempBranch.getName())) {
                    String tracking = tempBranch.getTrackingAfter();
                    FilesHandler.writeToFile(repo + BRANCHES_FOLDER + "\\" + tempBranch.getName()
                                + ".txt", sha1 + ",tracking=" + tracking, false);
                }
                else {
                    FilesHandler.writeToFile(repo + BRANCHES_FOLDER + "\\" + tempBranch.getName()
                            + ".txt", sha1, false);
                }
                }
            }
        }




    public Repository getRepository() {
        return repository;
    }

    public String getHeadBranchName() {
        return headBranchName;
    }


}


