package managers;

import exceptions.PathNotExistsException;
import exceptions.InvalidMagitRepoExceptions;
import models.Repository;
import utils.FilesHandler;

import java.io.IOException;

import static utils.Consts.*;

public class RepoManager {

    private Repository repository;

    public void initRepo(Repository repo, String headBranch)
            throws IOException, PathNotExistsException {
        setActiveRepo(repo);

        String activeRepo = repo.getLocation();
        FilesHandler.createDirectory(activeRepo + OBJECTS_FOLDER);
        FilesHandler.createDirectory(activeRepo + BRANCHES_FOLDER);
        FilesHandler.writeToFile(activeRepo + HEAD_BRANCH_FILE, headBranch, false);


        String filePath = activeRepo + BRANCHES_FOLDER + "\\" + headBranch + ".txt";
        if (!FilesHandler.isFile(filePath))
            FilesHandler.writeToFile(filePath, "", false);
    }

    public void changeRepo(String repoPath)
            throws InvalidMagitRepoExceptions, PathNotExistsException, IOException {
        if (!FilesHandler.isDirectory(repoPath))
            throw new PathNotExistsException();

        if (!FilesHandler.isDirectory(repoPath + GIT_FOLDER))
            throw new InvalidMagitRepoExceptions();

        getExistRepoName(repoPath);
    }

    private void getExistRepoName(String repoPath)
            throws PathNotExistsException, IOException, InvalidMagitRepoExceptions {
        String repoNameFilePath = repoPath + REPO_NAME_FILE;
        if (!FilesHandler.isFile(repoNameFilePath))
            throw new InvalidMagitRepoExceptions();
        String repoName = FilesHandler.readFile(repoPath + REPO_NAME_FILE);

        String remoteLocation = FilesHandler.readFile(repoPath + REMOTE_PATH_FILE);
        this.repository = new Repository(repoName, repoPath, remoteLocation);
    }

    private void setActiveRepo(Repository repo)
            throws IOException, PathNotExistsException {
        FilesHandler.writeToFile(repo.getLocation() + REPO_NAME_FILE, repo.getName(), false);
        this.repository = repo;
    }

    public Repository getRepository() {
        return repository;
    }
}
