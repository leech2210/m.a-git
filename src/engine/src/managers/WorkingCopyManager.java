package managers;

import enums.FileType;
import exceptions.PathNotExistsException;
import models.*;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.collections4.list.TreeList;
import puk.team.course.magit.ancestor.finder.AncestorFinder;
import puk.team.course.magit.ancestor.finder.CommitRepresentative;
import utils.Consts;
import utils.FilesHandler;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static utils.Consts.*;

public class WorkingCopyManager {

    private Commit currCommit;
    private Folder rootFolder;
    private String activeRepo;

    private SimpleDateFormat dateFormat;

    public WorkingCopyManager(){
        dateFormat = new SimpleDateFormat(Consts.DATE_FORMAT);
    }

    public Commit getCurrCommit() {
        return currCommit;
    }

    public void updateCurrCommit() throws PathNotExistsException, IOException {
        String headBranchName = FilesHandler.readFile(activeRepo + HEAD_BRANCH_FILE);
        String currCommitSha1 = FilesHandler.readFile(activeRepo + BRANCHES_FOLDER + "\\" + headBranchName + ".txt");
        String onlySha1 = parseMetadataCommitSha1(currCommitSha1);
        if (!currCommitSha1.isEmpty()){
            String commitFile = FilesHandler.readFile(activeRepo + OBJECTS_FOLDER + "\\" + onlySha1 + ".zip");
            currCommit = parseMetadataCommitFile(commitFile,onlySha1);
        }
    }

    public Commit getBranchCommit(String branch) throws IOException, PathNotExistsException {
        String currCommitSha1 = FilesHandler.readFile(activeRepo + BRANCHES_FOLDER + "\\" + branch + ".txt");
        String onlySha1=parseMetadataCommitSha1(currCommitSha1);
        if (!currCommitSha1.isEmpty()){
            String commitFile = FilesHandler.readFile(activeRepo + OBJECTS_FOLDER + "\\" + onlySha1 + ".zip");
            return parseMetadataCommitFile(commitFile,onlySha1);
        }

        return null;
    }


    public Commit getRemoteBranchCommit (String branchPath,String branchName)  throws IOException, PathNotExistsException {

        String currCommitSha1 = FilesHandler.readFile(branchPath + BRANCHES_FOLDER + "\\" + branchName + ".txt");
        if (!currCommitSha1.isEmpty()){
            String commitFile = FilesHandler.readFile(branchPath + OBJECTS_FOLDER + "\\" + currCommitSha1 + ".zip");
            return parseMetadataCommitFile(commitFile,currCommitSha1);
        }

        return null;
    }



    public CommitRepresentative getCommitRep(String sha1) {
        try {
            String content = FilesHandler.readFile(activeRepo + OBJECTS_FOLDER + "\\" + sha1 + ".zip");
            return parseMetadataCommitFile(content, sha1);
        } catch (PathNotExistsException | IOException e){
            return null;
        }
    }
    private String parseMetadataCommitSha1(String content) {
        String[] split = content.split(",");
        return split[0];
    }

    public String parseMetadataTrackingBranch(String content) {
        String[] split = content.split(",");
        if(split.length==2) {
            return split[1];
        }
        else
            return "";

    }



    public Commit parseMetadataCommitFile(String content, String sha1)
            throws PathNotExistsException, IOException {
        String[] split = content.split("\n");
        String commitMessage = split[0];
        String date = split[1];
        String user = split[2];
        String rootSha1 = split[3];
        if (split.length == 5){
            String prevCommit = split[4];
            String commitFile = FilesHandler.readFile(activeRepo + OBJECTS_FOLDER + "\\" + prevCommit + ".zip");
            Commit prev = parseMetadataCommitFile(commitFile, prevCommit);
            return new Commit(commitMessage, user, sha1, date, rootSha1, prev);
        }
        else if (split.length == 6){
            String firstFile = FilesHandler.readFile(activeRepo + OBJECTS_FOLDER + "\\" + split[4] + ".zip");
            String secondFile = FilesHandler.readFile(activeRepo + OBJECTS_FOLDER + "\\" + split[5] + ".zip");
            Commit first = parseMetadataCommitFile(firstFile, split[4]);
            Commit second = parseMetadataCommitFile(secondFile, split[5]);
            return new Commit(commitMessage, user, sha1, date, rootSha1, first, second);
        }
        else
            return new Commit(commitMessage, user, sha1, date, rootSha1, null);
    }

    public String commit(String commitMessage, String activeUser, String activeBranch,
                         Commit firstParent, Commit secondParent)
            throws PathNotExistsException, IOException {
        String now = dateFormat.format(new Date());

        Changes changes = ChangesManager.getChanges(activeRepo, rootFolder);

        // if nothing changed - there is nothing to commit
        if (changes.DeletedFiles.size() == 0
            && changes.UpdatedFiles.size() == 0
            && changes.CreatedFiles.size() == 0)
            return null;

        // commit open changes - update rootFolder
        commitAllOpenChanges(activeUser, now);

        // create commit file
        String first = firstParent != null ? firstParent.getSha1() : "";
        String second = secondParent != null ? secondParent.getSha1() : "";
        String rootSha1 = rootFolder.getSha1();
        String content = createMetadataCommitFile(commitMessage, now, first, second, rootSha1, activeUser);
        String sha1 = DigestUtils.sha1Hex(content);
        FilesHandler.writeToFile(activeRepo + OBJECTS_FOLDER + "\\" + sha1, content, true);
        currCommit = new Commit(commitMessage, activeUser, sha1, now, rootSha1,
                                firstParent, secondParent);

        // change the head branch file
        String oldContent=FilesHandler.readFile(activeRepo + BRANCHES_FOLDER + "\\" + activeBranch + ".txt");
        String tracking=parseMetadataTrackingBranch(oldContent);
        if (!tracking.equals("")) {
            FilesHandler.writeToFile(activeRepo + BRANCHES_FOLDER + "\\" + activeBranch + ".txt",
                    sha1 + "," + tracking, false);
        }
        else
            FilesHandler.writeToFile(activeRepo + BRANCHES_FOLDER + "\\" + activeBranch + ".txt",
                    sha1 , false);


        return sha1;

    }

    public String getAncestor(String mergedSha1){
        AncestorFinder ancestorFinder = new AncestorFinder((s) -> getCommitRep(s));

        // find ancestor
        return ancestorFinder.traceAncestor(currCommit.getSha1(), mergedSha1);
    }

    public List<Conflict> getConflicts(String mergedSha1)
            throws IOException, PathNotExistsException {
        // get ancestor sha1
        String ancestorCommitSha1 = getAncestor(mergedSha1);

        // get files from 3 commits
        List<GitObject> oursFiles = getCommitFiles(currCommit.getSha1());
        List<GitObject> theirsFiles = getCommitFiles(mergedSha1);
        List<GitObject> ancestorFiles = getCommitFiles(ancestorCommitSha1);

        MergeHandler handler = new MergeHandler(activeRepo);
        return handler.merge(oursFiles, theirsFiles, ancestorFiles);
    }

    private String createMetadataCommitFile(String commitMessage, String date, String prevCommit, String secondPrev,
                                            String rootSha1, String activeUser){
        return String.join("\n", commitMessage, date, activeUser, rootSha1, prevCommit, secondPrev);
    }

    private void commitAllOpenChanges(String userName, String commitTime)
            throws IOException, PathNotExistsException {
        if (rootFolder == null || rootFolder.files == null || rootFolder.files.size() == 0){
            rootFolder = createFirstCommit(activeRepo, userName, commitTime);
        }
        else{
            rootFolder = updateCommittedFiles(rootFolder, userName, commitTime);
        }
    }

    private Folder updateCommittedFiles(Folder root, String userName, String commitTime)
            throws PathNotExistsException, IOException {
        if (FilesHandler.isDirectory(root.getFullName())){
            List<String> existFiles = FilesHandler.listFilesAndDirs(root.getFullName());
            TreeList<GitObject> newFiles = new TreeList<>();
            for (GitObject f: root.files) {
                if (f.getType() == FileType.FILE){
                    if (existFiles.contains(f.getFullName())){ // handle deleted files
                        String content = FilesHandler.readFile(f.getFullName());
                        updateGitObject(f, content, commitTime, userName);
                        newFiles.add(f);
                    }
                }
                else{
                    Folder folder = updateCommittedFiles((Folder)f, userName, commitTime);
                    if (folder != null)
                        newFiles.add(folder);
                }

                existFiles.remove(f.getFullName());
            }

            if (newFiles.size() > 0){
                root.files = newFiles;

                // iterate throw all the existing files to create them
                for (String path: existFiles) {
                    if (FilesHandler.isFile(path)){
                        String content = FilesHandler.readFile(path);
                        String sha1 = DigestUtils.sha1Hex(content);
                        FilesHandler.writeToFile(activeRepo + OBJECTS_FOLDER + "\\" + sha1, content, true);
                        root.files.add(new File(path, userName, sha1, commitTime));
                    }
                    else {
                        Folder newFol = createFirstCommit(path, userName, commitTime);
                        root.files.add(newFol);
                    }
                }

                String folderMetaFile = GitObject.createMetadata(root.files);
                updateGitObject(root, folderMetaFile, commitTime, userName);

                return root;
            }
        }

        return null;
    }

    private void updateGitObject(GitObject gitObject, String content, String date, String activeUser)
            throws IOException, PathNotExistsException {
        String newSha1 = DigestUtils.sha1Hex(content);
        if (!newSha1.equals(gitObject.getSha1())){
            FilesHandler.writeToFile(activeRepo + OBJECTS_FOLDER + "\\" + newSha1, content, true);
            gitObject.setSha1(newSha1);
            gitObject.setDateUpdated(date);
            gitObject.setUserName(activeUser);
        }
    }

    private Folder createFirstCommit(String dirPath, String userName, String commitTime)
            throws PathNotExistsException, IOException {
        TreeList<GitObject> result = new TreeList<>();
        List<String> listFiles = FilesHandler.listFilesAndDirs(dirPath);
        for (String path: listFiles) {
            if (FilesHandler.isFile(path)){
                String content = FilesHandler.readFile(path);
                String sha1 = DigestUtils.sha1Hex(content);
                FilesHandler.writeToFile(activeRepo + OBJECTS_FOLDER + "\\" + sha1, content, true);
                result.add(new File(path, userName, sha1, commitTime));
            }
            else if (FilesHandler.isDirectory(path)){
                Folder folder = createFirstCommit(path, userName, commitTime);
                result.add(folder);
            }
        }

        String folderMetaFile = GitObject.createMetadata(result);
        String sha1 = DigestUtils.sha1Hex(folderMetaFile);
        FilesHandler.writeToFile(activeRepo + OBJECTS_FOLDER + "\\" + sha1, folderMetaFile, true);
        return new Folder(dirPath, userName, sha1, commitTime, result);
    }

    public void  deployFiles() throws IOException, PathNotExistsException {
        if (rootFolder != null){
            FilesHandler.deleteDirectory(activeRepo);
            deployFilesRec(rootFolder);
        }
    }

    private void deployFilesRec(Folder folder) throws IOException, PathNotExistsException {
        for (GitObject gitObject: folder.files) {
            if (gitObject.getType() == FileType.FILE){
                String content = FilesHandler.readFile(activeRepo + OBJECTS_FOLDER + "\\" + gitObject.getSha1() + ".zip");
                FilesHandler.writeToFile(gitObject.getFullName(), content, false);
            }
            else if (gitObject.getType() == FileType.FOLDER){
                FilesHandler.createDirectory(gitObject.getFullName());
                deployFilesRec((Folder) gitObject);
            }
        }
    }

    public Folder createCommitRootFolder(String currRootSha1, String userName, String date)
            throws IOException, PathNotExistsException {
        return createCommitRootFolder(currRootSha1, userName, date, activeRepo);
    }

    public Folder createCommitRootFolder(String currRootSha1, String userName, String date, String repoPath)
            throws IOException, PathNotExistsException {
        List<String> rootData = getFileLines(currRootSha1);
        Folder root = new Folder(repoPath, userName, currRootSha1, date);
        getAllFiles(rootData, root);

        return root;
    }

    public List<GitObject> getCommitFiles(String commitSha1) throws IOException, PathNotExistsException {
        Folder root = getRootFolderByCommit(commitSha1);
        if (root == null)
            return null;
        return root.getFiles(true);
    }

    private List<String> getFileLines(String sha1)
            throws IOException, PathNotExistsException {
        String filePath = activeRepo + OBJECTS_FOLDER + "\\" + sha1 + ".zip";
        return FilesHandler.readAllLines(filePath);
    }

    private void getAllFiles(List<String> rootData, Folder root)
            throws IOException, PathNotExistsException {
        String fullPath = root.getFullName();
        for (String line: rootData) {
            // extract data from line
            String[] split = line.split(",");
            String name = split[0];
            String sha1 = split[1];
            FileType fileType = FileType.valueOf(split[2].toUpperCase());
            String user = split[3];
            String date = split[4];

            if (fileType == FileType.FILE){
                File currFile = new File(fullPath + "\\" + name, user, sha1, date);
                root.files.add(currFile);
            }
            else {
                Folder currFolder = new Folder(fullPath + "\\" + name, user, sha1, date);
                List<String> lines = getFileLines(sha1);
                root.files.add(currFolder);
                getAllFiles(lines, currFolder);
            }
        }
    }

    public boolean isCommitExists(String commitSha1){
        return FilesHandler.isFile(activeRepo + OBJECTS_FOLDER + "\\" + commitSha1 + ".zip");
    }

    public void updateRootFolder() throws PathNotExistsException, IOException {
        String headBranchName = FilesHandler.readFile(activeRepo + HEAD_BRANCH_FILE);
        String currCommitSha1 = FilesHandler.readFile(activeRepo + BRANCHES_FOLDER +
                                                        "\\" + headBranchName + ".txt");
        String onlySha1=parseMetadataCommitSha1(currCommitSha1);

        if (!onlySha1.isEmpty()){
            rootFolder = getRootFolderByCommit(onlySha1);
        }
    }

    public Folder getRootFolderByCommit(String commitSha1)
            throws IOException, PathNotExistsException {
        return getRootFolderByCommit(commitSha1, activeRepo);
    }

    public Folder getRootFolderByCommit(String commitSha1, String repoPath)
            throws IOException, PathNotExistsException {
        if (commitSha1.isEmpty()) return null;
        String commitFile = FilesHandler.readFile(repoPath + OBJECTS_FOLDER + "\\" + commitSha1 + ".zip");

        String[] split = commitFile.split("\n");
        String date = split[1];
        String userName = split[2];
        String rootSha1 = split[3];

        if (rootSha1 != null)
            return createCommitRootFolder(rootSha1, userName, date, repoPath);
        else
            return null;
    }

    public Folder getRootFolder() {
        return rootFolder;
    }

    public void setActiveRepo(String activeRepo) {
        this.activeRepo = activeRepo;
    }
}
