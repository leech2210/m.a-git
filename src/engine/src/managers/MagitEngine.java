package managers;

import abstractions.Engine;
import exceptions.*;
import models.*;
import utils.FilesHandler;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.stream.Collectors;

import static utils.Consts.*;

public class MagitEngine implements Engine {

    private String activeUser;

    private BranchesManager branchesManager;
    private WorkingCopyManager workingCopyManager;
    private RepoManager repoManager;

    public MagitEngine() {
        activeUser = "Administrator";

        branchesManager = new BranchesManager();
        workingCopyManager = new WorkingCopyManager();
        repoManager = new RepoManager();
    }

    //<editor-fold desc="Repo commands">
    @Override
    public Repository getActiveRepo() {
        return repoManager.getRepository();
    }

    @Override
    public void initRepo(Repository repo, String headData) throws PathNotExistsException,
            IOException, InvalidMagitRepoExceptions {
        repoManager.initRepo(repo, headData);
        updateActiveRepo(repo.getLocation());
    }

    @Override
    public void changeRepo(String repoPath) throws InvalidMagitRepoExceptions,
            PathNotExistsException, IOException {
        updateActiveRepo(repoPath);
        workingCopyManager.updateCurrCommit();
        workingCopyManager.updateRootFolder();
    }
    private void updateActiveRepo(String repo) throws InvalidMagitRepoExceptions,
            PathNotExistsException, IOException {
        repoManager.changeRepo(repo);
        branchesManager.setActiveRepo(repo);
        workingCopyManager.setActiveRepo(repo);
    }

    @Override
    public void loadRepoFromXML(String xmlPath) throws JAXBException, XmlFileExceptions,
            IOException, PathNotExistsException, InvalidMagitRepoExceptions {
        XmlManager xmlManager = new XmlManager(xmlPath);
        Repository repo = xmlManager.getRepository();

        xmlManager.creatMaps();
        xmlManager.creatBranches();
        xmlManager.updateBranchCommit();

        repoManager.initRepo(repo, xmlManager.getHeadBranchName());
        xmlManager.UpdateRemote();
        updateActiveRepo(repo.getLocation());

        workingCopyManager.updateCurrCommit();
        workingCopyManager.updateRootFolder();
        workingCopyManager.deployFiles();
    }
    public void loadRepoFromXML(InputStream inStream, String repoPath) throws JAXBException, XmlFileExceptions,
            IOException, PathNotExistsException, InvalidMagitRepoExceptions {
        XmlManager xmlManager = new XmlManager(inStream);
        Repository repo = xmlManager.getRepository();
        repo.setLocation(repoPath);
        xmlManager.creatMaps();
        xmlManager.creatBranches();
        xmlManager.updateBranchCommit();

        repoManager.initRepo(repo, xmlManager.getHeadBranchName());
        xmlManager.UpdateRemote();
        updateActiveRepo(repo.getLocation());

        workingCopyManager.updateCurrCommit();
        workingCopyManager.updateRootFolder();
        workingCopyManager.deployFiles();
    }

    @Override
    public void xmlFileValidation(String xmlPath) throws JAXBException, XmlFileExceptions {
        XmlManager xmlManager = new XmlManager(xmlPath);
        xmlManager.isXmlValid();
    }
    public void xmlFileValidation(InputStream inStream) throws JAXBException, XmlFileExceptions {
        XmlManager xmlManager = new XmlManager(inStream);
        xmlManager.isXmlValid();
    }

    @Override
    public Repository getXmlRepo(String xmlPath) throws JAXBException {
        XmlManager xmlManager = new XmlManager(xmlPath);
        return xmlManager.getRepository();
    }

    public Repository getXmlRepo(InputStream inStream) throws JAXBException {
        XmlManager xmlManager = new XmlManager(inStream);
        return xmlManager.getRepository();
    }

    //</editor-fold>

    //<editor-fold desc="Branches commands">
    @Override
    public List<String> getAllBranches() throws RepoNotInitializedException {
        if (repoManager.getRepository() == null) throw new RepoNotInitializedException();
        return branchesManager.getAllBranches();
    }

    @Override
    public String getHeadBranch() throws RepoNotInitializedException {
        if (repoManager.getRepository() == null) throw new RepoNotInitializedException();
        return branchesManager.getHeadBranch();
    }

    @Override
    public Map<String, List<String>> getAllBranchesSha1() throws IOException, PathNotExistsException {
        return branchesManager.getAllBranchesSha1();
    }
    @Override
    public Map<String, List<String>> getAllRTBBranchesSha1() throws IOException, PathNotExistsException {
        return branchesManager.getAllRTBBranchesSha1();
    }
    @Override
    public Map<String, List<String>> getAllRTBAndRbBranchesSha1() throws IOException, PathNotExistsException {
        return branchesManager.getAllRTBAndRbBranchesSha1();
    }

    @Override
    public boolean addNewBranch(String branchName, String commitSha1, String branchType, String rbBranchName)
            throws IOException, PathNotExistsException, RepoNotInitializedException {
        if (repoManager.getRepository() == null) throw new RepoNotInitializedException();
        return branchesManager.addNewBranch(branchName, commitSha1,branchType, rbBranchName);
    }
    public String isPointedAsRB(String commitSha1)throws IOException, PathNotExistsException{
        Map<String, List<String>> allBranchesSha1=getAllBranchesSha1();

        if (allBranchesSha1.containsKey(commitSha1)){
            List<String> rbName=  allBranchesSha1.get(commitSha1);
            for (String branch : rbName){
                String[] split = branch.split("\\\\");
                if(split.length==2) {
                    return branch;
                }
            }
        }
        return "";
    }
    @Override
    public boolean deleteBranch(String branchName) throws PathNotExistsException,
            RepoNotInitializedException {
        if (repoManager.getRepository() == null) throw new RepoNotInitializedException();
        return branchesManager.deleteBranch(branchName);
    }

    @Override
    public boolean switchBranch(String branchName) throws IOException, PathNotExistsException,
            RepoNotInitializedException {
        if (repoManager.getRepository() == null) throw new RepoNotInitializedException();
        if (branchesManager.switchBranch(branchName)) {
            workingCopyManager.updateRootFolder();
            workingCopyManager.updateCurrCommit();
            workingCopyManager.deployFiles();
            return true;
        }
        else
            return false;
    }

    @Override
    public boolean isCommitExists(String commitSha1) {
        return workingCopyManager.isCommitExists(commitSha1);
    }

    @Override
    public void resetHeadBranchToCommit(String commitSha1) throws IOException,
            PathNotExistsException, RepoNotInitializedException {
        if (repoManager.getRepository() == null) throw new RepoNotInitializedException();
        branchesManager.updateHeadBranchCommit(commitSha1);
        workingCopyManager.updateRootFolder();
        workingCopyManager.updateCurrCommit();
        workingCopyManager.deployFiles();
    }
    //</editor-fold>

    //<editor-fold desc="User commands">
    @Override
    public String getActiveUser() {
        return activeUser;
    }

    @Override
    public void updateUser(String userName) {
        this.activeUser = userName;
    }
    //</editor-fold>

    //<editor-fold desc="WorkingCopy commands">
    @Override
    public String commit(String commitMessage) throws IOException,
            PathNotExistsException, RepoNotInitializedException {
        if (repoManager.getRepository() == null) throw new RepoNotInitializedException();
        String activeBranch = branchesManager.getHeadBranch();
        Commit currCommit = workingCopyManager.getCurrCommit();
        String res = workingCopyManager.commit(commitMessage, activeUser, activeBranch, currCommit, null);
        branchesManager.updateHeadBranchCommit(res);
        return res;
    }

    @Override
    public List<Conflict> getConflicts(String branchToMerge) throws IOException,
            PathNotExistsException {
        String sha1ToMerge = branchesManager.getBranchSha1(branchToMerge);
        return workingCopyManager.getConflicts(sha1ToMerge);
    }

    @Override
    public boolean handleFFMerge(String branchToMerge) throws IOException,
            PathNotExistsException {
        String mergedSha1 = branchesManager.getBranchSha1(branchToMerge);
        String ancestorSha1 = workingCopyManager.getAncestor(mergedSha1);
        String headSha1 = branchesManager.getBranchSha1(branchesManager.getHeadBranch());
        if (ancestorSha1.equals(headSha1)) {
            branchesManager.updateHeadBranchCommit(mergedSha1);
            return true;
        }
        else if (ancestorSha1.equals(mergedSha1))  // head branch contain the branch that need to be merged
            return true;

        return false;
    }

    @Override
    public void merge(String branchToMerge, String commitMessage) throws RepoNotInitializedException,
            IOException, PathNotExistsException {
        String head = getHeadBranch();

        // get first and second commits
        Commit first = workingCopyManager.getCurrCommit();
        Commit second = workingCopyManager.getBranchCommit(branchToMerge);

        String res = workingCopyManager.commit(commitMessage, activeUser, head, first, second);
        branchesManager.updateHeadBranchCommit(res);
    }

    @Override
    public Changes getStatus() throws RepoNotInitializedException,
            PathNotExistsException, IOException {
        String repoPath = repoManager.getRepository().getLocation();
        String commitSha1 = workingCopyManager.getCurrCommit().getSha1();
        return getStatus(repoPath, commitSha1);
    }

    private Changes getStatus(String repoPath, String commitSha1) throws RepoNotInitializedException,
            PathNotExistsException, IOException {
        if (!FilesHandler.isDirectory(repoPath + GIT_FOLDER))
            throw new RepoNotInitializedException();
        Folder rootFolder = workingCopyManager.getRootFolderByCommit(commitSha1, repoPath);
        return ChangesManager.getChanges(repoPath, rootFolder);
    }

    @Override
    public Commit activeBranchHistory() throws RepoNotInitializedException {
        if (repoManager.getRepository() == null) throw new RepoNotInitializedException();
        return workingCopyManager.getCurrCommit();
    }

    @Override
    public Commit getBranchCommit(String branch) throws IOException, PathNotExistsException {
        return workingCopyManager.getBranchCommit(branch);
    }

    @Override
    public Folder getCurrentCommitFiles() throws RepoNotInitializedException {
        if (repoManager.getRepository() == null) throw new RepoNotInitializedException();
        return workingCopyManager.getRootFolder();
    }
    public List<String> getCommitFiles(String sha1) throws RepoNotInitializedException, IOException, PathNotExistsException {
        List<String> allFiles = new ArrayList<>();
        List<GitObject> temp=workingCopyManager.getCommitFiles(sha1);
        for (GitObject object: temp)
            allFiles.add(object.getFullName());
        return allFiles;
    }

    @Override
    public Folder getRootFolderCommit(String commitSha1) throws IOException, PathNotExistsException {
        return workingCopyManager.getRootFolderByCommit(commitSha1);
    }

    @Override
    public boolean isOpenChanges() throws RepoNotInitializedException, PathNotExistsException, IOException {
        Changes changes = getStatus();
        return (changes.CreatedFiles.size() > 0 ||
                changes.UpdatedFiles.size() > 0 ||
                changes.DeletedFiles.size() > 0);
    }

    private boolean isOpenChangesInRemote(String repoPath, String commitSha1)
            throws IOException, PathNotExistsException, RepoNotInitializedException {
        Changes changes = getStatus(repoPath, commitSha1);
        return (changes.CreatedFiles.size() > 0 ||
                changes.UpdatedFiles.size() > 0 ||
                changes.DeletedFiles.size() > 0);
    }

    @Override
    public Changes getDiff(String prevCommit, String commit) throws IOException, PathNotExistsException {
        Folder prevFolder = workingCopyManager.getRootFolderByCommit(prevCommit);
        Folder rootFolder = workingCopyManager.getRootFolderByCommit(commit);

        return ChangesManager.getDiff(prevFolder, rootFolder);
    }
    //</editor-fold>

    //<editor-fold desc="Remote commands">
    @Override
    public void clone(String remoteLocation, Repository localRepo)
            throws IOException, PathNotExistsException {

        String localLocation = localRepo.getLocation();
        if (remoteLocation == null) throw new PathNotExistsException();
        if (localLocation == null) throw new PathNotExistsException();

        // clone directory
        FilesHandler.copyDirectory(remoteLocation, localLocation);

        // change repo name
        FilesHandler.writeToFile(localLocation + REPO_NAME_FILE, localRepo.getName(), false);
        FilesHandler.writeToFile(localLocation + REMOTE_PATH_FILE, localRepo.getRemoteLocation(), false);

        // create RB branches
        String remoteRepoName = FilesHandler.readFile(remoteLocation + REPO_NAME_FILE);
        List<String> remoteBranches = FilesHandler.getAllFilesNames(remoteLocation + BRANCHES_FOLDER);

        for (String branchRemote : remoteBranches) {
            if (!branchRemote.equals("head")) {
                String branchSha1 = FilesHandler.readFile(remoteLocation + BRANCHES_FOLDER + "\\"
                        + branchRemote + ".txt");
                String rbFilePath = localLocation + BRANCHES_FOLDER + "\\" + remoteRepoName + "\\" + branchRemote + ".txt";
                FilesHandler.writeToFile(rbFilePath, branchSha1, false);
            }
        }

        // create RTB for head branch
        String headBranch = FilesHandler.readFile(remoteLocation + HEAD_BRANCH_FILE);
        String headCommit = FilesHandler.readFile(remoteLocation + BRANCHES_FOLDER + "\\" + headBranch + ".txt");
        FilesHandler.writeToFile(localLocation + BRANCHES_FOLDER + "\\" + headBranch + ".txt",
                headCommit + ",tracking=" + remoteRepoName + "\\" + headBranch,
                false);
    }

    @Override
    public void fetch()
            throws IOException, PathNotExistsException {
        Repository repository = repoManager.getRepository();

        String localLocation = repository.getLocation();
        String remoteLocation = repository.getRemoteLocation();

        // update objects data
        List<String> localObjects = FilesHandler.getAllFilesNames(localLocation + OBJECTS_FOLDER);
        List<String> remoteObjects = FilesHandler.getAllFilesNames(remoteLocation + OBJECTS_FOLDER);

        for (String objectRemote : remoteObjects) {
            if (!localObjects.contains(objectRemote)) {
                String objectContent = FilesHandler.readFile(remoteLocation + OBJECTS_FOLDER + "\\"
                        + objectRemote + ".zip");
                String objectFileNewPath = localLocation + OBJECTS_FOLDER + "\\" + objectRemote + ".zip";
                FilesHandler.writeToFile(objectFileNewPath, objectContent, true);
            }
        }

        // create new branches&RB branches from RR and update all RB new locations
        String remoteRepoName = FilesHandler.readFile(remoteLocation + REPO_NAME_FILE);
        String rbFolderPath = localLocation + BRANCHES_FOLDER + "\\" + remoteRepoName;
        List<String> localBranches = FilesHandler.getAllFilesNames(localLocation + BRANCHES_FOLDER);
        List<String> remoteBranches = FilesHandler.getAllFilesNames(remoteLocation + BRANCHES_FOLDER);

        for (String branchRemote : remoteBranches) {
            if (!localBranches.contains(branchRemote)) {
                String branchSha1 = FilesHandler.readFile(remoteLocation + BRANCHES_FOLDER + "\\"
                        + branchRemote + ".txt");
                String rbFilePath = localLocation + BRANCHES_FOLDER + "\\" + remoteRepoName + "\\"
                        + branchRemote + ".txt";
                FilesHandler.writeToFile(rbFilePath, branchSha1, false);
                String bFilePath = localLocation + BRANCHES_FOLDER + "\\" + branchRemote + ".txt";
                FilesHandler.writeToFile(bFilePath, branchSha1 + ",tracking=" + remoteRepoName + "\\" +
                        branchRemote, false);
            } else {
                if (!branchRemote.equals("head")) {
                    String rbSha1 = FilesHandler.readFile(rbFolderPath + "\\"
                            + branchRemote + ".txt");
                    String branchSha1 = FilesHandler.readFile(remoteLocation + BRANCHES_FOLDER + "\\"
                            + branchRemote + ".txt");
                    if (!rbSha1.equals(branchSha1)) {
                        FilesHandler.writeToFile(rbFolderPath + "\\" + branchRemote + ".txt",
                                branchSha1, false);

                    }
                }
            }
        }
    }


    @Override
    public void pull() throws IOException, PathNotExistsException, RemoteException, RepoNotInitializedException {
        Repository repository = repoManager.getRepository();
        String repoLocalPath = repository.getLocation();
        String repoRemotePath = repository.getRemoteLocation();
        String repoLocalName = FilesHandler.readFile(repoLocalPath + REPO_NAME_FILE);
        String repoRemoteName = FilesHandler.readFile(repoRemotePath + REPO_NAME_FILE);
        String activeBranch = FilesHandler.readFile(repoLocalPath + HEAD_BRANCH_FILE);


        //check if the head branch is rtb after another rb
        String rtbContent = FilesHandler.readFile(repoLocalPath + BRANCHES_FOLDER + "\\"
                + activeBranch + ".txt");
        String isTracking = workingCopyManager.parseMetadataTrackingBranch(rtbContent);
        if (isTracking.equals("")) {
            throw new RemoteException("The head branch is not RTB after another RB");
        }
        String[] split=rtbContent.split(",");
        if (isOpenChanges())
            throw new RemoteException("you have some open changes, the pull failed");
        else {
            String [] rbPath=split[1].split("=");
            String rbContent = FilesHandler.readFile(repoLocalPath + BRANCHES_FOLDER + "\\"
                    + rbPath[1] + ".txt");
            if (!rbContent.equals(split[0]))
                throw new RemoteException("you have some changes you need to PUSH before you PULL");
            else {
                String rrContent = FilesHandler.readFile(repoRemotePath + BRANCHES_FOLDER + "\\"
                        + activeBranch + ".txt");
                addCommitObjects(rrContent, repoRemotePath, repoLocalPath);
                FilesHandler.writeToFile(repoLocalPath + BRANCHES_FOLDER + "\\"
                        + rbPath[1] + ".txt", rrContent, false);
                FilesHandler.writeToFile(repoLocalPath + BRANCHES_FOLDER + "\\"
                        + activeBranch + ".txt", rrContent + "," + split[1], false);
                workingCopyManager.updateRootFolder();
                workingCopyManager.updateCurrCommit();
                workingCopyManager.deployFiles();
            }

        }
    }

    private String getHeadCommitSha1(String repoPath) throws IOException, PathNotExistsException {
        String headBranchName = FilesHandler.readFile(repoPath + HEAD_BRANCH_FILE);
        return FilesHandler.readFile(repoPath + BRANCHES_FOLDER + "\\" + headBranchName + ".txt");
    }

    @Override
    public void push() throws IOException, PathNotExistsException, RemoteException, RepoNotInitializedException {
        Repository repository = repoManager.getRepository();
        String repoLocalPath = repository.getLocation();
        String repoRemotePath = repository.getRemoteLocation();
        String repoRemoteName = FilesHandler.readFile(repoRemotePath + REPO_NAME_FILE);
        String activeBranch = FilesHandler.readFile(repoLocalPath + HEAD_BRANCH_FILE);
        String remoteSha1 = getHeadCommitSha1(repoRemotePath);

        String localBranchPath=repoLocalPath + BRANCHES_FOLDER + "\\" + activeBranch + ".txt";
        String localContent = FilesHandler.readFile(localBranchPath);

        /*if (isOpenChangesInRemote(repoRemotePath, remoteSha1))
            throw new RemoteException("The remote have some open changes, the push failed");*/

            String localBranchSha1=FilesHandler.readFile(localBranchPath);
            //create rb for the local branch
            String rbPath=repoLocalPath + BRANCHES_FOLDER + "\\" + repoRemoteName + "\\" + activeBranch + ".txt";
            FilesHandler.writeToFile(rbPath, localBranchSha1,false);

            //change the local branch to a rtb
            FilesHandler.writeToFile(localBranchPath, localBranchSha1 + ",tracking=" + repoRemoteName + "\\" + activeBranch,false);

            //create new local branch in the remote
            String rrPath=repoRemotePath + BRANCHES_FOLDER + "\\" + activeBranch + ".txt";
            FilesHandler.writeToFile(rrPath, localBranchSha1, false);

            //updated the object folder
            addCommitObjects(localContent, repoLocalPath, repoRemotePath);


    }

    private List<String> commitParents(String sha1, String path) throws PathNotExistsException, IOException {
        List<String> parents = new ArrayList<>();

        String content = FilesHandler.readFile(path + OBJECTS_FOLDER + "\\" + sha1 + ".zip");
        String[] split = content.split("\n");
        if (split.length == 5) {
            parents.add(split[4]);
        } else {
            if (split.length == 6) {
                parents.add(split[4]);
                parents.add(split[5]);
            }
        }
        return parents;
    }

    private void addCommitObjects(String sha1, String fromPath, String toPath)
            throws NoSuchElementException, IOException, PathNotExistsException {
        Queue<String> allParents = new LinkedList<>();
        List<String> allCommits = new ArrayList<>();

        //Make a list of the brand commits
        allParents.add(sha1);
        while (!allParents.isEmpty()) {
            String currentCommit = allParents.remove();
            allCommits.add(currentCommit);
            List<String> commitParents = commitParents(currentCommit, fromPath);
            for (String parent : commitParents) {
                allParents.add(parent);
            }
        }

        for (String commit : allCommits) {
            String content=FilesHandler.readFile(fromPath + "\\" + OBJECTS_FOLDER +
                     "\\" + commit + ".zip");
            String[] split = content.split("\n");
            getObjectsRec(split[3], fromPath, toPath);
            String data = FilesHandler.readFile(fromPath + OBJECTS_FOLDER + "\\" + split[3] + ".zip");
            FilesHandler.writeToFile(toPath + OBJECTS_FOLDER + "\\" + split[3]
                    , data, true);
            FilesHandler.writeToFile(toPath + OBJECTS_FOLDER + "\\" + commit
                    , content, true);
        }
    }

    private void getObjectsRec(String sha1, String fromPath, String toPath) throws IOException, PathNotExistsException {
        String content = FilesHandler.readFile(fromPath + OBJECTS_FOLDER + "\\" + sha1 + ".zip");
        String[] split = content.split("\n");

        for (String line : split) {
            String[] splitAgain = line.split(",");
            String data = FilesHandler.readFile(fromPath + OBJECTS_FOLDER + "\\" + splitAgain[1] + ".zip");
            FilesHandler.writeToFile(toPath + OBJECTS_FOLDER + "\\" + splitAgain[1]
                    , data, true);
            if (splitAgain[2].equals("FOLDER")) {
                getObjectsRec(splitAgain[1], fromPath, toPath);
            }

        }
    }
    //</editor-fold>

    //<editor-fold desc="Pull Requests">
    @Override
    public String createNewPr(String targetBranch, String baseBranch, String message, String assignedUser)
            throws IOException, PathNotExistsException {

        // create pull request object
        String targetSha1 = workingCopyManager.getBranchCommit(targetBranch).getSha1();
        String baseSha1 = workingCopyManager.getBranchCommit(baseBranch).getSha1();
        PullRequest pullRequest = new PullRequest(baseBranch, baseSha1, targetBranch, targetSha1,
                activeUser, assignedUser, message);

        // update the pull requests file
        String remoteRepo = getActiveRepo().getRemoteLocation();
        String pullRequestPath = remoteRepo + "\\.magit\\" + PULL_REQUEST_FILE;
        if (!FilesHandler.isFile(pullRequestPath))
            FilesHandler.writeToFile(pullRequestPath, pullRequest.toString() + "\n",false);
        else
            FilesHandler.appendToFile(pullRequestPath, pullRequest.toString() + "\n");

        // update the notifications file
        String notificationsPath = ALL_REPO_PATH + "\\" + assignedUser + "\\" + NOTIFICATIONS_FILE;
        String[] allData=pullRequest.toString().split(",");
        FilesHandler.appendToFile(notificationsPath,"You have new pull request from: " + allData[1] +
                ", with the message: " +allData[8]+ "\n");

        return pullRequest.getId();
    }

    @Override
    public void closePr(String id, Status status) throws IOException, PathNotExistsException {
        // update the pull requests file
        String activeRepo = getActiveRepo().getLocation();
        String pullRequestPath = activeRepo + "\\.magit\\" + PULL_REQUEST_FILE;
        List<PullRequest> pullRequests = parsePullRequestFile(pullRequestPath);
        PullRequest pr = pullRequests.stream().filter((x) -> x.getId().equals(id)).findFirst().get();
        pr.setStatus(status);
        List<String> prsStrings = pullRequests.stream().map((x) -> x.toString()).collect(Collectors.toList());

        FilesHandler.writeToFile(pullRequestPath, prsStrings);

        // update the notifications file
        String notificationsPath = ALL_REPO_PATH + "\\" + pr.getCreatedBy() + "\\" + NOTIFICATIONS_FILE;
        FilesHandler.appendToFile(notificationsPath, "Your pull request: " + pr.getId()
                + " has been " + pr.getStatus() + "\n");
    }

    @Override
    public List<Commit> getPrCommits(String id) throws IOException, PathNotExistsException {
        List<Commit> commits = new ArrayList<>();

        String activeRepo = getActiveRepo().getLocation();
        String pullRequestPath = activeRepo + "\\.magit\\" +  PULL_REQUEST_FILE;
        List<PullRequest> pullRequests = parsePullRequestFile(pullRequestPath);
        PullRequest pr = pullRequests.stream().filter((x) -> x.getId().equals(id)).findFirst().get();

        String target = pr.getTargetSha1();
        String base = pr.getBaseSha1();


        String targetCommitFile = FilesHandler.readFile(activeRepo + OBJECTS_FOLDER + "\\" + target + ".zip");
        Commit targetCommit = workingCopyManager.parseMetadataCommitFile(targetCommitFile, target);

        String baseCommitFile = FilesHandler.readFile(activeRepo + OBJECTS_FOLDER + "\\" + base + ".zip");
        String baseCommitSha1 = workingCopyManager.parseMetadataCommitFile(baseCommitFile, base).getSha1();


        getAllCommitsUntilBase(targetCommit, baseCommitSha1, commits);

        return commits;
    }

    private void getAllCommitsUntilBase(Commit targetCommit, String baseCommit, List<Commit> allCommits){
        if (targetCommit.getSha1().equals(baseCommit))
            return;
        allCommits.add(targetCommit);
        getAllCommitsUntilBase(targetCommit.getFirstParent(), baseCommit, allCommits);
    }

    public List<Commit> getBranchCommits(String branchName) throws IOException, PathNotExistsException {
        List<Commit> commits = new ArrayList<>();
        Commit thisCommit=getBranchCommit(branchName);
        getRec(thisCommit, commits);
        return commits;
    }
    private void getRec(Commit commit, List<Commit> allCommits){
        if (commit == null)
            return;
        allCommits.add(commit);
        getRec(commit.getFirstParent(),allCommits);
    }

    private List<PullRequest> parsePullRequestFile(String filePath) throws IOException, PathNotExistsException {
        List<PullRequest> pullRequests = new ArrayList<>();

        String content = FilesHandler.readFile(filePath);
        String[] rows = content.split("\n");
        for (String row : rows)
            pullRequests.add(PullRequest.parse(row));

        return pullRequests;
    }

    @Override
    public List<PullRequest> getUserPullRequests() throws IOException, PathNotExistsException {
        String activeRepo = getActiveRepo().getLocation();
        String pullRequestPath = activeRepo + "\\" + PULL_REQUEST_FILE;
        return parsePullRequestFile(pullRequestPath);
    }
    //</editor-fold>
}