package managers;

import enums.FileType;
import exceptions.PathNotExistsException;
import models.Changes;
import models.Folder;
import models.GitObject;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.collections4.CollectionUtils;
import utils.FilesHandler;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

public class ChangesManager {

    public static Collection<String> getDeletedFiles(Folder rootFolder){
        if (rootFolder != null) {
            List<String> listFiles = FilesHandler.listFiles(rootFolder.getFullName(), true);
            List<String> listCommitFiles = getListCommitFiles(rootFolder);

            List<Path> listPathCommitFiles=listCommitFiles.stream().map(Paths::get).collect(Collectors.toList());
            listCommitFiles = listPathCommitFiles.stream().map(Path::toString).collect(Collectors.toList());

            return CollectionUtils.subtract(listCommitFiles, listFiles);
        }
        else
            return new ArrayList<>();
    }

    public static Collection<String> getCreatedFiles(String activeRepo, Folder rootFolder){
        if (rootFolder != null){
            List<String> listFiles = FilesHandler.listFiles(rootFolder.getFullName(), true);
            List<String> listCommitFiles = getListCommitFiles(rootFolder);

            List<Path> listPathCommitFiles=listCommitFiles.stream().map(Paths::get).collect(Collectors.toList());
            listCommitFiles = listPathCommitFiles.stream().map(Path::toString).collect(Collectors.toList());

            return CollectionUtils.subtract(listFiles, listCommitFiles);
        }
        else{
            return FilesHandler.listFiles(activeRepo, true);
        }
    }

    private static Collection<String> getUpdatedFilesRec(Folder rootFolder) throws PathNotExistsException, IOException {
        Collection<String> updatedFiles = new ArrayList<>();
        for (GitObject f: rootFolder.files) {
            if (f.getType() == FileType.FILE){
                if (FilesHandler.isFile(f.getFullName())){
                    String content = FilesHandler.readFile(f.getFullName());
                    if (!DigestUtils.sha1Hex(content).equals(f.getSha1()))
                        updatedFiles.add(f.getFullName());
                }
            }
            else{
                updatedFiles.addAll(getUpdatedFilesRec((Folder)f));
            }
        }

        return updatedFiles;
    }

    public static Collection<String> getUpdatedFiles(Folder rootFolder) throws PathNotExistsException, IOException {
        if (rootFolder != null){
            return getUpdatedFilesRec(rootFolder);
        }
        return new ArrayList<>();
    }

    private static Collection<String> getUpdatedFiles(Folder prevFolder, Folder folder){
        Collection<String> updated = new ArrayList<>();
        Map<String, String> fileToSha1 = new HashMap<>();
        getFilesSha1(fileToSha1, prevFolder);
        for (GitObject f: folder.getFiles(true)) {
            if (fileToSha1.containsKey(f.getFullName()))
            {
                String prevSha1 = fileToSha1.get(f.getFullName());
                String currSha1 = f.getSha1();
                if (!prevSha1.equals(currSha1))
                    updated.add(f.getFullName());
            }
        }
        return  updated;
    }

    private static void getFilesSha1(Map<String, String> fileToSha1, Folder folder){
        for (GitObject object: folder.files) {
            if (object.getType() == FileType.FILE)
                fileToSha1.put(object.getFullName(), object.getSha1());
            else
                getFilesSha1(fileToSha1, (Folder) object);
        }
    }

    private static List<String> getListCommitFiles(Folder root){
        // list all files as written in the commit
        List<String> listCommitFiles = new ArrayList<>();
        for (GitObject file: root.files) {
            if (file.getType() == FileType.FILE)
                listCommitFiles.add(file.getFullName());
            else
                listCommitFiles.addAll(getListCommitFiles((Folder)file));
        }

        return listCommitFiles;
    }

    public static Changes getChanges(String activeRepo, Folder rootFolder) throws PathNotExistsException, IOException {
        Collection<String> deleted = getDeletedFiles(rootFolder);
        Collection<String> created = getCreatedFiles(activeRepo, rootFolder);
        Collection<String> updated = getUpdatedFiles(rootFolder);

        Changes changes = new Changes();
        changes.CreatedFiles = created;
        changes.DeletedFiles = deleted;
        changes.UpdatedFiles = updated;

        return changes;
    }

    public static Changes getDiff(Folder rootFolderPrev, Folder rootFolder){
        Changes changes = new Changes();
        List<String> list = getListCommitFiles(rootFolder);
        if (rootFolderPrev != null && rootFolder != null){
            List<String> listPrev = getListCommitFiles(rootFolderPrev);

            changes.DeletedFiles = CollectionUtils.subtract(listPrev, list);
            changes.CreatedFiles = CollectionUtils.subtract(list, listPrev);
            changes.UpdatedFiles = getUpdatedFiles(rootFolderPrev, rootFolder);

        }
        else if (rootFolder != null){
            changes.CreatedFiles = list;
        }

        return changes;
    }
}
