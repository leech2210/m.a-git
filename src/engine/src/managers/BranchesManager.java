package managers;

import exceptions.PathNotExistsException;
import utils.FilesHandler;

import java.io.IOException;
import java.util.*;

import static utils.Consts.BRANCHES_FOLDER;
import static utils.Consts.HEAD_BRANCH_FILE;

public class BranchesManager {

    private String headBranch;
    private List<String> allBranches;
    private String activeRepo;

    public BranchesManager(){
        this.headBranch = "master";
    }

    private boolean isBranchExist(String branchName){
        if (allBranches.contains(branchName))
            return true;
        return false;
    }

    public List<String> getAllBranches(){
        return this.allBranches;
    }

    public String getHeadBranch(){
        return headBranch;
    }

    public boolean deleteBranch(String branchName) throws PathNotExistsException {
        if (isBranchExist(branchName)){
            FilesHandler.deleteFile(activeRepo + BRANCHES_FOLDER + "\\" + branchName + ".txt");
            allBranches.remove(branchName);
            return true;
        }
        else
            return false;
    }
   public boolean addNewBranch(String branchName, String commitSha1, String branchType, String rbBranchName) throws IOException, PathNotExistsException {
        if (!isBranchExist(branchName)){
            //String commit = FilesHandler.readFile(activeRepo + BRANCHES_FOLDER + "\\" + headBranch + ".txt");
            if(branchType.equals("2"))
            FilesHandler.writeToFile(activeRepo + BRANCHES_FOLDER + "\\" + branchName + ".txt",
                    commitSha1, false);
            else {
                FilesHandler.writeToFile(activeRepo + BRANCHES_FOLDER + "\\" + branchName + ".txt",
                        commitSha1 + ",tracking=" + rbBranchName, false);
                //FilesHandler.writeToFile(activeRepo +  BRANCHES_FOLDER + "\\" + rbBranchName
                            // + ".txt", commitSha1, false);

            }
            allBranches.add(branchName);
            //allBranches.add(rbBranchName);
            return true;
        }
        else
            return false;
    }


    public boolean switchBranch(String branchName) throws IOException, PathNotExistsException {
        if (isBranchExist(branchName)){
            updateHeadBranch(branchName);
            return true;
        }
        else
            return false;
    }

    private void updateHeadBranch(String branchName) throws IOException, PathNotExistsException {
        FilesHandler.writeToFile(activeRepo + HEAD_BRANCH_FILE, branchName, false);
        this.headBranch = branchName;
    }

    public void updateHeadBranchCommit(String commitSha1) throws IOException, PathNotExistsException {
        // change the head branch file content
        String oldContent=FilesHandler.readFile(activeRepo + BRANCHES_FOLDER + "\\" + headBranch + ".txt");
        String tracking=branchTracking(oldContent);
        if (!tracking.equals("")) {
            FilesHandler.writeToFile(activeRepo + BRANCHES_FOLDER + "\\" + headBranch + ".txt",
                    commitSha1 + "," + tracking, false);
        }
        else
            FilesHandler.writeToFile(activeRepo + BRANCHES_FOLDER + "\\" + headBranch + ".txt",
                    commitSha1 , false);
    }

    public void setActiveRepo(String activeRepo) throws PathNotExistsException, IOException {
        this.activeRepo = activeRepo;
        updateListBranches();
        updateHeadBranch();
    }

    private void updateListBranches(){
        this.allBranches = FilesHandler.listOnlyFilesFilter(activeRepo + BRANCHES_FOLDER, "head.txt");
        List<String> allFolders=FilesHandler.listOnlyFoldersFilter(activeRepo + BRANCHES_FOLDER);
        if (allFolders.size()!= 0) {
            String remote = allFolders.get(0).toString();
            allFolders = FilesHandler.listRemoteFilesFilter(activeRepo + BRANCHES_FOLDER + "\\" + remote, remote);
            this.allBranches.addAll(allFolders);
        }
    }

    private void updateHeadBranch() throws PathNotExistsException, IOException {
        this.headBranch = FilesHandler.readFile(activeRepo + HEAD_BRANCH_FILE);
    }

    public Map<String, List<String>> getAllBranchesSha1() throws IOException, PathNotExistsException {
        Map<String, List<String>> branchToSha1 = new TreeMap<>();
        for (String branch: allBranches) {
            String branchContent = FilesHandler.readFile(activeRepo + BRANCHES_FOLDER + "\\" + branch + ".txt");
            String sha1=branchSha1(branchContent);

            if (branchToSha1.containsKey(sha1)){
                branchToSha1.get(sha1).add(branch);
            }
            else{
                List<String> branches = new ArrayList<>();
                branches.add(branch);
                branchToSha1.put(sha1, branches);
            }
        }
        return branchToSha1;
    }

    public Map<String, List<String>> getAllRTBBranchesSha1() throws IOException, PathNotExistsException {
        Map<String, List<String>> branchToSha1 = new TreeMap<>();
        String sha1 = null;
        for (String branch: allBranches) {
            String branchContent = FilesHandler.readFile(activeRepo + BRANCHES_FOLDER + "\\" + branch + ".txt");
            String isRTB=branchTracking(branchContent);
            if(!isRTB.equals("")) {
                sha1 = branchSha1(branchContent);

                if (branchToSha1.containsKey(sha1)) {
                    branchToSha1.get(sha1).add(branch);
                } else {
                    List<String> branches = new ArrayList<>();
                    branches.add(branch);
                    branchToSha1.put(sha1, branches);
                }
            }
        }
        return branchToSha1;
    }
    public Map<String, List<String>> getAllRTBAndRbBranchesSha1() throws IOException, PathNotExistsException {
        Map<String, List<String>> branchToSha1 = new TreeMap<>();
        String sha1 = null;
        Boolean toAdd= false;
        for (String branch: allBranches) {
            String branchContent = FilesHandler.readFile(activeRepo + BRANCHES_FOLDER + "\\" + branch + ".txt");
            String isRTB=branchTracking(branchContent);
            if(!isRTB.equals("")) {
                sha1 = branchSha1(branchContent);
                toAdd=true;
            }
            else{
                if (branch.contains("\\")){
                    sha1 = branchSha1(branchContent);
                    toAdd=true;
                }
            }
            if(toAdd) {
                if (branchToSha1.containsKey(sha1)) {
                    branchToSha1.get(sha1).add(branch);
                } else {
                    List<String> branches = new ArrayList<>();
                    branches.add(branch);
                    branchToSha1.put(sha1, branches);
                }
            }

        }
        return branchToSha1;
    }

    public String getBranchSha1(String branch) throws IOException, PathNotExistsException {
        String branchContent=FilesHandler.readFile(activeRepo + BRANCHES_FOLDER + "\\" + branch + ".txt");
        return branchSha1(branchContent);
    }


    private String branchSha1(String content) {
        String[] split = content.split(",");;
        return split[0];
    }
    private String branchTracking(String content) {
        String[] split = content.split(",");
        if(split.length==2) {
            return split[1];
        }
        else
            return "";

    }

}

