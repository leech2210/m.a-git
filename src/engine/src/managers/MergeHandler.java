package managers;

import enums.MergeCase;
import exceptions.PathNotExistsException;
import models.Conflict;
import models.GitObject;
import models.MergeFile;
import utils.FilesHandler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static utils.Consts.OBJECTS_FOLDER;

public class MergeHandler {

    private String repo;

    public MergeHandler(String repo){
        this.repo = repo;
    }

    public List<Conflict> merge(List<GitObject> ours,
                                List<GitObject> theirs,
                                List<GitObject> ancestor)
            throws PathNotExistsException, IOException {

        // union files
        Map<String, MergeFile> allFiles = unionFiles(ours, theirs, ancestor);

        // foreach file, check for merge case
        List<Conflict> conflicts = new ArrayList<>();
        for (Map.Entry<String, MergeFile> file : allFiles.entrySet()) {
            MergeFile value = file.getValue();
            String key = file.getKey();
            updateFileContent(value);

            // exists in ancestor
            if (value.inAncestor()){
                if (value.inTheirs() && value.inOurs() && !value.equalOursTheirs()
                        && !value.equalOursAncestor() && !value.equalTheirsAncestor()){
                    conflicts.add(new Conflict(key, MergeCase.UPDATE_BOTH_DIFF, value));
                }
                else if (!value.inTheirs() && value.inOurs()){
                    if (!value.equalOursAncestor())
                        conflicts.add(new Conflict(key, MergeCase.DELETE_THEIRS_UPDATED_OURS, value));
                    else
                        FilesHandler.deleteFile(key);
                }
                else if (value.inTheirs() && !value.inOurs()){
                    if (!value.equalTheirsAncestor())
                        conflicts.add(new Conflict(key, MergeCase.DELETE_OURS_UPDATED_THEIRS, value));
                }
            }
            else{
                if (value.inTheirs() && value.inOurs()){
                    if (!value.equalOursTheirs())
                        conflicts.add(new Conflict(key, MergeCase.CREATE_BOTH_DIFFERENT, value));
                }
                else if (value.inTheirs() && !value.inOurs()){
                    FilesHandler.writeToFile(key, value.contentTheirs, false);
                }
                else if (!value.inTheirs() && value.inOurs()){
                    FilesHandler.writeToFile(key, value.contentOurs, false);
                }
            }
        }

        return conflicts;
    }

    private void updateFileContent(MergeFile value){
        if (value.sha1Ours != null)
            value.contentOurs = getFileContent(value.sha1Ours);
        if (value.sha1Theirs != null)
            value.contentTheirs = getFileContent(value.sha1Theirs);
        if (value.sha1Ancestor != null)
            value.contentAncestor = getFileContent(value.sha1Ancestor);
    }

    private String getFileContent(String sha1){
        try {
            return FilesHandler.readFile(repo + OBJECTS_FOLDER  + "\\" + sha1 + ".zip");
        } catch (PathNotExistsException | IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private Map<String, MergeFile> unionFiles(List<GitObject> ours,
                                              List<GitObject> theirs,
                                              List<GitObject> ancestor){
        Map<String, MergeFile> allFiles = new HashMap<>();

        for (GitObject gitObject: ours) {
            if (allFiles.containsKey(gitObject.getFullName())){
                MergeFile mergeFile = allFiles.get(gitObject.getFullName());
                mergeFile.sha1Ours = gitObject.getSha1();
            }
            else{
                MergeFile mergeFile = new MergeFile();
                mergeFile.sha1Ours = gitObject.getSha1();
                allFiles.put(gitObject.getFullName(), mergeFile);
            }
        }

        for (GitObject gitObject: theirs) {
            if (allFiles.containsKey(gitObject.getFullName())){
                MergeFile mergeFile = allFiles.get(gitObject.getFullName());
                mergeFile.sha1Theirs = gitObject.getSha1();
            }
            else{
                MergeFile mergeFile = new MergeFile();
                mergeFile.sha1Theirs = gitObject.getSha1();
                allFiles.put(gitObject.getFullName(), mergeFile);
            }
        }

        for (GitObject gitObject: ancestor) {
            if (allFiles.containsKey(gitObject.getFullName())){
                MergeFile mergeFile = allFiles.get(gitObject.getFullName());
                mergeFile.sha1Ancestor = gitObject.getSha1();
            }
            else{
                MergeFile mergeFile = new MergeFile();
                mergeFile.sha1Ancestor = gitObject.getSha1();
                allFiles.put(gitObject.getFullName(), mergeFile);
            }
        }

        return allFiles;
    }
}
