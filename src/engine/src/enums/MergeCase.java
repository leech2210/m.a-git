package enums;

public enum MergeCase {
    DELETE_BOTH,
    DELETE_OURS_UPDATED_THEIRS, // conflict
    DELETE_OURS,
    DELETE_THEIRS_UPDATED_OURS, // conflict
    DELETE_THEIRS,
    UPDATE_BOTH_SAME,
    UPDATE_BOTH_DIFF, // conflict
    UPDATE_OURS,
    UPDATE_THEIRS,
    CREATE_OURS,
    CREATE_THEIRS,
    CREATE_BOTH_SAME,
    CREATE_BOTH_DIFFERENT // conflict
}
