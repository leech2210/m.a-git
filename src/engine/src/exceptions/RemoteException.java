package exceptions;

public class RemoteException extends Exception {
    public RemoteException(){
        super();
    }

    public RemoteException(String message){
        super(message);
    }
}

