package exceptions;

public class PathNotExistsException extends Exception {

    public PathNotExistsException(){
        super();
    }

    public PathNotExistsException(String fileName){
        super(fileName + " not exists!");
    }
}
