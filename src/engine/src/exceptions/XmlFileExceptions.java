package exceptions;

public class XmlFileExceptions extends Exception {
    public XmlFileExceptions(){
        super();
    }

    public XmlFileExceptions(String message){
        super(message);
    }

}
