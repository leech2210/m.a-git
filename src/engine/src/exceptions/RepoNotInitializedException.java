package exceptions;

public class RepoNotInitializedException extends Exception {

    public RepoNotInitializedException(){
        super();
    }

    @Override
    public String getMessage() {
        return "you didn't initialize repository, please first init a repo or switch to existing one";
    }
}
