package exceptions;

public class InvalidMagitRepoExceptions extends Exception {

    public InvalidMagitRepoExceptions(){
        super();
    }

    @Override
    public String getMessage() {
        return "the repository is not a valid magit repository";
    }
}
