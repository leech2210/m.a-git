package utils;

public class Consts {
    public static final String GIT_FOLDER = "\\.magit\\";
    public static final String BRANCHES_FOLDER = "\\.magit\\branches";
    //public static final String REMOTE_BRANCHES_FOLDER = "\\.magit\\remote_branches";
    //public static final String REMOTE_TRACKING_BRANCHES = "\\.magit\\remote_tracking_branches";
    public static final String HEAD_BRANCH_FILE = "\\.magit\\branches\\head.txt";
    public static final String REPO_NAME_FILE = "\\.magit\\repo.txt";
    public static final String REMOTE_PATH_FILE = "\\.magit\\remote_path.txt";
    public static final String OBJECTS_FOLDER = "\\.magit\\objects";
    public static final String DATE_FORMAT = "dd.MM.yyyy-HH:mm:ss:SSS";
    public static final String ALL_REPO_PATH = "C:\\magit-ex3";
    public static final String PULL_REQUEST_FILE = "pull_request.txt";
    public static final String PULL_REQUEST= "\\.magit\\pull_request.txt";
    public static final String NOTIFICATIONS_FILE = "notifications.txt";
}
