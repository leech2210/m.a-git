package utils;

import exceptions.PathNotExistsException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

import java.io.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

public class FilesHandler {

    public static String getFileExtension(String filePath){
        return FilenameUtils.getExtension(filePath);
    }

    public static String getFileDirectory(String filePath){
        return FilenameUtils.getFullPath(filePath);
    }

    public static void createDirectory(String dirPath) throws IOException {
        if(dirPath!=null && !"".equals(dirPath.trim()))
        {
            File dirFileObj = FileUtils.getFile(dirPath);

            if (!dirFileObj.exists())
                FileUtils.forceMkdir(dirFileObj);
        }
    }

    public static void copyDirectory(String src, String dest){
        File srcDir = new File(src);
        File destDir = new File(dest);

        try {
            FileUtils.copyDirectory(srcDir, destDir);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String readFile(String filePath) throws PathNotExistsException, IOException {
        File fileObj = FileUtils.getFile(filePath);

        if (!filePath.endsWith(".zip"))
            return FileUtils.readFileToString(fileObj, "utf-8");

        String unzippedFile = unzip(filePath, fileObj.getParent());
        String res = readFile(unzippedFile);
        FilesHandler.deleteFile(unzippedFile);
        return res;
    }

    public static List<String> readAllLines(String filePath) throws IOException, PathNotExistsException {
        File fileObj = FileUtils.getFile(filePath);

        if (!filePath.endsWith(".zip")){
            return FileUtils.readLines(fileObj, "utf-8");
        }
        else{
            String unzippedFile = unzip(filePath, fileObj.getParent());
            File unzippedFileObj = FileUtils.getFile(unzippedFile);
            List<String> res = FileUtils.readLines(unzippedFileObj, "utf-8");
            FilesHandler.deleteFile(unzippedFile);
            return res;
        }
    }

    public static List<String> listFiles(String dirPath, boolean recursive){
        List<String> filesNames = new ArrayList<>();

        File fileObj = FileUtils.getFile(dirPath);
        Collection<File> files = FileUtils.listFiles(fileObj,null, recursive);
        for (File file: files) {
            if (!file.getPath().contains(".magit"))
                filesNames.add(file.getPath());
        }

        return filesNames;
    }


    public static List<String> getAllFilesNames(String path){
        List<String> filesNames = new ArrayList<>();
        File dir = new File(path);
        for (File file: dir.listFiles()) {
            String fileNameWithOutExt = FilenameUtils.removeExtension(file.getName());
            filesNames.add(fileNameWithOutExt);
        }
        return filesNames;
    }

    public static List<String> listRemoteFilesFilter(String path, String remote){
        List<String> filesNames = new ArrayList<>();
        File dir = new File(path);
        File[] files = dir.listFiles();
        for (File file: files) {
            String fileNameWithOutExt = FilenameUtils.removeExtension(file.getName());
            filesNames.add(remote+ "\\" + fileNameWithOutExt);
        }
        return filesNames;

    }
    public static List<String> listOnlyFilesFilter(String path, String filter){
        List<String> filesNames = new ArrayList<>();
        File dir = new File(path);
        File[] files = dir.listFiles((File filePath, String name) -> !name.toLowerCase().endsWith(filter));
        for (File file: files) {
            if(file.isFile()) {
                String fileNameWithOutExt = FilenameUtils.removeExtension(file.getName());
                filesNames.add(fileNameWithOutExt);
            }
        }
        return filesNames;
    }

    public static List<String> listOnlyFoldersFilter(String path){
        List<String> filesNames = new ArrayList<>();
        File dir = new File(path);
        File[] files = dir.listFiles();
        for (File file: files) {
            if(!file.isFile()) {
                filesNames.add(file.getName());
            }
        }
        return filesNames;
    }
    public static List<String> listFilesAndDirs(String dirPath){
        List<String> filesNames = new ArrayList<>();
        File file = new File(dirPath);
        String[] dirs = file.list();
        for (String dir: dirs) {
            if (!dir.contains(".magit"))
                filesNames.add(dirPath + "\\" + dir);
        }

        return filesNames;
    }

    public static int numberOfFiles(String dirPath){
        List<String> filesNames = new ArrayList<>();

        File fileObj = FileUtils.getFile(dirPath);
        Collection<File> files = FileUtils.listFiles(fileObj,null, true);
        for (File file: files) {
                filesNames.add(file.getPath());
        }
        return filesNames.size();
    }

    public static boolean isDirectory(String path){
        File file = new File(path);

        if (file.exists())
            return file.isDirectory();

        return false;
    }

    public static boolean isFile(String path){
        File file = new File(path);

        if (file.exists())
            return file.isFile();

        return false;
    }

    public static void writeToFile(String filePath, String toWrite, boolean shouldZip) throws IOException, PathNotExistsException {
        if (shouldZip){
            String txtFilePath = filePath + ".txt";
            String zipFilePath = filePath + ".zip";
            File file = new File(txtFilePath);
            FileUtils.write(file, toWrite, "utf-8");
            zipFile(txtFilePath, zipFilePath);
            deleteFile(txtFilePath);
        }
        else{
            File file = new File(filePath);
            FileUtils.write(file, toWrite, "utf-8");
        }
    }

    public static void writeToFile(String filePath, Collection<String> toWrite) throws IOException {
        File file = new File(filePath);
        FileUtils.writeLines(file,"utf-8", toWrite,"\n");
    }

    public static void appendToFile(String filePath, String toWrite) throws IOException {
        File file = new File(filePath);
        FileUtils.write(file, toWrite, "utf-8", true);
    }

    public static String getFileName(String fullPath){
        File file = new File(fullPath);
        return file.getName();
    }

    public static boolean deleteDirectory(String directoryToBeDeleted){
        return deleteDirectory(directoryToBeDeleted, false);
    }

    public static boolean deleteDirectory(String directoryToBeDeleted, boolean shouldDeleteMagit) {
        File dir = new File(directoryToBeDeleted);
        File[] allContents = dir.listFiles();
        if (allContents != null) {
            for (File file : allContents) {
                if (shouldDeleteMagit || !file.getPath().contains(".magit"))
                    deleteDirectory(file.getPath(), shouldDeleteMagit);
            }
        }
        return dir.delete();
    }

    public static void deleteFile(String filePath) throws PathNotExistsException{
        File file = new File(filePath);
        if(file.exists())
            file.delete();
        else
            throw new PathNotExistsException(filePath);
    }

    private static String unzip(String zipFilePath, String destDir) throws IOException {
        FileInputStream fis;
        byte[] buffer = new byte[1024];

        fis = new FileInputStream(zipFilePath);
        ZipInputStream zis = new ZipInputStream(fis);
        ZipEntry ze = zis.getNextEntry();
        String fileName = ze.getName();

        String filePath = destDir + File.separator + fileName;
        File newFile = new File(filePath);

        //create directories for sub directories in zip
        new File(newFile.getParent()).mkdirs();
        FileOutputStream fos = new FileOutputStream(newFile);

        int len;
        while ((len = zis.read(buffer)) > 0) {
            fos.write(buffer, 0, len);
        }
        fos.close();

        //close this ZipEntry
        zis.closeEntry();

        //close last ZipEntry
        zis.closeEntry();
        zis.close();
        fis.close();

        return  filePath;
    }

    public static void zipFile(String srcPath, String destPath) throws IOException {
        FileOutputStream fos = new FileOutputStream(destPath);
        ZipOutputStream zipOut = new ZipOutputStream(fos);
        File fileToZip = new File(srcPath);
        FileInputStream fis = new FileInputStream(fileToZip);
        ZipEntry zipEntry = new ZipEntry(fileToZip.getName());
        zipOut.putNextEntry(zipEntry);
        byte[] bytes = new byte[1024];
        int length;
        while((length = fis.read(bytes)) >= 0) {
            zipOut.write(bytes, 0, length);
        }
        zipOut.close();
        fis.close();
        fos.close();
    }
}
