package models;

import enums.MergeCase;
import exceptions.PathNotExistsException;
import utils.FilesHandler;

import java.io.IOException;

public class Conflict {

    private MergeCase mergeCase;
    private MergeFile mergeFile;
    private String filePath;

    private String resolvedContent;

    public Conflict(String filePath, MergeCase mergeCase, MergeFile mergeFile){
        this.filePath = filePath;
        this.mergeFile = mergeFile;
        this.mergeCase = mergeCase;
    }

    public void resolveConflict(String resolveContent) {
        this.resolvedContent = resolveContent;
        try {
            FilesHandler.writeToFile(filePath, resolveContent, false);
        } catch (IOException | PathNotExistsException e) {
            e.printStackTrace();
        }
    }

    public String getResolvedContent() {
        return resolvedContent;
    }

    @Override
    public String toString() {
        return filePath;
    }

    public MergeFile getMergeFile() {
        return mergeFile;
    }
}
