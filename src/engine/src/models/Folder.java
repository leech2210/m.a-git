package models;

import enums.FileType;
import org.apache.commons.collections4.list.TreeList;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Folder extends GitObject {

    public TreeList<GitObject> files;

    public Folder(String name, String userName, String sha1, String dateCreated) {
        this(name, userName, sha1, dateCreated, new TreeList<>());
    }

    public Folder(String name, String userName, String sha1, String dateCreated, TreeList<GitObject> files) {
        super(name, userName, sha1, dateCreated);
        this.files = files;
    }

    @Override
    public FileType getType() {
        return FileType.FOLDER;
    }

    public void printFolder(){
        System.out.println("ROOT FOLDER: " + this.getFullName());
        System.out.println(this.getSha1());
        System.out.println(this.getUserName() + " " + this.getUpdatedDate());
        System.out.println("====================================================");

        printFolderRec(this);
    }

    private void printFolderRec(Folder folder){
        for (GitObject gitObj: folder.files) {
            System.out.println("[" + gitObj.getType().toString() + "]" + " ---> " + gitObj.getFullName());
            System.out.println(gitObj.getSha1());
            System.out.println(gitObj.getUserName() + " " + gitObj.getUpdatedDate());
            System.out.println("====================================================");
            if (gitObj.getType() == FileType.FOLDER)
                printFolderRec((Folder)gitObj);
        }
    }

    public List<GitObject> getFiles(boolean recursive){
        if (recursive){
            List<GitObject> list = new ArrayList<>();
            getFilesRec(this, list);
            return list;
        }
        else{
            return this.files.stream()
                    .filter(x -> x.getType() == FileType.FILE)
                    .collect(Collectors.toList());
        }
    }

    private void getFilesRec(Folder folder, List<GitObject> list){
        for (GitObject gitObj: folder.files) {
            if (gitObj.getType() == FileType.FOLDER)
                getFilesRec((Folder)gitObj, list);
            else
                list.add(gitObj);
        }
    }
}
