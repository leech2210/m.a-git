package models;

import utils.Consts;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

public class PullRequest {

    private String id;

    private String message;

    private String createdBy;
    private String assignedTo;

    private String targetBranch;
    private String targetSha1;

    private String baseBranch;
    private String baseSha1;

    private String dateCreated;

    private Status status;

    public PullRequest(String baseBranch,
                       String baseSha1,
                       String targetBranch,
                       String targetSha1,
                       String createdBy,
                       String assignedTo,
                       String message){
        this.id = UUID.randomUUID().toString();
        this.status = Status.OPEN;
        this.dateCreated = new SimpleDateFormat(Consts.DATE_FORMAT).format(new Date());

        this.message = message;
        this.baseBranch = baseBranch;
        this.baseSha1 = baseSha1;
        this.targetBranch = targetBranch;
        this.targetSha1 = targetSha1;
        this.createdBy = createdBy;
        this.assignedTo = assignedTo;
    }

    private PullRequest(){

    }

    @Override
    public String toString() {
        return String.join(",",
                                        id,
                                        createdBy,
                                        assignedTo,
                                        targetBranch,
                                        targetSha1,
                                        baseBranch,
                                        baseSha1,
                                        dateCreated,
                                        message,
                                        status.toString());
    }

    public static PullRequest parse(String toParse){
        String[] split = toParse.split(",");
        PullRequest pr = new PullRequest();
        pr.id = split[0];
        pr.createdBy = split[1];
        pr.assignedTo = split[2];
        pr.targetBranch = split[3];
        pr.targetSha1 = split[4];
        pr.baseBranch = split[5];
        pr.baseSha1 = split[6];
        pr.dateCreated = split[7];
        pr.message = split[8];
        String temp=split[9];
        String[] split1 = temp.split("\r");
        if(split1.length!=0)
            temp=split1[0];
        pr.status = Status.valueOf(temp);

        return pr;
    }

    public void setStatus(Status status){
        this.status = status;
    }

    public String getStatus(){ return status.toString();}

    public String getCreatedBy(){ return createdBy;}

    public String getId(){
        return id;
    }

    public String getTargetBranch(){
        return targetBranch;
    }

    public String getBaseBranch(){
        return baseBranch;
    }

    public String getBaseSha1(){
        return baseSha1;
    }

    public String getTargetSha1(){
        return targetSha1;
    }

}
