package models;

import enums.FileType;
import org.apache.commons.collections4.list.TreeList;
import utils.FilesHandler;

public abstract class GitObject {
    private String dateCreated;
    private String dateUpdated;
    private String sha1;
    private String userName;
    private String name; // path

    public GitObject(String name, String userName, String sha1, String dateCreated){
        this.name = name;
        this.userName = userName;
        this.sha1 = sha1;
        this.dateCreated = dateCreated;
        this.dateUpdated = dateCreated;
    }

    public String getSha1(){
        return sha1;
    }

    public void setSha1(String sha1){
        this.sha1 = sha1;
    }

    public String getFullName(){
        return name;
    }

    public String getShortName(){
        return FilesHandler.getFileName(name);
    }

    public String getUserName(){
        return userName;
    }

    public String getUpdatedDate(){
        return dateUpdated;
    }

    public void setUserName(String user){
        this.userName = user;
    }

    public void setDateUpdated(String date){
        this.dateUpdated = date;
    }

    public abstract FileType getType();

    public static String createMetadata(TreeList<GitObject> gitobjects){
        String content = "";
        for (GitObject f: gitobjects) {
            String fileName =  FilesHandler.getFileName(f.getFullName());
            String sha1 = f.getSha1();
            String type = f.getType().toString();
            String userName = f.getUserName();
            String date = f.getUpdatedDate();
            String fileMetadata = String.join(",",fileName, sha1, type , userName, date);
            content = content + fileMetadata + "\n";
        }

        return content;
    }

    @Override
    public String toString() {
        return this.getShortName();
    }
}
