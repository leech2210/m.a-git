package models;

import javafx.beans.property.SimpleStringProperty;

public class Branch {

    private final SimpleStringProperty name;

    public Branch(String name) {
        this.name = new SimpleStringProperty(name);
    }
    public String getName() {
        return name.get();
    }
    public void setName(String mName) {
        name.set(mName);
    }

    @Override
    public String toString() {
        return getName();
    }
}
