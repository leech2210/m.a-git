package models;

import enums.FileType;
import exceptions.PathNotExistsException;
import utils.Consts;
import utils.FilesHandler;

import java.io.IOException;

public class File extends GitObject {

    public File(String name, String userName, String sha1, String dateCreated) {
        super(name, userName, sha1, dateCreated);
    }

    @Override
    public FileType getType() {
        return FileType.FILE;
    }

    public String getFileContent() throws IOException, PathNotExistsException {
        String repoPath = FilesHandler.getFileDirectory(this.getFullName());
        return FilesHandler.readFile(repoPath + Consts.OBJECTS_FOLDER + "\\" + getSha1() + ".zip");
    }
}
