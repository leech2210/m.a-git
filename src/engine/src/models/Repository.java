package models;

public class Repository {

    private String name;
    private String location;
    private String remoteLocation;

    public Repository(String name, String location, String remoteLocation){
        this.name = name;
        this.location = location;
        this.remoteLocation = remoteLocation;
    }

    public String getName() {
        return name;
    }

    public String getLocation() {
        return location;
    }
    public void setLocation(String location) {
        this.location = location;
    }

   public String getRemoteLocation() {
        return remoteLocation;
    }

    @Override
    public String toString() {
        return "Repository Name: " + name + "\nRepository Location: " + location;
    }
}
