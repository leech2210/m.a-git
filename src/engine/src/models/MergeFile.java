package models;

public class MergeFile {
    public String sha1Ours;
    public String sha1Theirs;
    public String sha1Ancestor;

    public String contentOurs;
    public String contentTheirs;
    public String contentAncestor;

    public boolean inOurs(){
        return sha1Ours != null;
    }

    public boolean inTheirs(){
        return sha1Theirs != null;
    }

    public boolean inAncestor(){
        return sha1Ancestor != null;
    }

    public boolean equalOursTheirs(){
        return sha1Ours.equals(sha1Theirs);
    }

    public boolean equalOursAncestor(){
        return sha1Ours.equals(sha1Ancestor);
    }

    public boolean equalTheirsAncestor(){
        return sha1Ancestor.equals(sha1Theirs);
    }
}
