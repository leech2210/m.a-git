package models;

import java.util.Collection;

public class Changes {
    public Collection<String> DeletedFiles;
    public Collection<String> CreatedFiles;
    public Collection<String> UpdatedFiles;

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Deleted files: \n");
        if (DeletedFiles != null)
            sb.append(getFileNames(DeletedFiles));
        sb.append("\n=================================\n");

        sb.append("Created files: \n");
        if (CreatedFiles != null)
            sb.append(getFileNames(CreatedFiles));
        sb.append("\n=================================\n");

        sb.append("Updated files:\n");
        if (UpdatedFiles != null)
            sb.append(getFileNames(UpdatedFiles));
        sb.append("\n=================================\n");
        return sb.toString();
    }

    private static String getFileNames(Collection<String> files){
        StringBuilder sb = new StringBuilder();
        for (String file: files) {
            sb.append(file + "\n");
        }

        return sb.toString();
    }
}
