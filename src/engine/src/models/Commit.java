package models;

import puk.team.course.magit.ancestor.finder.CommitRepresentative;

public class Commit implements CommitRepresentative {
    private String dateStr;
    private String sha1;
    private String userName;

    private String commitMessage;
    private String sha1Root;
    private Commit firstParent;
    private Commit secondParent;

    private String branchName;

    public Commit(String commitMessage,
                  String userName,
                  String sha1,
                  String date,
                  String rootSha1,
                  Commit firstParent,
                  Commit secondParent){
        this.sha1 = sha1;
        this.sha1Root = rootSha1;
        this.userName = userName;
        this.commitMessage = commitMessage;
        this.firstParent = firstParent;
        this.secondParent = secondParent;

        this.dateStr = date;
    }

    public Commit(String commitMessage,
                  String userName,
                  String sha1,
                  String date,
                  String rootSha1,
                  Commit firstParent){
        this(commitMessage, userName, sha1, date, rootSha1, firstParent, null);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("sha1: " + sha1);
        sb.append("\n");
        sb.append("user: " + userName);
        sb.append("\n");
        sb.append("message: " + commitMessage);
        sb.append("\n");
        sb.append("date: " + dateStr);
        sb.append("\n");
        sb.append("parents:");
        sb.append("\n");
        String first = firstParent != null ? firstParent.sha1 : "";
        sb.append("1: " + first);
        sb.append("\n");
        String second = secondParent != null ? secondParent.sha1 : "";
        sb.append("2: " + second);
        return sb.toString();
    }

    public String getSha1Root(){
        return this.sha1Root;
    }

    public String getSha1(){
        return this.sha1;
    }

    @Override
    public String getFirstPrecedingSha1() {
        if (this.firstParent != null)
            return this.firstParent.sha1;
        else
            return "";
    }

    @Override
    public String getSecondPrecedingSha1() {
        if (this.secondParent != null)
            return this.secondParent.sha1;
        else
            return "";
    }

    public Commit getFirstParent() { return this.firstParent; }

    public Commit getSecondParent() { return this.secondParent; }

    public void print(){
        printRec(this);
    }

    private void printRec(Commit commit){
        if (commit == null)
            return;
        System.out.println("COMMIT SHA1: " + commit.sha1);
        System.out.println("Message: " + "[" + commit.commitMessage + "]");
        System.out.println("Author: " + "[" + commit.userName+ "]");
        System.out.println("Date: " + "[" + commit.dateStr + "]");
        System.out.println("===================================================");
        printRec(commit.firstParent);
    }


    public String createMetadata(){
        return String.join("\n", commitMessage, dateStr, userName, sha1Root, firstParent.sha1Root);
    }

    public void setSecondParent(Commit secondParent) {
        this.secondParent = secondParent;
    }

    public String getDate(){
        return dateStr;
    }

    public String getUserName(){
        return userName;
    }

    public String getCommiter(){
        return userName;
    }

    public String getCommitMessage(){
        return commitMessage;
    }
}
