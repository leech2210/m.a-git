package abstractions;

import exceptions.*;
import models.*;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

public interface Engine {

    // branches
    List<String> getAllBranches() throws RepoNotInitializedException;

    String getHeadBranch() throws RepoNotInitializedException;

    boolean addNewBranch(String branchName, String commitSha1, String branchType, String rbBranchName) throws IOException, PathNotExistsException, RepoNotInitializedException;

    String isPointedAsRB (String commitSha1) throws IOException, PathNotExistsException;

    boolean deleteBranch(String branchName) throws PathNotExistsException, RepoNotInitializedException;

    boolean switchBranch(String branchName) throws IOException, PathNotExistsException, RepoNotInitializedException;

    void resetHeadBranchToCommit(String commitSha1) throws IOException, PathNotExistsException, RepoNotInitializedException;

    Map<String, List<String>> getAllBranchesSha1() throws IOException, PathNotExistsException;
    Map<String, List<String>> getAllRTBBranchesSha1() throws IOException, PathNotExistsException;
    Map<String, List<String>> getAllRTBAndRbBranchesSha1() throws IOException, PathNotExistsException;
    // user
    void updateUser(String userName);

    String getActiveUser();

    String commit(String commitMessage) throws IOException, PathNotExistsException, RepoNotInitializedException;

    List<Conflict> getConflicts(String branchToMerge) throws IOException, PathNotExistsException;

    void merge(String branchToMerge, String commitMessage) throws RepoNotInitializedException, IOException, PathNotExistsException;

    boolean handleFFMerge(String branchToMerge) throws IOException, PathNotExistsException;

    Changes getStatus() throws PathNotExistsException, RepoNotInitializedException, IOException; // get all current changes

    boolean isOpenChanges() throws PathNotExistsException, RepoNotInitializedException, IOException;

    Folder getCurrentCommitFiles() throws RepoNotInitializedException; // get all current files

    List<String> getCommitFiles(String sha1) throws RepoNotInitializedException, IOException, PathNotExistsException;

    Folder getRootFolderCommit(String sha1) throws IOException, PathNotExistsException;

    Changes getDiff(String prevCommit, String commit) throws IOException, PathNotExistsException;

    // history
    Commit activeBranchHistory() throws RepoNotInitializedException;

    Commit getBranchCommit(String branch) throws IOException, PathNotExistsException;

    boolean isCommitExists(String commitSha1);

    // repository
    void initRepo(Repository repo, String headData) throws PathNotExistsException, IOException, InvalidMagitRepoExceptions;

    void changeRepo(String repoPath) throws PathNotExistsException, InvalidMagitRepoExceptions, IOException;

    void loadRepoFromXML (String xmlPath) throws JAXBException, XmlFileExceptions, IOException, PathNotExistsException, InvalidMagitRepoExceptions;
    void loadRepoFromXML (InputStream inStream, String repoPath) throws JAXBException, XmlFileExceptions, IOException, PathNotExistsException, InvalidMagitRepoExceptions;

    void xmlFileValidation (String xmlPath) throws JAXBException, XmlFileExceptions;
    void xmlFileValidation (InputStream inStream) throws JAXBException, XmlFileExceptions;

    Repository getXmlRepo(String xmlPath) throws JAXBException;
    Repository getXmlRepo(InputStream inStream) throws JAXBException;

    Repository getActiveRepo();

    // remote
    void clone(String remoteLocation, Repository localLocation) throws IOException, PathNotExistsException, RepoNotInitializedException;

    void fetch() throws IOException, PathNotExistsException;

    void pull() throws IOException, PathNotExistsException, RemoteException, RepoNotInitializedException;

    void push() throws IOException, PathNotExistsException, RemoteException, RepoNotInitializedException;

    // pull request
    String createNewPr(String targetBranch, String baseBranch, String message, String assignedUser)
            throws IOException, PathNotExistsException;

    void closePr(String id, Status status) throws IOException, PathNotExistsException;

    List<Commit> getPrCommits(String id) throws IOException, PathNotExistsException;
    List<Commit> getBranchCommits(String branchName) throws IOException, PathNotExistsException;
    List<PullRequest> getUserPullRequests() throws IOException, PathNotExistsException;
}
