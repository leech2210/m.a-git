import abstractions.Engine;
import controllers.*;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import managers.MagitEngine;

import java.io.IOException;
import java.net.URL;

public class UI extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws IOException {
        Engine engine = new MagitEngine();

        // repo controller
        FXMLLoader repoLoader = new FXMLLoader();
        URL repoFxml = getClass().getResource("views/RepoPage.fxml");
        repoLoader.setLocation(repoFxml);
        Parent repoRoot = repoLoader.load();

        RepoController repoController = repoLoader.getController();
        repoController.setPrimaryStage(primaryStage);
        repoController.setEngine(engine);

        // branches controller
        FXMLLoader branchesLoader = new FXMLLoader();
        URL branchesFxml = getClass().getResource("views/BranchesPage.fxml");
        branchesLoader.setLocation(branchesFxml);
        Parent branchesRoot = branchesLoader.load();

        BranchesController branchesController = branchesLoader.getController();
        branchesController.setEngine(engine);

        // commit tree controller
        FXMLLoader commitLoader = new FXMLLoader();
        URL commitFxml = getClass().getResource("views/CommitTree.fxml");
        commitLoader.setLocation(commitFxml);
        Parent commitRoot = commitLoader.load();

        CommitGraphController commitController = commitLoader.getController();
        commitController.setEngine(engine);
        commitController.setStage(primaryStage);

        // merge controller
        FXMLLoader mergeLoader = new FXMLLoader();
        URL mergeFxml = getClass().getResource("views/AllConflicts.fxml");
        mergeLoader.setLocation(mergeFxml);
        Parent mergeRoot = mergeLoader.load();

        MergeController mergeController = mergeLoader.getController();
        mergeController.setEngine(engine);
        mergeController.setStage(primaryStage);

        // remote controller
        FXMLLoader remoteLoader = new FXMLLoader();
        URL remoteFxml = getClass().getResource("views/RemotePage.fxml");
        remoteLoader.setLocation(remoteFxml);
        Parent remoteRoot = remoteLoader.load();

        RemoteController remoteController = remoteLoader.getController();
        remoteController.setEngine(engine);
        remoteController.setPrimaryStage(primaryStage);

        // home controller (main)
        FXMLLoader loader = new FXMLLoader();
        URL mainFXML = getClass().getResource("views/Home.fxml");
        loader.setLocation(mainFXML);
        Parent root = loader.load();

        HomeController homeController = loader.getController();
        homeController.setPrimaryStage(primaryStage);
        homeController.setEngine(engine);
        homeController.setRepo(repoController, repoRoot);
        homeController.setBranches(branchesController, branchesRoot);
        homeController.bindButtonToInitProp(repoController.repoInitProperty(), remoteController.repoInitProperty());
        homeController.setCommit(commitController, commitRoot);
        homeController.setMerge(mergeController, mergeRoot);
        homeController.setRemote(remoteController, remoteRoot);

        Scene scene = new Scene(root, 900, 600);
        primaryStage.setTitle("My Magit Application");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
