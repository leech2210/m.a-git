package utils;

import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;

import java.io.File;
import java.util.List;
import java.util.Optional;

public class Utils {

    public static void showErrorAlert(String errorMessage){
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error Dialog");
        alert.setHeaderText(null);
        alert.setContentText(errorMessage);

        alert.showAndWait();
    }

    public static void showSuccessAlert(String text){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Magit Dialog");
        alert.setHeaderText(null);
        alert.setContentText(text);

        alert.showAndWait();
    }


    public static void showWarningAlert(String text){
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Magit Dialog");
        alert.setHeaderText(null);
        alert.setContentText(text);

        alert.showAndWait();
    }

    public static ButtonType openYesNoDialog(String title, String text){
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle(title);
        alert.setContentText(text);
        ButtonType yesButton = new ButtonType("Yes", ButtonBar.ButtonData.YES);
        ButtonType noButton = new ButtonType("No", ButtonBar.ButtonData.NO);
        alert.getButtonTypes().setAll(yesButton, noButton);
        return alert.showAndWait().get();
    }

    public static String showInputTextDialog(String title, String headerText, String content) {

        TextInputDialog dialog = new TextInputDialog("");

        dialog.setTitle(title);
        dialog.setHeaderText(headerText);
        dialog.setContentText(content);

        Optional<String> result = dialog.showAndWait();
        if (result.isPresent())
            return result.get();
        return "";
    }

    public static String showChooseDialog(List<String> choices,
                                          String title,
                                          String text,
                                          String content){
        ChoiceDialog<String> dialog = new ChoiceDialog<>("", choices);

        dialog.setTitle(title);
        dialog.setHeaderText(text);
        dialog.setContentText(content);

        Optional<String> result = dialog.showAndWait();
        if (result.isPresent())
            return result.get();
        return "";
    }

    public static void showTextDialog(String title, String headerText, String mainMessage){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(title);
        alert.setHeaderText(headerText);

        TextArea textArea = new TextArea(mainMessage);
        textArea.setEditable(false);
        textArea.setWrapText(true);

        textArea.setMaxWidth(Double.MAX_VALUE);
        textArea.setMaxHeight(Double.MAX_VALUE);
        GridPane.setVgrow(textArea, Priority.ALWAYS);
        GridPane.setHgrow(textArea, Priority.ALWAYS);

        GridPane expContent = new GridPane();
        expContent.setMaxWidth(Double.MAX_VALUE);
        expContent.add(textArea, 0, 1);

        alert.getDialogPane().setContent(textArea);
        alert.showAndWait();
    }

    public static String openDirectory(Stage primaryStage) {
        DirectoryChooser dirChooser = new DirectoryChooser();
        dirChooser.setTitle("Select directory");
        File selectedFile = dirChooser.showDialog(primaryStage);
        if (selectedFile == null) {
            return "";
        }

        return selectedFile.getAbsolutePath();
    }
}
