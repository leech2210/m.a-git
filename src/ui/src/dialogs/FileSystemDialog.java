package dialogs;

import enums.FileType;
import exceptions.PathNotExistsException;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import models.File;
import models.Folder;
import models.GitObject;
import utils.Consts;
import utils.FilesHandler;
import utils.Utils;

import java.io.IOException;

public class FileSystemDialog extends Stage {
    private static final int WIDTH_DEFAULT = 300;
    private static final int HEIGHT_DEFAULT = 250;

    private String repoPath;
    private static FileSystemDialog popup;

    private FileSystemDialog(Folder folder, String repoPath){
        this.repoPath = repoPath;
        setResizable(false);
        initModality(Modality.APPLICATION_MODAL);
        initStyle(StageStyle.TRANSPARENT);

        addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent ke) {
                if (ke.getCode() == KeyCode.ESCAPE) {
                    FileSystemDialog.this.close();
                }
            }
        });

        TreeItem<GitObject> rootItem = CreateTree(folder);
        rootItem.setExpanded(true);
        TreeView<GitObject> tree = new TreeView<> (rootItem);
        EventHandler<MouseEvent> mouseEventHandle = (MouseEvent event) -> {
            handleClick(event);
        };

        tree.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEventHandle);

        BorderPane root = new BorderPane();
        root.setCenter(tree);

        Button button = new Button("Close");
        button.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                FileSystemDialog.this.close();
            }
        });

        BorderPane bottom = new BorderPane();
        bottom.setRight(button);
        root.setBottom(bottom);

        setScene(new Scene(root, 300, 250));
    }

    private void handleClick(MouseEvent event) {
        try {
            TreeView treeView = (TreeView)event.getSource();
            TreeItem<GitObject> treeItem = (TreeItem<GitObject>)treeView.getSelectionModel().getSelectedItem();
            if (treeItem != null){
                GitObject obj = treeItem.getValue();
                if (obj.getType() == FileType.FILE){
                    File file = (File) obj;
                    String sha1 = file.getSha1();
                    String content = FilesHandler.readFile(repoPath +
                            Consts.OBJECTS_FOLDER + "\\" + sha1 + ".zip");
                    Utils.showTextDialog("File Content", "content:", content);
                }
            }
        } catch (PathNotExistsException | IOException e) {
            e.printStackTrace();
        }
    }

    private static TreeItem<GitObject> CreateTree(Folder folder){
        TreeItem<GitObject> item = new TreeItem<>(folder);
        for (GitObject obj: folder.files) {
            if (obj.getType() == FileType.FILE){
                TreeItem<GitObject> file = new TreeItem<>(obj);
                item.getChildren().add(file);
            }
            else {
                TreeItem<GitObject> currFolder = CreateTree((Folder)obj);
                item.getChildren().add(currFolder);
            }
        }

        return item;
    }

    public static void show(Stage owner, Folder folder, String repoPath) {
        popup = new FileSystemDialog(folder, repoPath);

        popup.setWidth(WIDTH_DEFAULT);
        popup.setHeight(HEIGHT_DEFAULT);

        // make sure this stage is centered on top of its owner
        popup.setX(owner.getX() + (owner.getWidth() / 2 - popup.getWidth() / 2));
        popup.setY(owner.getY() + (owner.getHeight() / 2 - popup.getHeight() / 2));

        popup.showAndWait();
    }
}
