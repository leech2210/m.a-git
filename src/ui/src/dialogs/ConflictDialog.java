package dialogs;

import controllers.ConflictController;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;
import java.net.URL;

public class ConflictDialog extends Stage {
    private static final int WIDTH_DEFAULT = 700;
    private static final int HEIGHT_DEFAULT = 600;

    private static String result;
    private static Label label;
    private static ConflictDialog popup;

    private ConflictDialog(String ours, String theirs, String ancestor) {
        setResizable(false);
        initModality(Modality.APPLICATION_MODAL);
        initStyle(StageStyle.TRANSPARENT);

//        addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
//            @Override
//            public void handle(KeyEvent ke) {
//                if (ke.getCode() == KeyCode.ESCAPE) {
//                    ConflictDialog.this.close();
//                }
//            }
//        });

        FXMLLoader loader = new FXMLLoader();
        URL fxml = getClass().getResource("../views/Conflicts.fxml");
        loader.setLocation(fxml);
        AnchorPane root = null;
        try {
            root = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }

        ConflictController controller = loader.getController();
        controller.setTxtAncestor(ancestor);
        controller.setTxtOurs(ours);
        controller.setTxtTheirs(theirs);

        Scene scene = new Scene(root, 320, 150);
        Button btnResolve = (Button) scene.lookup("#btnResolve");

        btnResolve.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                result = controller.getTxtResolved();
                ConflictDialog.this.close();
            }
        });
        btnResolve.setAlignment(Pos.BOTTOM_CENTER);

        Button btnCancel = (Button) scene.lookup("#btnCancel");
        btnCancel.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                ConflictDialog.this.close();
            }
        });
        btnCancel.setAlignment(Pos.BOTTOM_CENTER);

        setScene(scene);
    }

    public static String show(Stage owner, String ours, String theirs, String ancestor) {
        popup = new ConflictDialog(ours, theirs, ancestor);

        popup.setWidth(WIDTH_DEFAULT);
        popup.setHeight(HEIGHT_DEFAULT);

        popup.setX(owner.getX() + (owner.getWidth() / 2 - popup.getWidth() / 2));
        popup.setY(owner.getY() + (owner.getHeight() / 2 - popup.getHeight() / 2));

        popup.showAndWait();

        return result;
    }
}
