package dialogs;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import utils.Utils;

import java.io.IOException;

public class CommitMessageDialog extends Stage {
    private static final int WIDTH_DEFAULT = 400;
    private static final int HEIGHT_DEFAULT = 300;

    private static String result;
    private static CommitMessageDialog popup;

    private CommitMessageDialog() {
        setResizable(false);
        initModality(Modality.APPLICATION_MODAL);
        initStyle(StageStyle.TRANSPARENT);

        addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent ke) {
                if (ke.getCode() == KeyCode.ESCAPE) {
                    CommitMessageDialog.this.close();
                }
            }
        });

        VBox root = new VBox();
        root.setPadding(new Insets(10));
        root.setSpacing(5);

        root.getChildren().add(new Label("Enter commit message:"));

        TextArea textArea = new TextArea();
        root.getChildren().add(textArea);

        Button button = new Button("Commit!");
        button.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

                result = textArea.getText();
                if (result.isEmpty())
                    Utils.showErrorAlert("You must insert commit message!");
                else
                    CommitMessageDialog.this.close();
            }
        });
        button.setAlignment(Pos.CENTER);

        root.getChildren().add(button);

        Scene scene = new Scene(root, 320, 150);
        setScene(scene);
    }


    public static String show(Stage owner) throws IOException {
        popup = new CommitMessageDialog();

        popup.setWidth(WIDTH_DEFAULT);
        popup.setHeight(HEIGHT_DEFAULT);

        // make sure this stage is centered on top of its owner
        popup.setX(owner.getX() + (owner.getWidth() / 2 - popup.getWidth() / 2));
        popup.setY(owner.getY() + (owner.getHeight() / 2 - popup.getHeight() / 2));

        popup.showAndWait();

        return result;
    }
}