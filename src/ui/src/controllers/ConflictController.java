package controllers;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.layout.Pane;

import java.net.URL;
import java.util.ResourceBundle;

public class ConflictController implements Initializable {


    @FXML
    private Pane pnlOverview;

    @FXML
    private TextArea txtOurs;

    @FXML
    private TextArea txtTheirs;

    @FXML
    private TextArea txtAncestor;

    @FXML
    private TextArea txtResolved;

    @FXML
    private Button btnResolve;

    public ConflictController(){

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    public void setTxtTheirs(String txtTheirs) {
        this.txtTheirs.setText(txtTheirs);
    }

    public void setTxtOurs(String txtOurs) {
        this.txtOurs.setText(txtOurs);
    }

    public void setTxtAncestor(String txtAncestor) {
        this.txtAncestor.setText(txtAncestor);
    }

    public String getTxtResolved() {
        return txtResolved.getText();
    }
}
