package controllers;

import abstractions.Engine;
import exceptions.InvalidMagitRepoExceptions;
import exceptions.PathNotExistsException;
import exceptions.RemoteException;
import exceptions.RepoNotInitializedException;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import models.Repository;
import utils.Utils;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class RemoteController implements Initializable {

    @FXML private TextField localPath;
    @FXML private TextField remotePath;

    @FXML private Button btnClone;
    @FXML private Button btnFetch;
    @FXML private Button btnPull;
    @FXML private Button btnPush;

    private SimpleStringProperty selectedSrcProperty;
    private SimpleStringProperty selectedDestProperty;
    private SimpleBooleanProperty repoInit;
    private Engine engine;
    private Stage primaryStage;

    public RemoteController(){
        selectedSrcProperty = new SimpleStringProperty();
        selectedDestProperty = new SimpleStringProperty();
        repoInit = new SimpleBooleanProperty(false);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        localPath.textProperty().bind(selectedDestProperty);
        remotePath.textProperty().bind(selectedSrcProperty);
    }

    @FXML
    public void dstOpenDirectory() {
        selectedDestProperty.set(Utils.openDirectory(primaryStage));
    }

    @FXML
    public void srcOpenDirectory() {
        selectedSrcProperty.set(Utils.openDirectory(primaryStage));
    }

    @FXML
    public void clone(ActionEvent event) throws RemoteException {
        try{
            String repoRemotePath = selectedSrcProperty.get();
            if(repoRemotePath == null)
                Utils.showErrorAlert("you didn't insert remote path");
            else {
                String repoLocalPath = selectedDestProperty.get();
                if (repoLocalPath == null)
                    Utils.showErrorAlert("you didn't insert local path");
                else {
                    String folderName = Utils.showInputTextDialog("New Repo",
                            "Enter folder name:",
                            "Folder Name:");
                    if (folderName.isEmpty())
                        Utils.showErrorAlert("you didn't insert folder Name");
                    else {
                        String repoName = Utils.showInputTextDialog("New Repo",
                                "Enter repo name:",
                                "Name:");
                        if (repoName.isEmpty())
                            Utils.showErrorAlert("you didn't insert repo Name");
                        else {
                            Repository repo = new Repository(repoName, repoLocalPath + "\\" + folderName, repoRemotePath);

                            engine.clone(repoRemotePath, repo);
                            engine.changeRepo(repo.getLocation());

                            this.repoInit.set(true);
                            Utils.showSuccessAlert("clone successfully!");
                        }
                    }
                }
            }
        } catch ( RuntimeException | RepoNotInitializedException | IOException | PathNotExistsException
                | InvalidMagitRepoExceptions e) {
            Utils.showErrorAlert(e.getMessage());
        }
    }

    @FXML
    public void fetch(ActionEvent event) {
        try{
            engine.fetch();
            Utils.showSuccessAlert("Fetch successfully!");
        } catch (IOException | PathNotExistsException e) {
            Utils.showErrorAlert(e.getMessage());
        }

    }

    @FXML
    public void pull(ActionEvent event) {
        try{
            engine.pull();
            Utils.showSuccessAlert("Pull successfully!");
        } catch (IOException | PathNotExistsException | RepoNotInitializedException
                |RemoteException e) {
            Utils.showErrorAlert(e.getMessage());
        }
    }

    @FXML
    public void push(ActionEvent event) {
         try{
        engine.push();
        Utils.showSuccessAlert("Push successfully!");
         } catch (IOException | PathNotExistsException | RepoNotInitializedException
                |RemoteException e) {
        Utils.showErrorAlert(e.getMessage());
     }
    }

    public void setEngine(Engine engine) {
        this.engine = engine;
    }

    public void setPrimaryStage(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }

    public SimpleBooleanProperty repoInitProperty() {
        return repoInit;
    }
}
