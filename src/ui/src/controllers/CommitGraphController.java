package controllers;

import abstractions.Engine;
import com.fxgraph.graph.Graph;
import com.fxgraph.graph.Model;
import com.fxgraph.graph.PannableCanvas;
import dialogs.CommitMessageDialog;
import exceptions.PathNotExistsException;
import exceptions.RepoNotInitializedException;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.stage.Stage;
import models.*;
import utils.Utils;

import java.io.IOException;
import java.net.URL;
import java.util.*;

public class CommitGraphController implements Initializable {

    @FXML
    private ScrollPane scrollPaneTree;

    @FXML
    private Button btnCommit;

    @FXML
    private Button btnStatus;

    @FXML
    private Label lblCommitTree;

    private Engine engine;

    private Stage stage;


    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @FXML
    public void getStatus() throws PathNotExistsException, RepoNotInitializedException, IOException {
        Changes changes = engine.getStatus();
        Utils.showTextDialog("git status", "working copy current status:", changes.toString());
    }

    @FXML
    public void commit() throws IOException, PathNotExistsException,
            RepoNotInitializedException {
        if (!engine.isOpenChanges()){
            Utils.showErrorAlert("there are no changes to commit!");
            return;
        }
        String commitMessage = CommitMessageDialog.show(stage);
        String commitSha1 = engine.commit(commitMessage);
        Utils.showSuccessAlert("all the changes are committed - " + commitSha1);
        drawCommitTree();
    }

    public void drawCommitTree(){
        Graph tree = new Graph();
        PannableCanvas canvas = tree.getCanvas();
        createCommits(tree);

        Platform.runLater(() -> {
            tree.getUseViewportGestures().set(false);
            tree.getUseNodeGestures().set(false);
        });

        scrollPaneTree.setContent(canvas);
    }
//check with lee
    private void createCommits(Graph graph) {
        try {
            final Model model = graph.getModel();
            graph.beginUpdate();

            Map<String, List<String>> branches = engine.getAllBranchesSha1();
            Map<String, CommitNode> commits = new HashMap<>();

            for (Map.Entry<String, List<String>>  branch: branches.entrySet()) {
                String branchesStr = String.join(", ",branch.getValue());
                Commit currCommit = engine.getBranchCommit(branch.getValue().get(0));
                if (currCommit != null){
                    if (commits.containsKey(currCommit.getSha1())) {
                        commits.get(currCommit.getSha1()).setBranch(branchesStr);
                        continue;
                    }

                    CommitNode currCommitNode = new CommitNode(currCommit, branchesStr, engine, stage);
                    commits.put(currCommit.getSha1(), currCommitNode);

                    createTree(currCommitNode, commits);
                }
            }

            // get root of tree
            for (Map.Entry<String, CommitNode>  commit: commits.entrySet()){
                Commit currCommit = commit.getValue().getCommit();
                if (currCommit.getFirstParent() == null && currCommit.getSecondParent() == null){
                    commit.getValue().addGraphics(model, 0, 0);
                    break;
                }
            }

            graph.endUpdate();
            graph.layout(new CommitTreeLayout());

        } catch (IOException | PathNotExistsException e) {
            e.printStackTrace();
        }
    }

    private void createTree(CommitNode cn, Map<String, CommitNode> commitsMap){
        Commit commit = cn.getCommit();

        if (commit.getFirstParent() != null){
            String parentSha1 = commit.getFirstParent().getSha1();
            if (commitsMap.containsKey(parentSha1)) {
                CommitNode parent = commitsMap.get(parentSha1);
                parent.addChild(cn);
            }
            else{
                CommitNode parent = new CommitNode(commit.getFirstParent(), engine, stage);
                commitsMap.put(parentSha1, parent);
                parent.addChild(cn);
                createTree(parent, commitsMap);
            }
        }
        if (commit.getSecondParent() != null){
            String parentSha1 = commit.getSecondParent().getSha1();
            if (commitsMap.containsKey(parentSha1)) {
                CommitNode parent = commitsMap.get(parentSha1);
                parent.addChild(cn);
            }
            else{
                CommitNode parent = new CommitNode(commit.getSecondParent(), engine, stage);
                commitsMap.put(parentSha1, parent);
                parent.addChild(cn);
                createTree(parent, commitsMap);
            }
        }
    }

    public void setEngine(Engine engine) {
        this.engine = engine;
    }
}
