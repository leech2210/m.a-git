package controllers;

import abstractions.Engine;
import exceptions.InvalidMagitRepoExceptions;
import exceptions.PathNotExistsException;
import exceptions.XmlFileExceptions;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import models.Repository;
import tasks.LoadXmlTask;
import utils.FilesHandler;
import utils.Utils;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

import static utils.Consts.GIT_FOLDER;

public class RepoController implements Initializable {

    @FXML
    private Pane pnlRepository;

    @FXML private TextField selectedFolderName; // change repo
    @FXML private TextField selectedFileName; // xml
    @FXML private TextField selectedInitFolderName; // init repo

    @FXML private Label lblRepoName;
    @FXML private Label lblRepoLocation;

    private Stage primaryStage;

    private SimpleStringProperty selectedFileProperty;
    private SimpleStringProperty selectedInitFolderProperty;
    private SimpleStringProperty selectedFolderProperty;

    private SimpleStringProperty repoLocationProperty;
    private SimpleStringProperty repoNameProperty;

    private SimpleBooleanProperty repoInit;

    private Engine engine;

    public RepoController(){
        selectedFileProperty = new SimpleStringProperty();
        selectedFolderProperty = new SimpleStringProperty();
        selectedInitFolderProperty = new SimpleStringProperty();
        repoLocationProperty = new SimpleStringProperty("Repository Location - ");
        repoNameProperty = new SimpleStringProperty("Repository Name - ");
        repoInit = new SimpleBooleanProperty(false);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        selectedFileName.textProperty().bind(selectedFileProperty);
        selectedFolderName.textProperty().bind(selectedFolderProperty);
        selectedInitFolderName.textProperty().bind(selectedInitFolderProperty);
        lblRepoLocation.textProperty().bind(repoLocationProperty);
        lblRepoName.textProperty().bind(repoNameProperty);
    }

    public void setPrimaryStage(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }

    public void setEngine(Engine engine){
        this.engine = engine;
    }

    @FXML
    public void initOpenDirectoryAction(){
        selectedInitFolderProperty.set(Utils.openDirectory(primaryStage));
    }

    @FXML
    public void changeOpenDirectoryAction(){
        selectedFolderProperty.set(Utils.openDirectory(primaryStage));
    }

    @FXML
    public void openXmlFileAction() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("select xml file");
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("xml files", "*.xml"));
        File selectedFile = fileChooser.showOpenDialog(primaryStage);
        if (selectedFile == null) {
            return;
        }

        String absolutePath = selectedFile.getAbsolutePath();
        selectedFileProperty.set(absolutePath);

        //isFileSelected.set(true);
    }

    @FXML
    private void loadFromXml() {
        try {
            String xmlFilePath = selectedFileProperty.get();
            if (xmlFilePath == null)
                Utils.showErrorAlert("you didn't insert path");
            else{
                Repository repo = engine.getXmlRepo(xmlFilePath);
                String repoPath = repo.getLocation();
                engine.xmlFileValidation(xmlFilePath);

                if (FilesHandler.isDirectory(repoPath + GIT_FOLDER))
                    showRepoExistsAlert(repo, xmlFilePath);
                else
                    //engine.loadRepoFromXML(xmlFilePath);
                    RunLoadInBackground(xmlFilePath, repo);
            }
        } catch (XmlFileExceptions xmlFileExceptions) {
            Utils.showErrorAlert("The xml file is invalid: " +
                    "\n" + xmlFileExceptions.getMessage());
        } catch (Exception e) {
            Utils.showErrorAlert(e.getMessage());
        }
    }

    private void RunLoadInBackground(String xmlFilePath, Repository repo){
        Task task = new LoadXmlTask(xmlFilePath, engine);
        new Thread(task).start();
        task.setOnSucceeded(e -> {
            updateRepoDetails(repo);
            Utils.showSuccessAlert("repository - " + repo.getLocation() + " loaded successfully!");
        });
    }

    @FXML
    private void changeRepo() throws IOException, PathNotExistsException {
        String repoPath = selectedFolderProperty.get();

        if (repoPath == null)
            Utils.showErrorAlert("you didn't insert path");
        else{
            try {
                engine.changeRepo(repoPath);
                Repository repo = engine.getActiveRepo();

                updateRepoDetails(repo);
                Utils.showSuccessAlert("repository - "+ repoPath + " switched successfully!");

            } catch (InvalidMagitRepoExceptions e2) {
                Utils.showErrorAlert("repository - " + repoPath + " is not a magit repository");
            }
        }
    }

    @FXML
    private void initRepo(){
        String repoPath = selectedInitFolderProperty.get();
        if (repoPath == null)
            Utils.showErrorAlert("you didn't insert path");
        else{
            String folderName = Utils.showInputTextDialog("New Repo",
                    "Enter folder name:",
                    "Folder Name:");
            String repoName = Utils.showInputTextDialog("New Repo",
                    "Enter repo name:",
                    "Name:");
            Repository repo = new Repository(repoName, repoPath + "\\" + folderName, null);

            try {
                engine.initRepo(repo,"master");
                updateRepoDetails(repo);
                Utils.showSuccessAlert("repository - "+ repoPath + " initialized successfully!");
            } catch (IOException | InvalidMagitRepoExceptions | PathNotExistsException e) {
                Utils.showSuccessAlert(e.getMessage());
            }
        }
    }

    private void updateRepoDetails(Repository repo){
        this.repoInit.set(true);
        String repoPath = repo.getLocation();
        String repoName = repo.getName();

        repoLocationProperty.set("Repository Location - " + repoPath);
        repoNameProperty.set("Repository Name - " + repoName);
    }

    private void showRepoExistsAlert(Repository repo, String xmlFilePath)
            throws Exception {
        String repoPath = repo.getLocation();
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Magit Dialog");
        alert.setHeaderText("folder .magit is already exists");
        alert.setContentText("Choose your option:");

        ButtonType buttonContinue = new ButtonType("continue with the existing folder");
        ButtonType buttonDeleteInit = new ButtonType("delete and initialize new repo");
        ButtonType buttonTypeCancel = new ButtonType("Cancel", ButtonBar.ButtonData.CANCEL_CLOSE);

        alert.getButtonTypes().setAll(buttonContinue, buttonDeleteInit, buttonTypeCancel);

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == buttonContinue){
            engine.changeRepo(repoPath);
        } else if (result.get() == buttonDeleteInit) {
            FilesHandler.deleteDirectory(repoPath, true);
            RunLoadInBackground(xmlFilePath, repo);
        } else {
            throw new Exception("you didn't choose option - failed to load repo");
        }
    }

    public boolean isRepoInit() {
        return repoInit.get();
    }

    public SimpleBooleanProperty repoInitProperty() {
        return repoInit;
    }
}

