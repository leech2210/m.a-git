package controllers;

import abstractions.Engine;
import exceptions.RepoNotInitializedException;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextInputDialog;
import javafx.stage.Stage;
import models.Branch;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

public class HomeController implements Initializable {

    @FXML
    private Button btnRepository;

    @FXML
    private Button btnBranches;

    @FXML
    private Button btnCommits;

    @FXML
    private Button btnMerge;

    @FXML
    private Button btnRemote;

    @FXML
    private Button btnUser;

    @FXML
    private ScrollPane pnlMain;

    @FXML
    private Label lblUserName;

    private Stage primaryStage;

    private Engine engine;

    private RepoController repoController;
    private Parent rootRepo;

    private BranchesController branchesController;
    private Parent rootBranches;

    private CommitGraphController commitsController;
    private Parent rootCommits;

    private MergeController mergeController;
    private Parent rootMerge;

    private RemoteController remoteController;
    private Parent rootRemote;

    public HomeController()
    {
    }

    @Override
    public void initialize(URL location, ResourceBundle resources)
    {

    }

    public void bindButtonToInitProp(SimpleBooleanProperty prop1, SimpleBooleanProperty prop2){
        btnBranches.disableProperty().bind(Bindings.not(Bindings.or(prop1, prop2)));
        btnCommits.disableProperty().bind(Bindings.not(Bindings.or(prop1, prop2)));
        btnMerge.disableProperty().bind(Bindings.not(Bindings.or(prop1, prop2)));
    }

    public void setPrimaryStage(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }

    public void handleClicks(ActionEvent actionEvent) throws RepoNotInitializedException {
        if (actionEvent.getSource() == btnRepository) {
            loadComponent(pnlMain, rootRepo);
        }
        if (actionEvent.getSource() == btnBranches) {
            loadComponent(pnlMain, rootBranches);
            List<Branch> branches = getAllBranches();
            branchesController.updateBranchesList(branches);
            branchesController.updateHeadBranch();
        }
        if (actionEvent.getSource() == btnCommits){
            loadComponent(pnlMain, rootCommits);
            commitsController.drawCommitTree();
        }
        if (actionEvent.getSource() == btnMerge){
            loadComponent(pnlMain, rootMerge);
            List<Branch> branches = getAllBranches();
            mergeController.updateBranchesList(branches);
        }
        if (actionEvent.getSource() == btnRemote){
            loadComponent(pnlMain, rootRemote);
        }
        if (actionEvent.getSource() == btnUser){
            showInputTextDialog();
            engine.updateUser(this.lblUserName.getText());
        }
    }

    private void loadComponent(ScrollPane pnl, Parent root) {
        pnl.setContent(root);
    }

    private void showInputTextDialog() {

        TextInputDialog dialog = new TextInputDialog("Administrator");

        dialog.setTitle("User Name");
        dialog.setHeaderText("Enter your name:");
        dialog.setContentText("Name:");

        Optional<String> result = dialog.showAndWait();

        result.ifPresent(name -> {
            this.lblUserName.setText(name);
        });
    }

    public void setEngine(Engine engine) {
        this.engine = engine;
    }

    public void setRepo(RepoController repoController, Parent rootRepo) {
        this.repoController = repoController;
        this.rootRepo = rootRepo;
    }

    public void setBranches(BranchesController branchesController, Parent rootBranches){
        this.branchesController = branchesController;
        this.rootBranches = rootBranches;
    }

    public void setCommit(CommitGraphController commitController, Parent rootCommits){
        this.commitsController = commitController;
        this.rootCommits = rootCommits;
    }

    public void setMerge(MergeController mergeController, Parent rootMerge){
        this.mergeController = mergeController;
        this.rootMerge = rootMerge;
    }

    public void setRemote(RemoteController remoteController, Parent rootRemote){
        this.remoteController = remoteController;
        this.rootRemote = rootRemote;
    }

    private List<Branch> getAllBranches() throws RepoNotInitializedException {
        List<String> branchesStr = engine.getAllBranches();

        List<Branch> branches = new ArrayList<>();
        for (String branchStr: branchesStr)
            branches.add(new Branch(branchStr));

        return branches;
    }
}