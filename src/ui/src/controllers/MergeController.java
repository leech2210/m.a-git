package controllers;

import abstractions.Engine;
import dialogs.CommitMessageDialog;
import dialogs.ConflictDialog;
import exceptions.PathNotExistsException;
import exceptions.RepoNotInitializedException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseButton;
import javafx.stage.Stage;
import models.Branch;
import models.Conflict;
import models.MergeFile;
import utils.Utils;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class MergeController implements Initializable {

    @FXML private ListView<Conflict> listViewConflicts;
    @FXML private ComboBox<Branch> cmbBranch;

    private Engine engine;
    private Stage stage;
    private ObservableList<Conflict> listConflicts;
    private ObservableList<Branch> listBranches;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        listViewConflicts.setOnMouseClicked(event -> {
            if(event.getButton().equals(MouseButton.PRIMARY)){
                if(event.getClickCount() == 2){
                    Conflict selectedItem = listViewConflicts.getSelectionModel().getSelectedItem();
                    int selectedIndex = listViewConflicts.getSelectionModel().getSelectedIndex();
                    MergeFile mergeFile = selectedItem.getMergeFile();
                    String resolved = ConflictDialog.show(stage, mergeFile.contentOurs, mergeFile.contentTheirs,
                            mergeFile.contentAncestor);
                    if (resolved != null && !resolved.isEmpty()){
                        selectedItem.resolveConflict(resolved);
                        listConflicts.remove(selectedIndex);
                    }
                }
            }
        });
    }

    @FXML
    public void merge(ActionEvent event) throws IOException, PathNotExistsException, RepoNotInitializedException {
        try {
            String branchToMerge = cmbBranch.getSelectionModel().getSelectedItem().getName();
            if (engine.isOpenChanges()) {
                Utils.showErrorAlert("you have some open changes, please commit your changes before you merge");
            } else {
                boolean isFFMerge = engine.handleFFMerge(branchToMerge);
                if (isFFMerge) {
                    Utils.showWarningAlert("Fast Forward merge - one branch contains another");
                    return;
                }

                List<Conflict> conflicts = engine.getConflicts(branchToMerge);

                if (conflicts.size() > 0) {
                    Utils.showErrorAlert("you have some merge conflicts! please resolve them before merge");
                    listConflicts = FXCollections.observableArrayList(conflicts);
                    listViewConflicts.setItems(listConflicts);
                } else {
                    String commitMessage = CommitMessageDialog.show(stage);
                    engine.merge(branchToMerge, commitMessage);
                    Utils.showSuccessAlert("branch - " + branchToMerge + "merged into head successfully!");
                }
            }
        } catch (RuntimeException e) {
            Utils.showErrorAlert("you didn't select branch name to marge, please select from the branches list");
        }
    }

    @FXML
    public void mergeAfterResolve(ActionEvent event) throws PathNotExistsException,
            IOException, RepoNotInitializedException {
        String branchToMerge = cmbBranch.getSelectionModel().getSelectedItem().getName();
        if (listConflicts.size() > 0){
            Utils.showErrorAlert("you have some merge conflicts! please resolve them before merge");
        }
        else{
            String commitMessage = CommitMessageDialog.show(stage);
            engine.merge(branchToMerge, commitMessage);
            Utils.showSuccessAlert("branch - " + branchToMerge + "merged into head successfully!");
        }
    }

    public void setEngine(Engine engine) {
        this.engine = engine;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public void updateBranchesList(List<Branch> branches){
        listBranches = FXCollections.observableArrayList(branches);
        cmbBranch.setItems(listBranches);
    }
}
