package controllers;

import abstractions.Engine;
import dialogs.FileSystemDialog;
import exceptions.PathNotExistsException;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.stage.Stage;
import models.Changes;
import models.Commit;
import models.Folder;
import utils.Utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CommitNodeController {

    @FXML
    private Label branchLabel;

    @FXML
    private Label dateLabel;

    @FXML
    private Label messageLabel;

    @FXML
    private Label userLabel;

    private Commit commitData;

    private Engine engine;

    private Stage stage;

    public void setCommitBranch(String branch) {
        branchLabel.setText(branch);
        branchLabel.setTooltip(new Tooltip(branch));
    }

    public void setCommitData(Commit commitData){
        this.commitData = commitData;
        dateLabel.setText(commitData.getDate());
        dateLabel.setTooltip(new Tooltip(commitData.getDate()));

        userLabel.setText(commitData.getCommiter());
        userLabel.setTooltip(new Tooltip(commitData.getCommiter()));

        messageLabel.setText(commitData.getCommitMessage());
        messageLabel.setTooltip(new Tooltip(commitData.getCommitMessage()));
    }

    public void setStage(Stage stage){
        this.stage = stage;
    }

    @FXML
    public void showFileSystem() throws IOException, PathNotExistsException {
        Folder folder = engine.getRootFolderCommit(commitData.getSha1());
        FileSystemDialog.show(stage, folder, engine.getActiveRepo().getLocation());
    }

    @FXML
    public void showCommitData() throws IOException, PathNotExistsException {
        if (commitData.getSecondParent() != null)
        {
            List<String> options = new ArrayList<>();
            options.add(commitData.getFirstPrecedingSha1());
            options.add(commitData.getSecondPrecedingSha1());
            String res = Utils.showChooseDialog(options,
                    "choose parent",
                    "choose between which parent to make the diff",
                    "parent:");
        }
        String parentSha1 = commitData.getFirstParent() != null ? commitData.getFirstParent().getSha1() : "";
        Changes diff = engine.getDiff(parentSha1, commitData.getSha1());
        StringBuilder sb = new StringBuilder();
        sb.append(commitData.toString());
        sb.append("\n");
        sb.append("diff from prev commit: \n");
        sb.append(diff.toString());
        Utils.showTextDialog("Commit Data", "Commit Details: ",  sb.toString());
    }

    public void setEngine(Engine engine) {
        this.engine = engine;
    }
}
