package controllers;

import abstractions.Engine;
import exceptions.PathNotExistsException;
import exceptions.RepoNotInitializedException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import models.Branch;
import utils.Utils;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class BranchesController implements Initializable {

    @FXML private Pane pnlOverview;
    @FXML private Label lblHead;
    @FXML private TextField txtHeadBranch;
    @FXML private VBox pnItems;
    @FXML private ListView<Branch> listViewBranches;
    @FXML private Button btnCreate;
    @FXML private Button btnDelete;
    @FXML private Button btnReset;
    @FXML private Button btnSwitch;
    @FXML private TextField selectedBranch;

    private Engine engine;
    private ObservableList<Branch> listBranches;
    private int selectedIndex = -1;

    @FXML
    public void createBranch(ActionEvent event) throws RepoNotInitializedException,
            PathNotExistsException, IOException {
        String branchType="2";
        String branchToAdd = selectedBranch.getText();

        if (branchToAdd.isEmpty())
            Utils.showErrorAlert("you didn't insert branch name to create, please insert in the text box");
        else{
            String commitSha1 = Utils.showInputTextDialog("New Branch",
                    "new branch sha1",
                    "please insert the commit sha1:");
            if(!engine.isPointedAsRB(commitSha1).equals("")){
                branchType = Utils.showInputTextDialog("New Branch",
                        "This sha1 pointed by the branch: " + engine.isPointedAsRB(commitSha1) +  "\n" +
                                "if you want to creat the new branch as RTB to " + engine.isPointedAsRB(commitSha1) +
                                " press 1 else press 2" , "please insert your choice:");
            }
            if (engine.addNewBranch(branchToAdd, commitSha1,branchType, engine.isPointedAsRB(commitSha1))){
                Utils.showSuccessAlert("branch added - " + branchToAdd);

                ButtonType answer = Utils.openYesNoDialog("","do you want to switch to branch- " + branchToAdd + "?");

                if (answer.getText().equals("Yes")) {
                    if (engine.isOpenChanges())
                        Utils.showErrorAlert("you have some open changes, the switch branch failed");
                    else
                        callSwitch(branchToAdd);
                }
            }
            else
                Utils.showErrorAlert("branch - " + branchToAdd + " is already exists!");

            listBranches.add(new Branch(branchToAdd));
            selectedBranch.clear();
        }
    }

    @FXML
    public void deleteBranch(ActionEvent event)
            throws PathNotExistsException, RepoNotInitializedException {
        try {
            String branchToDelete = listViewBranches.getSelectionModel().getSelectedItem().getName();

        if (!engine.getHeadBranch().equals(branchToDelete)) {
            if (engine.deleteBranch(branchToDelete)){
                listBranches.remove(selectedIndex);
                Utils.showSuccessAlert("branch deleted - " + branchToDelete);
            }

            else
                Utils.showErrorAlert("branch - " + branchToDelete + " not exists!");
        }
        else
            Utils.showErrorAlert("branch - " + branchToDelete + " is the head branch, you can't delete it!");
        selectedBranch.clear();
        } catch (RuntimeException e) {
            Utils.showErrorAlert("you didn't select branch name to delete, please select from the branches list");
        }
    }

    @FXML
    public void switchBranch(ActionEvent event) throws PathNotExistsException,
            RepoNotInitializedException, IOException {
        List<String> branchesStr = engine.getAllBranches();
        String branchName = Utils.showChooseDialog(branchesStr,
                "Switch Branch",
                "Enter branch name you want to switch to- ",
                "Branch: ");
        if (branchName.isEmpty())
            Utils.showErrorAlert("you didn't choose any branch");
        else {
            String start = branchName.substring(0, 1);
            if (start.equals("\\")) {
                Utils.showErrorAlert("the branch you choose is rb, you can create an rtb and switch to him");
            } else if (engine.isOpenChanges())
                Utils.showErrorAlert("you have some open changes, please commit your changes before you switch branch");
            else
                callSwitch(branchName);
        }
    }

    private void callSwitch(String branch) throws RepoNotInitializedException,
            PathNotExistsException, IOException {
        if (engine.switchBranch(branch)){
            lblHead.setText("Head Branch - " + branch);
            Utils.showSuccessAlert("branch switched to - " + branch);
        }
        else
            Utils.showErrorAlert("branch - " + branch + " not exist!");
    }

    @FXML
    public void reset(ActionEvent event) throws PathNotExistsException,
            RepoNotInitializedException, IOException {
        String commitSha1 = Utils.showInputTextDialog("Reset",
                "Enter the commit sha1 you want to reset the head branch to- ",
                "Sha1: ");
        if (engine.isOpenChanges())
            Utils.showErrorAlert("you have some open changes, please commit them before you reset branch");
        else{
            if (engine.isCommitExists(commitSha1)){
                engine.resetHeadBranchToCommit(commitSha1);
                Utils.showSuccessAlert("reset head branch to commit - " + commitSha1);
            }
            else
                Utils.showErrorAlert("the commit sha1 not exists!");
        }
    }

    public void updateBranchesList(List<Branch> branches) {
        listBranches = FXCollections.observableArrayList(branches);
        listViewBranches.setItems(listBranches);
    }

    public void updateHeadBranch() throws RepoNotInitializedException {
        String headBranch = engine.getHeadBranch();
        lblHead.setText("Head Branch - " + headBranch);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        listViewBranches.setOnMouseClicked(event -> {
            String selectedItem = listViewBranches.getSelectionModel().getSelectedItem().toString();
            selectedIndex = listViewBranches.getSelectionModel().getSelectedIndex();
            selectedBranch.setText(selectedItem);
        });
    }

    public void setEngine(Engine engine) {
        this.engine = engine;
    }
}