package tasks;

import abstractions.Engine;
import javafx.concurrent.Task;

public class LoadXmlTask extends Task {

    private String xmlFilePath;
    private Engine engine;

    public LoadXmlTask(String xmlFilePath, Engine engine){
        this.xmlFilePath = xmlFilePath;
        this.engine = engine;
    }

   @Override
    protected Object call() throws Exception {
        engine.loadRepoFromXML(xmlFilePath);
        return null;
    }
}
