package models;

import abstractions.Engine;
import com.fxgraph.cells.AbstractCell;
import com.fxgraph.graph.Graph;
import com.fxgraph.graph.IEdge;
import com.fxgraph.graph.Model;
import controllers.CommitNodeController;
import javafx.beans.binding.DoubleBinding;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.layout.Region;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class CommitNode extends AbstractCell {

    private Commit commit;
    private int x;
    private int y;
    private List<CommitNode> children;
    private String branch;
    private Engine engine;
    private Stage stage;

    public CommitNode(Commit commit, Engine engine, Stage stage) {
        this(commit, "", engine, stage);
    }

    public CommitNode(Commit commit, String branch, Engine engine, Stage stage) {
        this.branch = branch;
        this.commit = commit;
        this.children = new ArrayList<>();
        this.engine = engine;
        this.stage = stage;
    }

    public void addChild(CommitNode commitNode){
        children.add(commitNode);
    }

    public List<CommitNode> getChildren(){
        return this.children;
    }

    public void addGraphics(Model model, int x, int y){
        this.x = x;
        this.y = y;
        model.addCell(this);
        y++;
        for (CommitNode child : children) {
            model.addEdge(this, child);
            if (!model.getAddedCells().contains(child))
                child.addGraphics(model, x, y);
            x++;
        }
    }

    @Override
    public Region getGraphic(Graph graph) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
            URL url = getClass().getResource("/views/CommitNodeFull.fxml");
            fxmlLoader.setLocation(url);
            Region root = fxmlLoader.load();

            CommitNodeController commitNodeController = fxmlLoader.getController();
            commitNodeController.setEngine(engine);
            commitNodeController.setCommitBranch(branch);
            commitNodeController.setCommitData(commit);
            commitNodeController.setStage(stage);

            return root;
        } catch (IOException e) {
            return new Label("Error when tried to create graphic node !");
        }
    }

    @Override
    public DoubleBinding getXAnchor(Graph graph, IEdge edge) {
        final Region graphic = graph.getGraphic(this);
        return graphic.layoutXProperty().add(7);
    }

    public String getSha1() {
        return commit.getSha1();
    }

    public Commit getCommit() { return commit;}

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getBranch() {
        return branch;
    }
}
