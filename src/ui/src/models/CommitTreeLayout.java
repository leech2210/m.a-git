package models;

import com.fxgraph.graph.Graph;
import com.fxgraph.graph.ICell;
import com.fxgraph.layout.Layout;
import utils.Consts;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class CommitTreeLayout implements Layout {

    @Override
    public void execute(Graph graph) {
        final List<ICell> cells = graph.getModel().getAllCells();

        List<CommitNode> commitNodes = new ArrayList<>();

        int maxY = 0;
        for (ICell cell: cells){
            maxY++;
            commitNodes.add((CommitNode)cell);
        }

        Collections.sort(commitNodes, new Comparator<CommitNode>() {
            @Override
            public int compare(CommitNode o1, CommitNode o2) {
                try {
                    SimpleDateFormat formatter = new SimpleDateFormat(Consts.DATE_FORMAT);
                    Date date1 = formatter.parse(o1.getCommit().getDate());
                    Date date2 = formatter.parse(o2.getCommit().getDate());
                    return date1.compareTo(date2);
                } catch (ParseException e) {
                    e.printStackTrace();
                    return 0;
                }
            }
        });

        int startX = 150;
        int startY = 50;
        int currY = 0;
        for (CommitNode cell : commitNodes) {
            graph.getGraphic(cell).relocate(startX + cell.getX()*70, startY + (maxY - currY)*60);
            currY++;
        }
    }
}
